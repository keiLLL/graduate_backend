package com.kf.config;

import com.kf.mapper.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;



import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import org.springframework.scheduling.support.PeriodicTrigger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 定时任务
 */
@Data
@Slf4j
@Component
public class ScheduleTask1 implements SchedulingConfigurer {

    /**
     * cron
     *  @Value("${printTime.cron}")
     */
    private String cron="0 0/5 * * * ?";

    private Long timer = 10000000L;
    @Resource
    UserMapper userMapper;
    @Resource
    AdminMapper adminMapper;


    @Resource
    UserRoleMapper userRoleMapper;


    @Resource
    TeacherMapper teacherMapper;


    @Resource
    StudentMapper studentMapper;
    @Resource
    CategoryMapper categoryMapper;
    @Resource
    ChoCgyMapper choCgyMapper;
    @Resource
    ChoCgyRecordMapper choCgyRecordMapper;
    @Resource
    GradeMapper gradeMapper;
    @Resource
    PaperFileMapper paperFileMapper;
    @Resource
    RecordsMapper recordsMapper;
    @Resource
    ReportMapper reportMapper;
    @Resource
    WenFileMapper wenFileMapper;
    @Resource
    DefenceMapper defenceMapper;

    @Resource
    PermissionMapper permissionMapper;
    @Resource
    RoleMapper roleMapper;
    @Resource
    RolePermsMapper rolePermsMapper;
    @Resource
    SysDictDataMapper sysDictDataMapper;
    @Resource
    SysDictTypeMapper sysDictTypeMapper;
    @Resource
    TreePermMapper treePermMapper;
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        // 动态使用cron表达式设置循环间隔
        taskRegistrar.addTriggerTask(() -> {
            log.info("Current time： {}", LocalDateTime.now());

        }, triggerContext -> {
            // 使用CronTrigger触发器，可动态修改cron表达式来操作循环规则
//                CronTrigger cronTrigger = new CronTrigger(cron);
//                Date nextExecutionTime = cronTrigger.nextExecutionTime(triggerContext);

            // 使用不同的触发器，为设置循环时间的关键，区别于CronTrigger触发器，该触发器可随意设置循环间隔时间，单位为毫秒
            PeriodicTrigger periodicTrigger = new PeriodicTrigger(timer);
            Date nextExecutionTime = periodicTrigger.nextExecutionTime(triggerContext);
            return nextExecutionTime;
        });
    }
}