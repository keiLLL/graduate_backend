package com.kf.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;


@EnableSwagger2
@Configuration
public class SwaggerConfig {
    @Bean
    public Docket createRestApi() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        //作者信息
        Contact contact = new Contact("kei", "localhost:8081", "1234@qq.com");
        ApiInfo apiInfo = new ApiInfo("毕业设计过程管理及质量控制综合服务智能平台", "", "v1.0", "localhost:8081", contact, "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0", new ArrayList<>());
        docket.apiInfo(apiInfo);
        docket.groupName("Group One");
        ApiSelectorBuilder select = docket.select();
        //指定需要扫描的包
        select.apis(RequestHandlerSelectors.basePackage("com.kf.controller"));
        select.build();
        //路径，过滤什么路径
//        select.paths()
        return docket;
    }
}

