//package com.kf.config;
//
//import com.alibaba.nacos.api.config.annotation.NacosValue;
//import lombok.Data;
//import org.springframework.stereotype.Component;
//
//
//@Component
//@Data
//public class NacosOssConfig {
//    @NacosValue(value = "${endpoint}", autoRefreshed = true)
//    String endpoint;
//    @NacosValue(value = "${accessKeyId}", autoRefreshed = true)
//    String accessKeyId;
//    @NacosValue(value = "${accessKeySecret}", autoRefreshed = true)
//    String accessKeySecret;
//    @NacosValue(value = "${bucketName}", autoRefreshed = true)
//    String bucketName;
//    @NacosValue(value = "${wenurl}", autoRefreshed = true)
//    String wenurl;
//    @NacosValue(value = "${paperurl}", autoRefreshed = true)
//    String paperurl;
//}
