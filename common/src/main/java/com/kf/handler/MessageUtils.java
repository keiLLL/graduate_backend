

package com.kf.handler;

import com.kf.util.SpringContextUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;


public class MessageUtils {
    private static final MessageSource MESSAGE_SOURCE;
    static {
        MESSAGE_SOURCE = (MessageSource) SpringContextUtils.getBean("messageSource");
    }

    public static String getMessage(int code){
        return getMessage(code, new String[0]);
    }

    public static String getMessage(int code, String... params){
        return MESSAGE_SOURCE.getMessage(code+"", params, LocaleContextHolder.getLocale());
    }
}
