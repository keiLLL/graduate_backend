

package com.kf.handler;


import com.kf.util.ResultCode;


/**
 * 自定义异常
 */
public class MyException extends RuntimeException {
	private static final long serialVersionUID = 1L;

    private int code;
	private String msg;

	public MyException(ResultCode resultCode) {
		this.code = resultCode.getCode();
		this.msg = resultCode.getMessage();
	}
	public MyException(int code) {
		this.code = code;
		this.msg = MessageUtils.getMessage(code);
	}

	public MyException(int code, String... params) {
		this.code = code;
		this.msg = MessageUtils.getMessage(code, params);
	}

	public MyException(int code, Throwable e) {
		super(e);
		this.code = code;
		this.msg = MessageUtils.getMessage(code);
	}

	public MyException(int code, Throwable e, String... params) {
		super(e);
		this.code = code;
		this.msg = MessageUtils.getMessage(code, params);
	}

	public MyException(String msg) {
		super(msg);
		this.code = ResultCode.FAIL.getCode();
		this.msg = msg;
	}

	public MyException(String msg, Throwable e) {
		super(msg, e);
		this.code = ResultCode.FAIL.getCode();
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}


	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

}