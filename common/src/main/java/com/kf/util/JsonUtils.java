package com.kf.util;



import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.multipart.MultipartFile;


import java.util.List;

/**
 * JSON 工具类
 */
public class JsonUtils {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();


    public static String toJsonString(Object object) {
        try {
            if(object instanceof MultipartFile){
                System.out.println("你上次的文件");
                return ((MultipartFile) object).getName();
            }
            if(object instanceof List<?>){
                return "文件";
            }
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
