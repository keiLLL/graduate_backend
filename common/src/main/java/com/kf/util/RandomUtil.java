package com.kf.util;

import java.text.DecimalFormat;

import java.util.Random;

/**
 * 获取随机数
 */
public class RandomUtil {

	private static final Random RANDOM = new Random();


	private static final DecimalFormat SIXDF = new DecimalFormat("000000");


	public static String getSixBitRandom() {
		return SIXDF.format(RANDOM.nextInt(1000000));
	}


}
