package com.kf.util;

import lombok.Getter;

@Getter
public enum ResultCode {
    /** 成功*/
    SUCCESS(200,"成功"),
    FAIL(500,"失败"),
    PERMISSION_ERROR(403,"您没有权限"),
    TOKEN_ERROR(401,"无token"),
    SUCCESS_UPDATE_PWD(200,"修改成功"),
    SUCCESS_FLOGIN_STU(210,"学生第一次登录成功"),
    SUCCESS_FLOGIN_TEA(211,"老师第一次登录成功"),
    SUCCESS_FLOGIN_ADM(212,"管理员第一次登录成功"),
    SUCCESS_LOGIN_STU(213,"学生登录成功"),
    SUCCESS_LOGIN_TEA(214,"老师登录成功"),
    SUCCESS_LOGIN_ADM(215,"管理员登录成功"),
    SUCCESS_FIRST(201,"第一次成功"),
    CODE_ERROR(1000,"验证码错误"),
    DEFENCE_TEACHER_ERROR(216,"指导老师答辩安排重复"),
    /** 错误参数*/
    PARAM_IS_INVALID(1001,"参数无效"),
    PARAM_IS_BLANK(1002,"参为空"),
    PARAM_TYPE_ERROR(1003,"参数类型错误"),
    PARAM_NOT_COMPLETE(1004,"参数缺失"),
    LOGIN_ERROR(1005,"账号不存在"),
    LOGIN_ERROR1(1006,"账号与手机号不对应"),
    PHONE_ERROR(1007,"该手机号未绑定账号"),
    /** 用户错误*/
    USER_NOT_LOGIN_IN(2001,"用户未登录"),
    USER_LOGIN_ERROR(2002,"账号不存在或者密码错误"),

    USER_ACCOUNT_FORBIDDEN(2003,"账户被禁用"),
    USER_NOT_EXISTS(2004,"用户不存在"),
    USER_HAS_EXISTED(2005,"用户已存在"),
    USER_SELECT_ERROR(2006,"查询失败"),
    USER_UPDATE_ERROR(2006,"修改失败"),
    USER_DELETE_ERRPR(2007,"删除失败"),

    USER_UPFILE_ERROR(2008,"文件类型错误"),
    USER_DELETE_ERROR2(2009,"删除失败,您已成功选择课题，请联系老师删除"),
    USER_INSERT_ERROR(2010,"添加失败"),
    USER_CHECK_ERROR(2011,"审核失败"),
    CATEGORY_CHECK_ERROR(2012,"该课题应该在“审核课题”模块处理"),
    /** student*/
    STUDENT_UP_ERROR(3001,"操作失败,检查是否已选择指导老师"),
    STUDENT_CHOOSE_ERROR(3002,"身份验证失败"),
    STUDENT_CHOOSE_ERROR2(3003,"选择指导老师有误"),
    STUDENT_CHOOSE_ERROR3(3004,"您还未选择指导老师"),
    STUDENT_CHOOSE_ERROR4(3005,"只能选择老师的课题"),

    STUDENT_ADVISE_ERROR(3006,"请先让指导老师进行审核"),
    STUDENT_ADVISE_ERROR2(3007,"指导老师已审核通过，请联系指导老师修改"),
    STUDENT_DELETE_ERROR2(3008,"指导老师已审核通过，请联系指导老师删除"),
    STUDENT_CHOOSE_ERROR5(3008,"请检查指导老师教职工号是否填错"),
    STUDENT_CHOOSE_ERROR6(3009,"你已经选择课题，无法申报"),
    STUDENT_CHOOSE_ERROR7(3010,"你已经申报课题，无法选择"),
    STUDENT_CHOOSE_ERROR8(3011,"你已经成功选择题目，无法再次选择"),
    STUDENT_CHOOSE_ERROR9(3012,"没有数据，无法删除"),
    STUDENT_CHOOSE_ERROR10(3013,"未选择课题"),
    STUDENT_ADD_ERROR(3014,"用户名重复"),
    STUDENT_CHOOSE_ERROR11(3014,"您已经选择成功，无法再选"),
    STUDENT_CHOOSE_ERROR12(3015,"审核通过无法再次选择"),
    STUDENT_ADD_ERROR2(3016,"手机号重复"),
    SHUJU_ADD_ERROR3(3017,"数据重复"),
    STUDENT_APPLY_ERROR3(3018,"您申报的课题已经被老师通过，请联系指导老师"),
    CATEGORY_APPLY_ERROR3(3019,"课题名重复"),
    CATEGORY_APPLY_ERROR(3020,"该课题已被院系老师审核通过，请联系院系老师"),
    //admin错误
    USER_ADD_ERROR(5001,"用户添加失败"),
    ROLE_DELETE_ERROR(5002,"角色删除失败"),
    ROLE_ADD_ERROR(5003,"角色添加失败"),
    ROLE_ADD_ERROR1(5004,"角色名字重复"),
    DICT_DELETE_ERROR(5005,"字典删除失败，数据库中已存在该字段"),
    CAI_ADD_ERROR3(5006,"同级目录下菜单名重复"),
    UPLOAD_ERROR(5007, "请勿重复发送");


    /** 错误代码*/
    private  Integer code;
    /** 提示信息*/
    private  String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }


}

