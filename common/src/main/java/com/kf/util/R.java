package com.kf.util;

import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

public class R extends HashMap<String, Object> {
    public R() {
        put("code", HttpStatus.SC_OK);
        put("msg", "success");
    }


    public static R error() {
        return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, "未知异常，请联系管理员");
    }

    public static R error(String msg) {
        return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
    }

    public static R error(int code, String msg) {
        R r = new R();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }
    public static R okey(ResultCode resultCode) {
        R r = new R();
        r.put("code", resultCode.getCode());
        r.put("msg", resultCode.getMessage());
        return r;
    }

    public static R error(ResultCode resultCode) {
        R r = new R();
        r.put("code", resultCode.getCode());
        r.put("msg", resultCode.getMessage());
        return r;
    }
    public static R okey(int code, String msg) {
        R r = new R();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }
    public static R okey(String msg) {
        return okey(HttpStatus.SC_CREATED,msg);

    }
    public static R ok(String msg) {
        R r = new R();
        r.put("msg", msg);
        return r;
    }


    public static R ok(Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        return r;
    }

    public static R ok() {
        return new R();
    }

    @Override
    public R put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public static R fail(String msg) {
        R r = new R();
        r.put("msg", msg);
        return r;
    }


}
