package com.kf.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.Report;
import com.kf.pojo.User;
import com.kf.pojo.form.*;

import java.util.List;

/**
 * <p>
 *  服务类
 */
public interface ReportService extends IService<Report> {

    Report selectByUsername(String username);
    void insert(Report report, User student, ReportForm reportForm);
    void delete(String username);
    IPage<Report> page(SelectReportForm selectCgyForm, User teacher);
    void deleteReport(DeleteForm deleteForm);


}
