package com.kf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.InspectionReport;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public interface InspectionReportService extends IService<InspectionReport> {
    int getCount(InspectionReport inspectionReport);

    void downInspectionReport(Integer stage, String susername, HttpServletResponse response) throws IOException;

    void updateStatus(String susername);
}
