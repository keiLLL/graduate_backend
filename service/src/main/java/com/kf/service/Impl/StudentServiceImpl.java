package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.mapper.*;
import com.kf.pojo.*;
import com.kf.pojo.form.StudentForm;
import com.kf.service.StudentService;
import com.kf.service.UserService;
import org.aspectj.lang.annotation.After;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper,Student > implements StudentService {
    @Autowired
    StudentMapper studentMapper;
    @Autowired
    UserService userService;
    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    ChoCgyRecordMapper choCgyRecordMapper;
    @Autowired
    ChoCgyMapper choCgyMapper;
    @Autowired
    GradeMapper gradeMapper;
    @Autowired
    PaperFileMapper paperFileMapper;
    @Autowired
    RecordsMapper recordsMapper;
    @Autowired
    ReportMapper reportMapper;
    @Autowired
    ReportFileMapper reportFileMapper;
    @Autowired
    WenFileMapper wenFileMapper;
    @Override
    public Student findById(int userId){
        QueryWrapper<Student> studentQueryWrapper = new QueryWrapper<>();
        studentQueryWrapper.eq("user_id",userId);
        Student student = studentMapper.selectOne(studentQueryWrapper);
        return student;
    }

    @Override
    public void updateSelf(Student student){

        studentMapper.updateById(student);

    }

    @Override
    public void insert(Student student) {
        studentMapper.insert(student);
    }

    @Override
    public void updateStudent(User user, Student student, String encrypt, StudentForm studentForm) {
        int userId = user.getUserId();

        user.setPassword(encrypt)
                .setName(studentForm.getStuName())
                .setEmail(studentForm.getStuMail())
                .setSex(studentForm.getStuSex())
                .setPhone(studentForm.getStuPhone())
                .setDepartment(studentForm.getStuDepartment())
                .setState(studentForm.getState())
                .setStatus(studentForm.getStatus())
                .setUsername(studentForm.getUsername());

        QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
        choCgyQueryWrapper.eq("user_id",userId);
        ChoCgy choCgy = choCgyMapper.selectOne(choCgyQueryWrapper);
        if(choCgy!=null){
            choCgy.setSusername(user.getUsername());
            choCgy.setStuName(user.getName());
            choCgyMapper.updateById(choCgy);
        }
        QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();
        choCgyQueryWrapper.eq("user_id",userId);
        Category category = categoryMapper.selectOne(categoryQueryWrapper);
        if(category!=null){
            category.setUsername(user.getUsername());
            category.setName(user.getName());
            categoryMapper.updateById(category);
        }


        QueryWrapper<ChoCgyRecord> choCgyRecordQueryWrapper = new QueryWrapper<>();
        choCgyRecordQueryWrapper.eq("user_id",userId);
        ChoCgyRecord choCgyRecord = choCgyRecordMapper.selectOne(choCgyRecordQueryWrapper);
        if(choCgyRecord!=null){
            choCgyRecord.setSusername(user.getUsername()).setStuName(user.getName());
            choCgyRecordMapper.updateById(choCgyRecord);
        }


        QueryWrapper<PaperFile> paperFileQueryWrapper = new QueryWrapper<>();
        paperFileQueryWrapper.eq("user_id",userId);
        PaperFile paperFile = paperFileMapper.selectOne(paperFileQueryWrapper);
        if(choCgyRecord!=null) {
            paperFile.setCreateby(user.getName()).setSusername(user.getUsername());
            paperFileMapper.updateById(paperFile);
        }


        QueryWrapper<Records> recordsQueryWrapper = new QueryWrapper<>();
        recordsQueryWrapper.eq("user_id",userId);
        List<Records> records = recordsMapper.selectList(recordsQueryWrapper);
        records.forEach(x->{
            x.setName(studentForm.getStuName()).setUsername(studentForm.getUsername()).setState(studentForm.getState());
            recordsMapper.updateById(x);
        });




        QueryWrapper<ReportFile> reportFileQueryWrapper = new QueryWrapper<>();
        reportFileQueryWrapper.eq("user_id",userId);
        ReportFile reportFile = reportFileMapper.selectOne(reportFileQueryWrapper);
        if(reportFile!=null){
            reportFile.setSusername(user.getUsername());
            reportFile.setCreateby(user.getName());
            reportFileMapper.updateById(reportFile);
        }


        QueryWrapper<WenFile> wenFileQueryWrapper = new QueryWrapper<>();
        wenFileQueryWrapper.eq("user_id",userId);
        WenFile wenFile = wenFileMapper.selectOne(wenFileQueryWrapper);
        if(wenFile!=null){
            wenFile.setSusername(user.getUsername()).setCreateby(user.getName());
            wenFileMapper.updateById(wenFile);
        }


        userService.updateById(user);
        student.setStuName(studentForm.getStuName())
                .setStuSex(studentForm.getStuSex())
                .setStuDepartment(studentForm.getStuDepartment())
                .setStuProfessional(studentForm.getStuProfessional())
                .setStuPhone(studentForm.getStuPhone())
                .setStuMail(studentForm.getStuMail())
                .setStuClass(studentForm.getStuClass())
                .setStuWx(studentForm.getStuWx())
                .setStuQq(studentForm.getStuQq())
                .setStuTitle(studentForm.getStuTitle())
                .setStuStatus(studentForm.getStatus());
        studentMapper.updateById(student);
    }

    @Override
    public void updateStudentSelf(User user, Student student, StudentForm studentForm) {
        int userId = user.getUserId();

        user.setName(studentForm.getStuName())
                .setEmail(studentForm.getStuMail())
                .setSex(studentForm.getStuSex())
                .setPhone(studentForm.getStuPhone())
                .setDepartment(studentForm.getStuDepartment())
                .setState(studentForm.getState())
                .setStatus(studentForm.getStatus())
                .setUsername(studentForm.getUsername());

//        QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
//        choCgyQueryWrapper.eq("user_id",userId);
//        ChoCgy choCgy = choCgyMapper.selectOne(choCgyQueryWrapper);
//        if(choCgy!=null){
//            choCgy.setSusername(user.getUsername());
//            choCgy.setStuName(user.getName());
//            choCgyMapper.updateById(choCgy);
//        }


//        QueryWrapper<ChoCgyRecord> choCgyRecordQueryWrapper = new QueryWrapper<>();
//        choCgyRecordQueryWrapper.eq("user_id",userId);
//        ChoCgyRecord choCgyRecord = choCgyRecordMapper.selectOne(choCgyRecordQueryWrapper);
//        if(choCgyRecord!=null){
//            choCgyRecord.setSusername(user.getUsername()).setStuName(user.getName());
//            choCgyRecordMapper.updateById(choCgyRecord);
//        }
//
//
//        QueryWrapper<PaperFile> paperFileQueryWrapper = new QueryWrapper<>();
//        paperFileQueryWrapper.eq("user_id",userId);
//        PaperFile paperFile = paperFileMapper.selectOne(paperFileQueryWrapper);
//        if(paperFile!=null) {
//            paperFile.setCreateby(user.getName()).setSusername(user.getUsername());
//            paperFileMapper.updateById(paperFile);
//        }
//
//
//        QueryWrapper<Records> recordsQueryWrapper = new QueryWrapper<>();
//        recordsQueryWrapper.eq("user_id",userId);
//        List<Records> records = recordsMapper.selectList(recordsQueryWrapper);
//        records.forEach(x->{
//            x.setName(studentForm.getStuName()).setUsername(studentForm.getUsername()).setState(studentForm.getState());
//            recordsMapper.updateById(x);
//        });
//
//
//
//
//        QueryWrapper<Report> reportQueryWrapper = new QueryWrapper<>();
//        reportQueryWrapper.eq("user_id",userId);
//        Report report = reportMapper.selectOne(reportQueryWrapper);
//        if(report!=null){
//            report.setSusername(user.getUsername());
//            report.setCreateby(user.getName());
//            reportMapper.updateById(report);
//        }
//
//
//        QueryWrapper<WenFile> wenFileQueryWrapper = new QueryWrapper<>();
//        wenFileQueryWrapper.eq("user_id",userId);
//        WenFile wenFile = wenFileMapper.selectOne(wenFileQueryWrapper);
//        if(wenFile!=null){
//            wenFile.setSusername(user.getUsername()).setCreateby(user.getName());
//            wenFileMapper.updateById(wenFile);
//        }


        userService.updateById(user);
        student.setStuName(studentForm.getStuName())
                .setStuSex(studentForm.getStuSex())
                .setStuDepartment(studentForm.getStuDepartment())
                .setStuProfessional(studentForm.getStuProfessional())
                .setStuPhone(studentForm.getStuPhone())
                .setStuMail(studentForm.getStuMail())
                .setStuClass(studentForm.getStuClass())
                .setStuWx(studentForm.getStuWx())
                .setStuQq(studentForm.getStuQq())
                .setStuTitle(studentForm.getStuTitle())
                .setStuStatus(studentForm.getStatus());
        studentMapper.updateById(student);
    }

}


