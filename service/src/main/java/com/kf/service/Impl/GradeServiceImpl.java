package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kf.mapper.DefenceMapper;
import com.kf.pojo.Defence;
import com.kf.pojo.Grade;
import com.kf.mapper.GradeMapper;
import com.kf.pojo.User;
import com.kf.pojo.form.AddGrade;
import com.kf.pojo.form.SelectForm;
import com.kf.service.GradeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class GradeServiceImpl extends ServiceImpl<GradeMapper, Grade> implements GradeService {
    @Resource
    GradeMapper gradeMapper;

    @Override
    public void insert(AddGrade addGrade, User user) {



    }

    @Override
    public IPage<Grade> pageGrade(SelectForm selectForm, User user) {
        Page<Grade> gradePage = new Page<>(selectForm.getCurrent(),selectForm.getSize());

        QueryWrapper<Grade> gradeQueryWrapper = new QueryWrapper<>();
        gradeQueryWrapper.eq("tusername",user.getUsername());
        gradeQueryWrapper.like("susername",selectForm.getName());
        gradeQueryWrapper.like("stu_name",selectForm.getName());
        IPage<Grade> gradeIPage = gradeMapper.selectPage(gradePage, gradeQueryWrapper);

        return gradeIPage;
    }
}
