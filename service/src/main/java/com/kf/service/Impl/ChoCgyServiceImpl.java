package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kf.config.AliyunOSSUtil;
import com.kf.mapper.*;
import com.kf.pojo.*;
import com.kf.pojo.form.AddGrade;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.SelectForm;
import com.kf.service.ChoCgyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class ChoCgyServiceImpl extends ServiceImpl<ChoCgyMapper, ChoCgy> implements ChoCgyService {

    @Autowired
    ChoCgyMapper choCgyMapper;
    @Autowired
    ChoCgyRecordMapper choCgyRecordMapper;
    @Autowired
    UserService userService;
    @Resource
    PaperFileMapper paperFileMapper;
    @Resource
    RecordsMapper recordsMapper;
    @Resource
    ReportMapper reportMapper;
    @Resource
    WenFileMapper wenFileMapper;
    @Resource
    AliyunOSSUtil aliyunOSSUtil;
    @Override
    public void insertCho(ChoCgy choCgy) {
        choCgyMapper.insert(choCgy);
    }

    @Override
    public void deleteChoMy(DeleteForm deleteForm) {
        List<Integer> list = deleteForm.getList();
        for (Integer integer : list) {
            QueryWrapper<ChoCgyRecord> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("id",integer);
            ChoCgyRecord choCgyRecord = choCgyRecordMapper.selectById(integer);
            Integer userId = choCgyRecord.getUserId();
            User user = userService.getById(userId);
            choCgyRecordMapper.delete(queryWrapper);

            QueryWrapper<ChoCgy> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("user_id",userId);
                QueryWrapper<PaperFile> paperFileQueryWrapper1 = new QueryWrapper<>();
                paperFileQueryWrapper1.eq("user_id",userId);
                PaperFile paperFile = paperFileMapper.selectOne(paperFileQueryWrapper1);
                if(paperFile!=null){
                    aliyunOSSUtil.deleteFile(paperFile.getPath() + "/" + paperFile.getFilename() + "." + paperFile.getSuffix());
                    QueryWrapper<PaperFile> paperFileQueryWrapper = new QueryWrapper<>();
                    paperFileQueryWrapper.eq("user_id",userId);
                    paperFileMapper.delete(paperFileQueryWrapper);
                }


                QueryWrapper<WenFile> wenFileQueryWrapper = new QueryWrapper<>();
                wenFileQueryWrapper.eq("user_id",userId);
                WenFile wenFile = wenFileMapper.selectOne(wenFileQueryWrapper);
                if(wenFile!=null){
                    aliyunOSSUtil.deleteFile(wenFile.getPath()+"/"+wenFile.getFilename()+"."+ wenFile.getSuffix());
                    QueryWrapper<WenFile> wenFileQueryWrapper1 = new QueryWrapper<>();
                    wenFileQueryWrapper1.eq("user_id",userId);
                    wenFileMapper.delete(wenFileQueryWrapper1);
                }



                QueryWrapper<Report> reportQueryWrapper = new QueryWrapper<>();
                reportQueryWrapper.eq("user_id",userId);
                reportMapper.delete(reportQueryWrapper);

                QueryWrapper<Records> recordsQueryWrapper = new QueryWrapper<>();
                recordsQueryWrapper.eq("user_id",userId);
                recordsMapper.delete(recordsQueryWrapper);

                QueryWrapper<Records> recordsQueryWrapper1 = new QueryWrapper<>();
                recordsQueryWrapper1.eq("re_target",user.getUsername());
                recordsMapper.delete(recordsQueryWrapper1);

            choCgyMapper.delete(queryWrapper1);

        }

    }

    @Override
    public ChoCgy selectBySusername(String susername) {
        QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
        choCgyQueryWrapper.eq("susername",susername);
        return choCgyMapper.selectOne(choCgyQueryWrapper);
    }

    @Override
    public ChoCgy selectByuserId(int userId) {
        QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
        choCgyQueryWrapper.eq("user_id",userId);
        ChoCgy choCgy = choCgyMapper.selectOne(choCgyQueryWrapper);
        return choCgy;
    }

    @Override
    public IPage<ChoCgy> page(SelectForm selectForm, User teacher) {
        Page<ChoCgy> choCgyPage = new Page<>(selectForm.getCurrent(), selectForm.getSize());
        QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
        choCgyQueryWrapper.like("susername",selectForm.getName());
//        choCgyQueryWrapper.like("stu_name",selectForm.getName());
        choCgyQueryWrapper.eq("tusername",teacher.getUsername());
        IPage<ChoCgy> choCgyIPage = choCgyMapper.selectPage(choCgyPage, choCgyQueryWrapper);
        return choCgyIPage;
    }

    @Override
    public void insertGrade(AddGrade addGrade) {

        QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
        choCgyQueryWrapper.eq("susername",addGrade.getSusername());
        ChoCgy choCgy = choCgyMapper.selectOne(choCgyQueryWrapper);

        choCgy.setGrade(addGrade.getGrade());
        choCgy.setGrade1(addGrade.getGrade1());
        choCgy.setGrade2(addGrade.getGrade2());
        double grade=(double) (addGrade.getGrade()+ addGrade.getGrade1()+ addGrade.getGrade2())/3.00;
        double avg = (double) Math.round(grade * 100) / 100.00;
        choCgy.setAverage(avg);

        choCgyMapper.updateById(choCgy);
    }

    @Override
    public IPage<ChoCgy> pageAll(SelectForm selectForm) {
        Page<ChoCgy> choCgyPage = new Page<>(selectForm.getCurrent(), selectForm.getSize());
        QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
        choCgyQueryWrapper.like("susername",selectForm.getName()).or()
                .like("stu_name",selectForm.getName()).eq("deleted",0);


        IPage<ChoCgy> choCgyIPage = choCgyMapper.selectPage(choCgyPage, choCgyQueryWrapper);
        return choCgyIPage;

    }
}
