package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kf.mapper.RecordsMapper;
import com.kf.pojo.Records;
import com.kf.pojo.User;
import com.kf.pojo.form.AddRecord;
import com.kf.pojo.form.RecordForm;
import com.kf.pojo.form.SelectForm;
import com.kf.pojo.form.SelectRecordForm;
import com.kf.service.RecordsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 学生指导记录表 服务实现类
 * </p>
 */
@Service
public class RecordsServiceImpl extends ServiceImpl<RecordsMapper, Records> implements RecordsService {


    @Resource
    RecordsMapper recordsMapper;
    @Override
    public void inertRecord(RecordForm recordForm,User student,String tusername) {
        Records records = new Records();
        records.setName(student.getName());
        records.setUserId(student.getUserId());
        records.setUsername(student.getUsername());
        records.setState(student.getState());
        records.setReAdvise(recordForm.getReAdvise());
        records.setReHarvest(recordForm.getReHarvest());
        records.setReTeacher(tusername);
        records.setReImfor(recordForm.getReImfor());
        records.setDepartment(student.getDepartment());
        recordsMapper.insert(records);

    }

    @Override
    public IPage<Records> pageRecord(SelectForm selectForm,User user) {
        Page<Records> recordsPage = new Page<>(selectForm.getCurrent(), selectForm.getSize());
        QueryWrapper<Records> recordsQueryWrapper = new QueryWrapper<>();
        recordsQueryWrapper.eq("username",user.getUsername());
//        recordsQueryWrapper.like("")
        return recordsMapper.selectPage(recordsPage, recordsQueryWrapper);
    }

    @Override
    public void updateRecord(RecordForm recordForm, User student) {
        Records records = recordsMapper.selectById(recordForm.getRecordsId());
        records.setName(student.getName());
        records.setUserId(student.getUserId());
        records.setUsername(student.getUsername());
        records.setState(student.getState());
        records.setReAdvise(recordForm.getReAdvise());
        records.setReHarvest(recordForm.getReHarvest());

        records.setReImfor(recordForm.getReImfor());
        recordsMapper.updateById(records);
    }

    @Override
    public void deleteRecord(List<Integer> list) {
        for (Integer integer : list) {
            recordsMapper.deleteById(integer);
        }

    }

    @Override
    public IPage<Records> page(SelectForm selectForm, User teacher) {
        Page<Records> recordsPage = new Page<>(selectForm.getCurrent(), selectForm.getSize());
        QueryWrapper<Records> recordsQueryWrapper = new QueryWrapper<>();
        recordsQueryWrapper.eq("re_teacher",teacher.getUsername());
        recordsQueryWrapper.like("username",selectForm.getName());

        return recordsMapper.selectPage(recordsPage, recordsQueryWrapper);
    }

    @Override
    public void insertRecordTea(AddRecord addRecord, User teacher) {
        Records records = new Records();
        records.setUserId(teacher.getUserId());
        records.setName(teacher.getName());
        records.setUsername(teacher.getUsername());
        records.setState(teacher.getState());

        records.setReTarget(addRecord.getSusername());
        records.setReImfor(addRecord.getReImfor());
        records.setStuSituation(addRecord.getStuSituation());
        records.setDepartment(teacher.getDepartment());
        recordsMapper.insert(records);
    }

    @Override
    public IPage<Records> listRecords(SelectForm selectForm) {
        Page<Records> recordsPage = new Page<>(selectForm.getCurrent(), selectForm.getSize());
        QueryWrapper<Records> recordsQueryWrapper = new QueryWrapper<>();
        recordsQueryWrapper.eq("re_target", selectForm.getName());
        return recordsMapper.selectPage(recordsPage, recordsQueryWrapper);

    }

    @Override
    public void deleteById(int id) {
        recordsMapper.deleteById(id);
    }

    @Override
    public IPage<Records> pageRecordAll(SelectRecordForm selectRecordForm,User user) {
        Page<Records> recordsPage = new Page<>(selectRecordForm.getCurrent(), selectRecordForm.getSize());
        QueryWrapper<Records> recordsQueryWrapper = new QueryWrapper<>();
        recordsQueryWrapper.eq("department",user.getDepartment());
        if(selectRecordForm.getState()!=3){
            recordsQueryWrapper.eq("state",selectRecordForm.getState());
        }
        if(selectRecordForm.getUsername()!=null){
            recordsQueryWrapper.like("username",selectRecordForm.getUsername());
        }
        return recordsMapper.selectPage(recordsPage, recordsQueryWrapper);

    }
}
