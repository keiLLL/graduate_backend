package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.mapper.UserRoleMapper;
import com.kf.pojo.UserRole;
import com.kf.service.UserRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    @Resource
    UserRoleMapper userRoleMapper;

    @Override
    public List<UserRole> getUserRole(int userId) {
        QueryWrapper<UserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userId);
        return userRoleMapper.selectList(queryWrapper);
    }

    @Override
    public void insertUserRole(int userId, List<Integer> list) {
        QueryWrapper<UserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        List<UserRole> userRoles = userRoleMapper.selectList(queryWrapper);
        if(userRoles!=null){
            userRoleMapper.delete(queryWrapper);
        }
        for (Integer integer : list) {
            UserRole userRole = new UserRole();
            userRole.setUserId(userId);
            userRole.setRoleId(integer);
            userRoleMapper.insert(userRole);
        }
    }
    @Override
    public void insertRoleUser(List<Integer> list,int roleId) {
        QueryWrapper<UserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_id", roleId);
        List<UserRole> userRoles = userRoleMapper.selectList(queryWrapper);
        if(userRoles!=null){
            userRoleMapper.delete(queryWrapper);
        }
        for (Integer integer : list) {
            UserRole userRole = new UserRole();
            userRole.setUserId(integer);
            userRole.setRoleId(roleId);
            userRoleMapper.insert(userRole);
        }
    }

    @Override
    public void deletRoleUser(List<Integer> list) {
        QueryWrapper<UserRole> userRoleQueryWrapper = new QueryWrapper<>();

        for (Integer integer : list) {
            userRoleQueryWrapper.eq("user_id",integer);
            userRoleMapper.delete(userRoleQueryWrapper);
        }

    }

    @Override
    public List<UserRole> selectUserRoleList(int roleId) {
        QueryWrapper<UserRole> userRoleQueryWrapper = new QueryWrapper<>();
        userRoleQueryWrapper.eq("role_id",roleId);
        return userRoleMapper.selectList(userRoleQueryWrapper);
    }

    @Override
    public void insert(UserRole userRole) {
        userRoleMapper.insert(userRole);
    }

}
