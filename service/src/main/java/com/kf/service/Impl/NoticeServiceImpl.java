package com.kf.service.Impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kf.config.AliyunOSSUtil;
import com.kf.mapper.NoticeMapper;
import com.kf.mapper.NoticefileMapper;
import com.kf.pojo.Notice;

import com.kf.pojo.Noticefile;
import com.kf.pojo.User;
import com.kf.pojo.form.AddNotice;
import com.kf.pojo.form.DeleteForm;

import com.kf.service.NoticeRepository;
import com.kf.service.NoticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 留言板信息表 服务实现类
 * </p>
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

    @Resource
    NoticeMapper noticeMapper;
    @Resource
    NoticeRepository noticeRepository;
    @Resource
    NoticefileMapper noticefileMapper;
    @Resource
    AliyunOSSUtil aliyunOSSUtil;
    @Override
    public int insertNotice(AddNotice addNotice, User user) {
        Notice notice = new Notice();
        notice.setUsername(user.getUsername());
        notice.setUserId(user.getUserId());
        notice.setCreateby(user.getName());
        notice.setNoticeTopic(addNotice.getNoticeTopic());
        notice.setNoticeContent(addNotice.getNoticeContent());
        notice.setTarget(addNotice.getTarget());
        long timeStamp = System.currentTimeMillis();
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd");
        String time1 = time.format(new Date(Long.parseLong(String.valueOf(timeStamp))));
        notice.setCreatetime(time1);
        long timeStamp2 = System.currentTimeMillis();
        SimpleDateFormat time2= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time3 = time2.format(new Date(Long.parseLong(String.valueOf(timeStamp))));
        notice.setFlagtime(time3);
        noticeMapper.insert(notice);
        QueryWrapper<Notice> noticeQueryWrapper = new QueryWrapper<>();
        noticeQueryWrapper.eq("user_id",user.getUserId())
                .eq("notice_topic",addNotice.getNoticeTopic())
                .eq("flagtime",time3);
        Notice notice1 = noticeMapper.selectOne(noticeQueryWrapper);

        noticeRepository.save(notice1);
        return notice1.getId();
    }

    @Override
    public Notice getNoticeById(String id) {
        Optional<Notice> notice = noticeRepository.findById(id);
        return notice.get();
    }

    @Override
    public void updateNotice(AddNotice addNotice,User user) {
        Optional<Notice> notice = noticeRepository.findById(addNotice.getId());
        Notice notice1 = notice.get();
        notice1.setUsername(user.getUsername());
        notice1.setUserId(user.getUserId());
        notice1.setCreateby(user.getName());
        long timeStamp = System.currentTimeMillis();
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd");
        String time1 = time.format(new Date(Long.parseLong(String.valueOf(timeStamp))));
        notice1.setCreatetime(time1);

        notice1.setNoticeTopic(addNotice.getNoticeTopic());
        notice1.setNoticeContent(addNotice.getNoticeContent());
        long timeStamp2 = System.currentTimeMillis();
        SimpleDateFormat time2= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time3 = time2.format(new Date(Long.parseLong(String.valueOf(timeStamp))));
        notice1.setFlagtime(time3);

        noticeRepository.save(notice1);
        noticeMapper.updateById(notice1);

    }

    @Override
    public void deleteNotice(DeleteForm deleteForm) {
        List<Integer> list = deleteForm.getList();

        for (Integer id : list) {
            QueryWrapper<Noticefile> noticefileQueryWrapper = new QueryWrapper<>();
            noticefileQueryWrapper.eq("notice_id",id);
            List<Noticefile> noticefiles = noticefileMapper.selectList(noticefileQueryWrapper);
            noticefiles.forEach(noticefile -> {
                aliyunOSSUtil.deleteFile(noticefile.getPath() + "/" + noticefile.getFilecode() + "." + noticefile.getSuffix());
                noticefileMapper.deleteById(noticefile);
            });

            noticeRepository.deleteById(String.valueOf(id));
            noticeMapper.deleteById(id);
        }

    }
}
