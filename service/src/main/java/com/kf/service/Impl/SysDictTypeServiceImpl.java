package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kf.config.SecurityUser;
import com.kf.handler.MyException;
import com.kf.mapper.SysDictDataMapper;
import com.kf.pojo.SysDictData;
import com.kf.pojo.SysDictType;
import com.kf.mapper.SysDictTypeMapper;
import com.kf.pojo.User;
import com.kf.pojo.form.AddSysDictType;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.SelectForm;
import com.kf.service.SysDictDataService;
import com.kf.service.SysDictTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.util.ResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.Resource;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

/**
 * <p>
 * 字典类型 服务实现类
 * </p>
 */
@Service
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements SysDictTypeService {
    @Resource
    SysDictTypeMapper sysDictTypeMapper;
    @Resource
    SysDictDataMapper sysDictDataMapper;
    @Override
    public IPage<SysDictType> selectPage(SelectForm selectForm) {
        Page<SysDictType> sysDictTypePage = new Page<>(selectForm.getCurrent(), selectForm.getSize());
        QueryWrapper<SysDictType> sysDictTypeQueryWrapper = new QueryWrapper<>();
        sysDictTypeQueryWrapper.like("dict_name",selectForm.getName());
        IPage<SysDictType> sysDictTypeIPage = sysDictTypeMapper.selectPage(sysDictTypePage, sysDictTypeQueryWrapper);
        return sysDictTypeIPage;
    }

    @Override
    public void delete(DeleteForm deleteForm) {
        List<Integer> list = deleteForm.getList();
        sysDictTypeMapper.deleteBatchIds(list);

    }

    @Override
    public void insertType(AddSysDictType addSysDictType) {
        User user = SecurityUser.getUser();
        SysDictType sysDictType = new SysDictType();
        sysDictType.setDictType(addSysDictType.getDictType());
        sysDictType.setDictName(addSysDictType.getDictName());
        sysDictType.setRemark(addSysDictType.getRemark());
        sysDictType.setSort(addSysDictType.getSort());
        sysDictType.setUsername(user.getUsername());
        sysDictTypeMapper.insert(sysDictType);

    }

    @Override
    public void updateType(AddSysDictType addSysDictType) {
        User user = SecurityUser.getUser();
        SysDictType sysDictType = sysDictTypeMapper.selectById(addSysDictType.getId());
        sysDictType.setDictType(addSysDictType.getDictType());
        sysDictType.setDictName(addSysDictType.getDictName());
        sysDictType.setRemark(addSysDictType.getRemark());
        sysDictType.setSort(addSysDictType.getSort());
        sysDictType.setUsername(user.getUsername());
        sysDictTypeMapper.updateById(sysDictType);
    }

    @Override
    public List<SysDictType> selectData() {
        List<SysDictType> sysDictTypes = sysDictTypeMapper.selectList(null);
        for (SysDictType sysDictType : sysDictTypes) {
            QueryWrapper<SysDictData> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("dict_type_id",sysDictType.getId());
            List<SysDictData> sysDictData = sysDictDataMapper.selectList(queryWrapper);
            sysDictType.setDataList(sysDictData);
        }
        return sysDictTypes;

    }
}
