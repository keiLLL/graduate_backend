package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kf.pojo.LogLogin;
import com.kf.mapper.LogLoginMapper;
import com.kf.service.LogLoginService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 登录日志 服务实现类
 * </p>
 */
@Service
public class LogLoginServiceImpl extends ServiceImpl<LogLoginMapper, LogLogin> implements LogLoginService {
    @Resource
    LogLoginMapper logLoginMapper;

    @Override
    public IPage<LogLogin> pageLogin(String name, int current, int size) {
        Page<LogLogin> logLoginPage = new Page<>(current, size);
        logLoginPage.setDesc("id");
        QueryWrapper<LogLogin> logLoginQueryWrapper = new QueryWrapper<>();
        logLoginQueryWrapper.like("login_username",name);
        return logLoginMapper.selectPage(logLoginPage, logLoginQueryWrapper);
    }

    @Override
    public LogLogin getByName(String username) {
        QueryWrapper<LogLogin> logLoginQueryWrapper = new QueryWrapper<>();
        logLoginQueryWrapper.eq("login_username",username);
        return logLoginMapper.selectOne(logLoginQueryWrapper);

    }
}
