package com.kf.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.mapper.StageMapper;
import com.kf.pojo.Stage;
import com.kf.service.StageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class StageServicelmpl extends ServiceImpl<StageMapper, Stage> implements StageService {
    @Resource
    StageMapper stageMapper;
    @Override
    public Stage findById(Integer stuId) {
        Stage stage = stageMapper.selectById(stuId);
        return stage;
    }
}
