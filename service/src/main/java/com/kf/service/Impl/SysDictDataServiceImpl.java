package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kf.config.SecurityUser;
import com.kf.pojo.SysDictData;
import com.kf.mapper.SysDictDataMapper;
import com.kf.pojo.User;
import com.kf.pojo.form.AddSysDicData;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.SelectDictData;
import com.kf.service.SysDictDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 字典数据 服务实现类
 * </p>
 */
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictData> implements SysDictDataService {

    @Resource
    SysDictDataMapper sysDictDataMapper;
    @Override
    public IPage<SysDictData> selecPage(SelectDictData selectForm) {
        Page<SysDictData> sysDictDataPage = new Page<>(selectForm.getCurrent(), selectForm.getSize());
        QueryWrapper<SysDictData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("dict_type_id",selectForm.getDictTypeId());
        queryWrapper.like("dict_label",selectForm.getName());

        return sysDictDataMapper.selectPage(sysDictDataPage, queryWrapper);
    }

    @Override
    public void delete(DeleteForm deleteForm) {
        List<Integer> list = deleteForm.getList();

        sysDictDataMapper.deleteBatchIds(list);

    }

    @Override
    public void insertData(AddSysDicData addSysDicData) {
        User user = SecurityUser.getUser();
        SysDictData sysDictData = new SysDictData();
        sysDictData.setDictLabel(addSysDicData.getDictLabel());
        sysDictData.setDictTypeId(addSysDicData.getDictTypeId());
        sysDictData.setRemark(addSysDicData.getRemark());
        sysDictData.setDictValue(addSysDicData.getDictValue());
        sysDictData.setSort(addSysDicData.getSort());
        sysDictData.setUsername(user.getUsername());
        sysDictDataMapper.insert(sysDictData);
    }

    @Override
    public void updateData(AddSysDicData addSysDicData) {
        User user = SecurityUser.getUser();
        SysDictData sysDictData = sysDictDataMapper.selectById(addSysDicData.getId());
        sysDictData.setDictLabel(addSysDicData.getDictLabel());
        sysDictData.setDictTypeId(addSysDicData.getDictTypeId());
        sysDictData.setRemark(addSysDicData.getRemark());
        sysDictData.setDictValue(addSysDicData.getDictValue());
        sysDictData.setSort(addSysDicData.getSort());
        sysDictData.setUsername(user.getUsername());
        sysDictDataMapper.updateById(sysDictData);
    }

}
