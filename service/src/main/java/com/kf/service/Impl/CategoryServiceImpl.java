package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kf.config.AliyunOSSUtil;

import com.kf.mapper.*;
import com.kf.pojo.*;
import com.kf.pojo.form.*;
import com.kf.service.CategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * <p>
 * 课题类别信息表 服务实现类
 * </p>
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Resource
    CategoryMapper categoryMapper;
    @Resource
    ChoCgyRecordMapper choCgyRecordMapper;
    @Resource
    ChoCgyMapper choCgyMapper;
    @Override
    public void insertCategory(Category category) {
        categoryMapper.insert(category);
    }

    @Override
    public IPage<Category> pageCategoryBycgyName(String username,String cgyName, int current, int size) {
        Page<Category> categoryPage = new Page<>(current,size);
        QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();
        categoryQueryWrapper.eq("tusername",username);
        categoryQueryWrapper.like("cgy_name",cgyName);
        categoryQueryWrapper.eq("cgy_source",1);
        return categoryMapper.selectPage(categoryPage, categoryQueryWrapper);
    }

    @Override
    public IPage<Category> pageCategoryByDuoCondi(SelectCgyForm selectCgyForm) {
        QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();

        Page<Category> categoryPage = new Page<>(selectCgyForm.getCurrent(),selectCgyForm.getSize());
        QueryWrapper<Category> queryWrapper = new QueryWrapper<>();
        if(selectCgyForm.getCgyName()!=null){
            queryWrapper.like("cgy_name",selectCgyForm.getCgyName());
        }
        if(selectCgyForm.getCgyDepart()!=null){
            queryWrapper.like("cgy_depart",selectCgyForm.getCgyDepart());
        }
        if(selectCgyForm.getTusername()!=null){
            queryWrapper.like("tusername",selectCgyForm.getTusername());
        }
        if(selectCgyForm.getCgySource()!=3){
            queryWrapper.like("cgy_source",selectCgyForm.getCgySource());
        }
        queryWrapper.eq("cgy_status",2);
        queryWrapper.eq("cgy_leadstatus",2);
        return categoryMapper.selectPage(categoryPage, queryWrapper);

    }

    @Override
    @Transactional(rollbackFor =Exception.class )
    public void insertCgyRecord(int cgyId,User student) {

        Category category = categoryMapper.selectById(cgyId);
        QueryWrapper<ChoCgyRecord> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("susername",student.getUsername());
        System.out.println("我进来了");
        ChoCgyRecord  choCgyRecord1=choCgyRecordMapper.selectOne(queryWrapper);

        System.out.println("是否有选择题目:");

        if(choCgyRecord1!=null){
            System.out.println("该同学已经选择课题，所以更新");
            choCgyRecord1.setCgyId(cgyId);
            choCgyRecord1.setUserId(student.getUserId());
            choCgyRecord1.setSusername(student.getUsername());
            choCgyRecord1.setStuName(student.getName());

            choCgyRecord1.setTusername(category.getTusername());
            choCgyRecord1.setTeaName(category.getTeaName());
            choCgyRecord1.setCgyName(category.getCgyName());
            choCgyRecord1.setCgySource(category.getCgySource());
            choCgyRecordMapper.updateById(choCgyRecord1);
        }else{
            System.out.println("该同学未选择课题，所以插入");
            ChoCgyRecord choCgyRecord2 = new ChoCgyRecord();
            choCgyRecord2.setCgyId(cgyId);
            choCgyRecord2.setUserId(student.getUserId());
            choCgyRecord2.setSusername(student.getUsername());
            choCgyRecord2.setStuName(student.getName());
            choCgyRecord2.setTusername(category.getTusername());
            choCgyRecord2.setTeaName(category.getTeaName());
            choCgyRecord2.setCgyName(category.getCgyName());
            choCgyRecord2.setCgySource(category.getCgySource());
            choCgyRecord2.setStatus(0);
            System.out.println("我已经到这儿了");
            System.out.println(choCgyRecord2);
            choCgyRecordMapper.insert(choCgyRecord2);


        }

    }

    @Override
    public Category selectByUserId(int userId) {
        QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();
        categoryQueryWrapper.eq("user_id",userId);
        return categoryMapper.selectOne(categoryQueryWrapper);
    }

    @Override
    public Category setCategory(AddCgyForm addCgyForm, User user,Category category) {
        category.setUserId(user.getUserId());
        category.setUsername(user.getUsername());
        category.setName(user.getName());
        category.setCgyName(addCgyForm.getCgyName());
        category.setCgyDepart(addCgyForm.getCgyDepart());
        category.setCgyType(addCgyForm.getCgyType());
        category.setCgySource(user.getState());
        String[] split = addCgyForm.getTusername().split(" ");
        category.setTusername(split[0]);
        category.setDescribes(addCgyForm.getDescribes());
        return category;
    }

    @Override
    public Category setCategoryUpdate(AddCgyForm addCgyForm, User user,Category category) {
        QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
        choCgyQueryWrapper.eq("cgy_id",category.getCgyId());
        List<ChoCgy> choCgies = choCgyMapper.selectList(choCgyQueryWrapper);
        choCgies.forEach(choCgy -> {

            choCgy.setCgyName(addCgyForm.getCgyName());
            choCgyMapper.updateById(choCgy);
        });

        QueryWrapper<ChoCgyRecord> choCgyRecordQueryWrapper = new QueryWrapper<>();
        choCgyRecordQueryWrapper.eq("cgy_id",category.getCgyId());
        List<ChoCgyRecord> choCgyRecords = choCgyRecordMapper.selectList(choCgyRecordQueryWrapper);
        choCgyRecords.forEach(choCgyRecord -> {
            choCgyRecord.setCgyName(addCgyForm.getCgyName());
            choCgyRecordMapper.updateById(choCgyRecord);
        });

        category.setUserId(user.getUserId());
        category.setUsername(user.getUsername());
        category.setName(user.getName());
        category.setCgyName(addCgyForm.getCgyName());
        category.setCgyDepart(addCgyForm.getCgyDepart());
        category.setCgyType(addCgyForm.getCgyType());
        category.setCgySource(user.getState());
        String[] split = addCgyForm.getTusername().split(" ");
        category.setTusername(split[0]);
        category.setDescribes(addCgyForm.getDescribes());
        return category;
    }
    @Override
    public Category setTeaCategory(TeaAddCgyForm addCgyForm, User user, Category category) {


        category.setUserId(user.getUserId());
        category.setUsername(user.getUsername());
        category.setName(user.getName());
        category.setCgyName(addCgyForm.getCgyName());
        category.setCgyDepart(user.getDepartment());
        category.setCgyType(addCgyForm.getCgyType());
        category.setCgySource(user.getState());
        category.setTusername(user.getUsername());
        category.setDescribes(addCgyForm.getDescribes());
        return category;
    }
    @Override
    public Category setTeaCategoryUpdate(TeaAddCgyForm addCgyForm, User user, Category category) {
        QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
        choCgyQueryWrapper.eq("cgy_id",category.getCgyId());
        List<ChoCgy> choCgies = choCgyMapper.selectList(choCgyQueryWrapper);
        choCgies.forEach(choCgy -> {
            choCgy.setCgyName(addCgyForm.getCgyName());
            choCgyMapper.updateById(choCgy);
        });
        QueryWrapper<ChoCgyRecord> choCgyRecordQueryWrapper = new QueryWrapper<>();
        choCgyRecordQueryWrapper.eq("cgy_id",category.getCgyId());
        List<ChoCgyRecord> choCgyRecords = choCgyRecordMapper.selectList(choCgyRecordQueryWrapper);
        choCgyRecords.forEach(choCgyRecord -> {
            choCgyRecord.setCgyName(addCgyForm.getCgyName());
            choCgyRecordMapper.updateById(choCgyRecord);
        });
        category.setUserId(user.getUserId());
        category.setUsername(user.getUsername());
        category.setName(user.getName());
        category.setCgyName(addCgyForm.getCgyName());
        category.setCgyDepart(user.getDepartment());
        category.setCgyType(addCgyForm.getCgyType());
        category.setCgySource(user.getState());
        category.setTusername(user.getUsername());
        category.setDescribes(addCgyForm.getDescribes());
        return category;
    }
    @Override
    public void delete(Category category) {
        categoryMapper.deleteById(category);
    }

    @Resource
    PaperFileMapper paperFileMapper;
    @Resource
    RecordsMapper recordsMapper;
    @Resource
    ReportMapper reportMapper;
    @Resource
    WenFileMapper wenFileMapper;
    @Resource
    AliyunOSSUtil aliyunOSSUtil;
    @Override
    public void deleteCgy(DeleteForm deleteForm,User user) {
        List<Integer> list = deleteForm.getList();
        for (Integer integer : list) {
            QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();
            categoryQueryWrapper.eq("cgy_id",integer);
            categoryMapper.delete(categoryQueryWrapper);
            QueryWrapper<ChoCgyRecord> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("cgy_id",integer);
            choCgyRecordMapper.delete(queryWrapper);
            QueryWrapper<ChoCgy> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("cgy_id",integer);
            List<ChoCgy> choCgies = choCgyMapper.selectList(queryWrapper1);
            choCgies.forEach(x->{
                QueryWrapper<PaperFile> paperFileQueryWrapper1 = new QueryWrapper<>();
                paperFileQueryWrapper1.eq("user_id",x.getUserId());
                PaperFile paperFile = paperFileMapper.selectOne(paperFileQueryWrapper1);
                if(paperFile!=null){
                    aliyunOSSUtil.deleteFile(paperFile.getPath() + "/" + paperFile.getFilename() + "." + paperFile.getSuffix());
                    QueryWrapper<PaperFile> paperFileQueryWrapper = new QueryWrapper<>();
                    paperFileQueryWrapper.eq("user_id",x.getUserId());
                    paperFileMapper.delete(paperFileQueryWrapper);
                }


                QueryWrapper<WenFile> wenFileQueryWrapper = new QueryWrapper<>();
                wenFileQueryWrapper.eq("user_id",x.getUserId());
                WenFile wenFile = wenFileMapper.selectOne(wenFileQueryWrapper);
                if(wenFile!=null){
                    aliyunOSSUtil.deleteFile(wenFile.getPath()+"/"+wenFile.getFilename()+"."+ wenFile.getSuffix());
                    QueryWrapper<WenFile> wenFileQueryWrapper1 = new QueryWrapper<>();
                    wenFileQueryWrapper1.eq("user_id",x.getUserId());
                    wenFileMapper.delete(wenFileQueryWrapper1);
                }



                QueryWrapper<Report> reportQueryWrapper = new QueryWrapper<>();
                reportQueryWrapper.eq("user_id",x.getUserId());
                reportMapper.delete(reportQueryWrapper);

                QueryWrapper<Records> recordsQueryWrapper = new QueryWrapper<>();
                recordsQueryWrapper.eq("user_id",x.getUserId());
                recordsMapper.delete(recordsQueryWrapper);

                QueryWrapper<Records> recordsQueryWrapper1 = new QueryWrapper<>();
                recordsQueryWrapper1.eq("re_target",x.getSusername());
                recordsMapper.delete(recordsQueryWrapper1);
            });


            choCgyMapper.delete(queryWrapper1);


        }
    }

    @Override
    public IPage<Category> teaPageMyCategory(SelectForm selectForm,User teacher) {
        Page<Category> categoryPage = new Page<>(selectForm.getCurrent(),selectForm.getSize());
        QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();
        categoryQueryWrapper.like("cgy_name",selectForm.getName());
        categoryQueryWrapper.eq("user_id",teacher.getUserId());
        return categoryMapper.selectPage(categoryPage, categoryQueryWrapper);
    }

    @Override
    public void batchCheckCategory(CheckList<Integer> checkList){

    }

    @Override
    public void batchLeaderCheck(CheckList<Integer> checkList) {
//        List<Integer> list = checkList.getList();
//        for (Integer id : list) {
//            Category category = getById(id);
//            if(category.getCgyStatus()==2){
//                category.setCgyLeadstatus(checkCgyForm.getCgyStatus());
//                category.setCgyLeadadvice(checkCgyForm.getCgyAdvise());
//                categoryService.updateById(category);
//                if(checkCgyForm.getCgyStatus()==2&&category.getCgySource()==1){
//                    choCgyRecordService.insertChoCgyRecord(category);
//                }
//        }
//    }

    }

    @Override
    public void deleteAllCgy(int cgyId) {
        QueryWrapper<ChoCgyRecord> choCgyRecordQueryWrapper = new QueryWrapper<>();
        choCgyRecordQueryWrapper.eq("cgy_id",cgyId);
        choCgyRecordMapper.delete(choCgyRecordQueryWrapper);
        QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
        choCgyQueryWrapper.eq("cgy_id",cgyId);
        List<ChoCgy> choCgies = choCgyMapper.selectList(choCgyQueryWrapper);
        choCgies.forEach(x->{
            QueryWrapper<PaperFile> paperFileQueryWrapper1 = new QueryWrapper<>();
            paperFileQueryWrapper1.eq("user_id",x.getUserId());
            PaperFile paperFile = paperFileMapper.selectOne(paperFileQueryWrapper1);
            if(paperFile!=null){
                aliyunOSSUtil.deleteFile(paperFile.getPath() + "/" + paperFile.getFilename() + "." + paperFile.getSuffix());
                QueryWrapper<PaperFile> paperFileQueryWrapper = new QueryWrapper<>();
                paperFileQueryWrapper.eq("user_id",x.getUserId());
                paperFileMapper.delete(paperFileQueryWrapper);
            }



            QueryWrapper<WenFile> wenFileQueryWrapper = new QueryWrapper<>();
            wenFileQueryWrapper.eq("user_id",x.getUserId());
            WenFile wenFile = wenFileMapper.selectOne(wenFileQueryWrapper);
            if(wenFile!=null){
                aliyunOSSUtil.deleteFile(wenFile.getPath()+"/"+wenFile.getFilename()+"."+ wenFile.getSuffix());
                QueryWrapper<WenFile> wenFileQueryWrapper1 = new QueryWrapper<>();
                wenFileQueryWrapper1.eq("user_id",x.getUserId());
                wenFileMapper.delete(wenFileQueryWrapper1);

            }


            QueryWrapper<Report> reportQueryWrapper = new QueryWrapper<>();
            reportQueryWrapper.eq("user_id",x.getUserId());
            reportMapper.delete(reportQueryWrapper);

            QueryWrapper<Records> recordsQueryWrapper = new QueryWrapper<>();
            recordsQueryWrapper.eq("user_id",x.getUserId());
            recordsMapper.delete(recordsQueryWrapper);

            QueryWrapper<Records> recordsQueryWrapper1 = new QueryWrapper<>();
            recordsQueryWrapper1.eq("re_target",x.getSusername());
            recordsMapper.delete(recordsQueryWrapper1);
        });

        choCgyMapper.delete(choCgyQueryWrapper);
    }

    @Override
    public Category selectByCgyName(String cgyName) {
        QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();
        categoryQueryWrapper.eq("cgy_name",cgyName);
        return categoryMapper.selectOne(categoryQueryWrapper);
    }

}
