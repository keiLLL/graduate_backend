package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.config.RedisUtil;
import com.kf.mapper.RoleMapper;
import com.kf.mapper.RolePermsMapper;
import com.kf.mapper.UserRoleMapper;
import com.kf.pojo.Role;
import com.kf.pojo.RolePerms;
import com.kf.pojo.UserRole;
import com.kf.pojo.form.PageFormRole;
import com.kf.pojo.form.SelectForm;
import com.kf.service.RoleService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Resource
    private RolePermsMapper rolePermsMapper;

    @Resource
    private UserRoleMapper userRoleMapper;
    @Resource
    private RoleMapper roleMapper;

    @Resource
    RedisUtil redisUtil;
    @Override
    public Role getRole(int roleId){
        QueryWrapper<Role> roleQueryWrapper = new QueryWrapper<>();
        roleQueryWrapper.eq("id",roleId);
        return roleMapper.selectOne(roleQueryWrapper);
    }
    @Override
    public List<Role> findAll(){
        QueryWrapper<Role> roleQueryWrapper = new QueryWrapper<>();
        roleQueryWrapper.select("id","name","remark");
        return roleMapper.selectList(roleQueryWrapper);
    }

    @Override
    public int addRole(Role role) {
        return roleMapper.insert(role);
    }

    @Override
    public IPage<Role> pageRole(String name,int current, int size) {
        Page<Role> roles = new Page<>(current,size);
        roles.setDesc("id");
        QueryWrapper<Role> roleQueryWrapper = new QueryWrapper<>();
        roleQueryWrapper.like("name",name);

        return roleMapper.selectPage(roles,roleQueryWrapper);
    }

    @Override
    public List<Role> selectRoleByName(String roleName) {
        return roleMapper.selectRoleByName(roleName);
    }

    /**
     * 删除角色
     *
     * @param roleIdList 角色id列表
     */
    @Override
    public void deleteRole(List<Integer> roleIdList) {
        roleMapper.deleteBatchIds(roleIdList);
        QueryWrapper<RolePerms> queryWrapper = new QueryWrapper<>();
        QueryWrapper<UserRole> userRoleQueryWrapper = new QueryWrapper<>();
        for (Integer roleId : roleIdList) {
            queryWrapper.eq("role_id",roleId);
            rolePermsMapper.delete(queryWrapper);
            userRoleQueryWrapper.eq("role_id",roleId);
            userRoleMapper.delete(userRoleQueryWrapper);
        }

    }

    @Override
    public Role findRoleUser(int roleId) {
        return roleMapper.findRoleUser(roleId);
    }

    @Override
    public PageFormRole redisGetRole(SelectForm selectForm, String pagekeys) throws InterruptedException {
        PageFormRole pageFormRole = new PageFormRole();
        ReentrantLock reentrantLock = new ReentrantLock();
        if(reentrantLock.tryLock()){
            IPage<Role> roleIPage = pageRole(selectForm.getName(), selectForm.getCurrent(), selectForm.getSize());
            List<Role> records = roleIPage.getRecords();
            pageFormRole.setList(records);
            pageFormRole.setTotal(roleIPage.getTotal());
            redisUtil.set(pagekeys,pageFormRole);
            reentrantLock.unlock();
            return pageFormRole;
        }else {
            Thread.sleep(100);
            return redisGetRole(selectForm, pagekeys);
        }
    }

}
