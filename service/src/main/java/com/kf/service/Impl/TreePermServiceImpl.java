package com.kf.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.mapper.TreePermMapper;
import com.kf.pojo.TreePerm;
import com.kf.service.TreePermService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class TreePermServiceImpl extends ServiceImpl<TreePermMapper, TreePerm> implements TreePermService {


}
