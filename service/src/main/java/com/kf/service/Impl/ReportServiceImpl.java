package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.handler.MyException;
import com.kf.mapper.ChoCgyRecordMapper;
import com.kf.mapper.ReportMapper;
import com.kf.pojo.ChoCgyRecord;
import com.kf.pojo.Report;
import com.kf.pojo.User;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.ReportForm;
import com.kf.pojo.form.SelectReportForm;
import com.kf.service.ReportService;
import com.kf.util.ResultCode;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class ReportServiceImpl extends ServiceImpl<ReportMapper, Report> implements ReportService {

    @Resource
    ReportMapper reportMapper;
    @Resource
    ChoCgyRecordMapper choCgyRecordMapper;
    @Override
    public void insert(Report report, User student, ReportForm reportForm){
        QueryWrapper<Report> reportQueryWrapper = new QueryWrapper<>();
        reportQueryWrapper.eq("susername",student.getUsername());
        Report report1 = reportMapper.selectOne(reportQueryWrapper);
        if(report1!=null){
            report1.setUserId(student.getUserId());
            report1.setSusername(student.getUsername());
            report1.setCreateby(student.getName());
            report1.setPlan(reportForm.getPlan());
            report1.setProspect(reportForm.getProspect());
            report1.setRoute(reportForm.getRoute());
            report.setStatus(0);
            QueryWrapper<ChoCgyRecord> choCgyQueryWrapper = new QueryWrapper<>();
            choCgyQueryWrapper.eq("susername",student.getUsername());

            ChoCgyRecord choCgyRecord = choCgyRecordMapper.selectOne(choCgyQueryWrapper);

            if(choCgyRecord.getStatus()!=2){
                System.out.println("我的指导老师为空");
                throw new MyException(ResultCode.STUDENT_CHOOSE_ERROR3);
            }
            report1.setRepTeacher(choCgyRecord.getTusername());
            reportMapper.updateById(report1);
        }else{
            report.setUserId(student.getUserId());

            report.setSusername(student.getUsername());
            report.setCreateby(student.getName());
            report.setPlan(reportForm.getPlan());
            report.setProspect(reportForm.getProspect());
            report.setRoute(reportForm.getRoute());
            report.setStatus(0);
            QueryWrapper<ChoCgyRecord> choCgyQueryWrapper = new QueryWrapper<>();
            choCgyQueryWrapper.eq("susername",student.getUsername());

            ChoCgyRecord choCgyRecord = choCgyRecordMapper.selectOne(choCgyQueryWrapper);

            if(choCgyRecord.getStatus()!=2){
                System.out.println("我的指导老师为空");
                throw new MyException(ResultCode.STUDENT_CHOOSE_ERROR3);
            }
            report.setRepTeacher(choCgyRecord.getTusername());
            reportMapper.insert(report);
        }

    }

    @Override
    public void delete(String username) {
        QueryWrapper<Report> reportQueryWrapper = new QueryWrapper<>();
        reportQueryWrapper.eq("susername",username);
       reportMapper.delete(reportQueryWrapper);
    }

    @Override
    public IPage<Report> page(SelectReportForm selectForm, User teacher) {
        Page<Report> reportPage = new Page<>(selectForm.getCurrent(),selectForm.getSize());
        QueryWrapper<Report> reportQueryWrapper = new QueryWrapper<>();
        if(selectForm.getStatus()!=3){
            reportQueryWrapper.eq("status",selectForm.getStatus());
        }
        reportQueryWrapper.eq("rep_teacher",teacher.getUsername());
        reportQueryWrapper.like("susername",selectForm.getName()).or().like("createby",selectForm.getName());
        reportQueryWrapper.eq("deleted",0);
        return reportMapper.selectPage(reportPage, reportQueryWrapper);
    }

    @Override
    public void deleteReport(DeleteForm deleteForm) {
        List<Integer> list = deleteForm.getList();
        for (Integer integer : list) {
            reportMapper.deleteById(integer);
        }
    }

    @Override
    public Report selectByUsername(String username) {
        QueryWrapper<Report> reportQueryWrapper = new QueryWrapper<>();
        reportQueryWrapper.eq("susername",username);
        return reportMapper.selectOne(reportQueryWrapper);
    }

}
