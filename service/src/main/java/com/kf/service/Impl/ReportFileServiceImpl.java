package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kf.config.AliyunOSSUtil;
import com.kf.handler.MyException;
import com.kf.mapper.ChoCgyMapper;
import com.kf.pojo.*;
import com.kf.mapper.ReportFileMapper;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.ReportVo;
import com.kf.pojo.form.SelectReportForm;
import com.kf.service.ReportFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.util.ResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ResourceBundle;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class ReportFileServiceImpl extends ServiceImpl<ReportFileMapper, ReportFile> implements ReportFileService {

    @Autowired
    ReportFileMapper reportFileMapper;
    @Autowired
    AliyunOSSUtil aliyunOSSUtil;
    @Autowired
    ChoCgyMapper choCgyMapper;
    @Override
    public ReportFile getByUserId(int id) {
        QueryWrapper<ReportFile> reportFileQueryWrapper = new QueryWrapper<>();
        reportFileQueryWrapper.eq("user_id",id);
        return reportFileMapper.selectOne(reportFileQueryWrapper);
    }

    @Override
    public void insert(ReportFile reportFile, User student, String[] split,String fileCode) {

        ResourceBundle resource = ResourceBundle.getBundle("oss");
        String reporturl = resource.getString("reporturl");
        QueryWrapper<ReportFile> reportFileQueryWrapper = new QueryWrapper<>();
        reportFileQueryWrapper.eq("user_id",student.getUserId());
        ReportFile reportFile1 = reportFileMapper.selectOne(reportFileQueryWrapper);
        if(reportFile1!=null){
            String filePath=reportFile1.getPath()+"/"+reportFile1.getFilecode()+"."+ reportFile1.getSuffix();
            aliyunOSSUtil.deleteFile(filePath);
            reportFile1.setUserId(student.getUserId());

            reportFile1.setFilename(split[0]);
            reportFile1.setSuffix(split[1]);
            reportFile1.setCreateby(student.getName());
            reportFile1.setPath(reporturl);
            reportFile1.setFilecode(fileCode);
            QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
            choCgyQueryWrapper.eq("susername",student.getUsername());
            ChoCgy choCgy = choCgyMapper.selectOne(choCgyQueryWrapper);
            if(choCgy==null){
                System.out.println("我的指导老师为空");
                throw new MyException(ResultCode.STUDENT_CHOOSE_ERROR3);
            }

            reportFileMapper.updateById(reportFile1);


        }else{
            reportFile.setFilename(split[0]);
            reportFile.setUserId(student.getUserId());
            reportFile.setPath(reporturl);
            reportFile.setSuffix(split[1]);
            reportFile.setCreateby(student.getName());
            reportFile.setFilecode(fileCode);
            reportFile.setSusername(student.getUsername());
            QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
            choCgyQueryWrapper.eq("susername",student.getUsername());
            ChoCgy choCgy = choCgyMapper.selectOne(choCgyQueryWrapper);
            if(choCgy==null){
                System.out.println("我的指导老师为空");
                throw new MyException(ResultCode.STUDENT_CHOOSE_ERROR3);
            }
            reportFile.setTusername(choCgy.getTusername());

            reportFile.setCreateby(student.getName());

            reportFileMapper.insert(reportFile);
        }


    }

    @Override
    public void delete(int userId) {
        ReportFile reportFile = getByUserId(userId);
        aliyunOSSUtil.deleteFile(reportFile.getPath()+"/"+reportFile.getFilecode()+"."+ reportFile.getSuffix());
        QueryWrapper<ReportFile> reportFileQueryWrapper = new QueryWrapper<>();
        reportFileQueryWrapper.eq("user_id",userId);
        reportFileMapper.delete(reportFileQueryWrapper);
    }

    @Override
    public IPage<ReportFile> pageReport(User user, SelectReportForm selectReportForm) {
        Page<ReportFile> reportFilePage = new Page<>(selectReportForm.getCurrent(), selectReportForm.getSize());
        QueryWrapper<ReportFile> reportFileQueryWrapper = new QueryWrapper<>();

        reportFileQueryWrapper.eq("tusername",user.getUsername());
        if(selectReportForm.getStatus()!=3){
            reportFileQueryWrapper.eq("status",selectReportForm.getStatus());
        }
        reportFileQueryWrapper.like("susername",selectReportForm.getName());
        return reportFileMapper.selectPage(reportFilePage,reportFileQueryWrapper);
    }

    @Override
    public void deletebyId(DeleteForm deleteForm) {
        List<Integer> list = deleteForm.getList();
        for (Integer id : list) {
            ReportFile reportFile = reportFileMapper.selectById(id);
            boolean b = aliyunOSSUtil.deleteFile(reportFile.getPath() + "/" + reportFile.getFilecode() + "." + reportFile.getSuffix());
            if(!b){
                throw new MyException(ResultCode.USER_DELETE_ERRPR);
            }
            reportFileMapper.deleteById(id);

        }
    }
}
