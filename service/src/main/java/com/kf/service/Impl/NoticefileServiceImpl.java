package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kf.config.AliyunOSSUtil;

import com.kf.pojo.Noticefile;
import com.kf.mapper.NoticefileMapper;

import com.kf.pojo.User;
import com.kf.service.NoticefileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.ResourceBundle;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class NoticefileServiceImpl extends ServiceImpl<NoticefileMapper, Noticefile> implements NoticefileService {

    @Resource
    NoticefileMapper noticefileMapper;
    @Resource
    AliyunOSSUtil aliyunOSSUtil;
    @Override
    public void insertNoticeFile(Noticefile noticefile, User user, String[] split,int noticeId,String fileCode) {
        ResourceBundle resource = ResourceBundle.getBundle("oss");
        String noticeurl = resource.getString("noticeurl");
        noticefile.setFilename(split[0]);
        noticefile.setPath(noticeurl);
        noticefile.setSuffix(split[1]);
        noticefile.setCreateby(user.getName());
        noticefile.setUserId(user.getUserId());
        noticefile.setUsername(user.getUsername());
        noticefile.setNoticeId(noticeId);
        noticefile.setFilecode(fileCode);
        noticefileMapper.insert(noticefile);

    }
    @Override
    public void updateNoticeFile(Noticefile noticefile, User user, String[] split,int noticeId,String fileCode) {


        ResourceBundle resource = ResourceBundle.getBundle("oss");
        String noticeurl = resource.getString("noticeurl");
        QueryWrapper<Noticefile> noticefileQueryWrapper = new QueryWrapper<>();
        noticefileQueryWrapper.eq("notice_id",noticeId);
        List<Noticefile> noticefiles = noticefileMapper.selectList(noticefileQueryWrapper);
        boolean flag=false;
        for (Noticefile noticefile1 : noticefiles) {
            if(noticefile1.getFilename().equals(split[0])){
                flag=true;
                noticefile1.setFilename(split[0]);
                noticefile1.setPath(noticeurl);
                noticefile1.setSuffix(split[1]);
                noticefile1.setCreateby(user.getName());
                noticefile1.setUserId(user.getUserId());
                noticefile1.setUsername(user.getUsername());
                noticefile1.setNoticeId(noticeId);
                noticefile1.setFilecode(fileCode);
                noticefileMapper.updateById(noticefile1);
            }
        }
        if(!flag){
            noticefile.setFilename(split[0]);
            noticefile.setPath(noticeurl);
            noticefile.setSuffix(split[1]);
            noticefile.setCreateby(user.getName());
            noticefile.setUserId(user.getUserId());
            noticefile.setUsername(user.getUsername());
            noticefile.setNoticeId(noticeId);
            noticefile.setFilecode(fileCode);
            noticefileMapper.insert(noticefile);
        }
    }
    @Override
    public void deleteFileByNoticeId(int noticeid) {
        QueryWrapper<Noticefile> noticefileQueryWrapper = new QueryWrapper<>();
        noticefileQueryWrapper.eq("notice_id", noticeid);
        List<Noticefile> noticefiles = noticefileMapper.selectList(noticefileQueryWrapper);
        noticefiles.forEach(noticefile -> {
            aliyunOSSUtil.deleteFile(noticefile.getPath() + "/" + noticefile.getFilecode() + "." + noticefile.getSuffix());
            noticefileMapper.deleteById(noticefile);
      }
        );

    }

    @Override
    public void deleteFile(int id) {

        Noticefile noticefile = noticefileMapper.selectById(id);
        aliyunOSSUtil.deleteFile(noticefile.getPath() + "/" + noticefile.getFilecode() + "." + noticefile.getSuffix());
        noticefileMapper.deleteById(id);
    }
}
