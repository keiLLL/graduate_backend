package com.kf.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.mapper.FileMapper;
import com.kf.pojo.GradFile;
import com.kf.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 文件表 服务实现类
 * </p>
 */
@Service
public class FileServiceImpl extends ServiceImpl<FileMapper, GradFile> implements FileService {

    @Resource
    FileMapper fileMapper;

    @Override
    public void insertRecords(String name, String suffix, String path,String createBy) {
        GradFile file = new GradFile(name,suffix,path,createBy);
        fileMapper.insert(file);

    }

    @Override
    public void insert(GradFile gradFile) {
        fileMapper.insert(gradFile);
    }
}
