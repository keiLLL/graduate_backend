package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kf.config.AliyunOSSUtil;
import com.kf.handler.MyException;
import com.kf.mapper.ChoCgyMapper;
import com.kf.pojo.ChoCgy;
import com.kf.pojo.PaperFile;
import com.kf.mapper.PaperFileMapper;
import com.kf.pojo.User;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.SelectForm;
import com.kf.service.PaperFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.kf.util.ResultCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.ResourceBundle;

/**
 * <p>
 *  服务实现类
 * </p>
 */

@Service
public class PaperFileServiceImpl extends ServiceImpl<PaperFileMapper, PaperFile> implements PaperFileService {
    @Resource
    PaperFileMapper paperFileMapper;
    @Resource
    ChoCgyMapper choCgyMapper;
    @Resource
    AliyunOSSUtil aliyunOSSUtil;
    @Override
    public void insertPaper(PaperFile paperFile, User student, String[] split,String fileCode) {
        ResourceBundle resource = ResourceBundle.getBundle("oss");
        String paperurl = resource.getString("paperurl");
        QueryWrapper<PaperFile> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("susername",student.getUsername());
        PaperFile paperFile1 = paperFileMapper.selectOne(queryWrapper);
        if(paperFile1!=null){
            String filePath=paperFile1.getPath()+"/"+paperFile1.getFilecode()+"."+ paperFile1.getSuffix();
            aliyunOSSUtil.deleteFile(filePath);
            paperFile1.setFilename(split[0]);
            paperFile1.setSuffix(split[1]);
            paperFile1.setPath(paperurl);
            paperFile1.setCreateby(student.getName());
            paperFile1.setFilecode(fileCode);
            //如果指导老师变了，那么wenfile表里面还是有这个学生的上传记录，这时候也要更新指导老师所以还是要
            //从cho_cgy里面拿
            QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
            choCgyQueryWrapper.eq("susername",student.getUsername());
            ChoCgy choCgy = choCgyMapper.selectOne(choCgyQueryWrapper);
            if(choCgy==null){
                throw new MyException(ResultCode.STUDENT_CHOOSE_ERROR3);
            }
            System.out.println("==================>选课记录不为空");
            paperFile1.setTusername(choCgy.getTusername());
            paperFileMapper.updateById(paperFile1);
        }else{
            System.out.println("==================>选课记录为空");

            paperFile.setFilename(split[0]);
            paperFile.setCreateby(student.getName());

            paperFile.setPath(paperurl);
            paperFile.setSuffix(split[1]);
            paperFile.setFilecode(fileCode);
            QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
            choCgyQueryWrapper.eq("susername",student.getUsername());

            ChoCgy choCgy = choCgyMapper.selectOne(choCgyQueryWrapper);

            if(choCgy==null){
                System.out.println("我的指导老师为空");
                throw new MyException(ResultCode.STUDENT_CHOOSE_ERROR3);
            }
            paperFile.setCreateby(student.getName());
            paperFile.setUserId(student.getUserId());
            paperFile.setSusername(student.getUsername());
            paperFile.setTusername(choCgy.getTusername());
            paperFileMapper.insert(paperFile);
        }

    }

    @Override
    public PaperFile selectByUsername(String username) {
        QueryWrapper<PaperFile> paperFileQueryWrapper = new QueryWrapper<>();
        paperFileQueryWrapper.eq("susername",username);
        return paperFileMapper.selectOne(paperFileQueryWrapper);
    }

    @Override
    public void  deletePaper(String username) {
        PaperFile paperFile = selectByUsername(username);
        boolean b = aliyunOSSUtil.deleteFile(paperFile.getPath() + "/" + paperFile.getFilecode() + "." + paperFile.getSuffix());
        if(!b){
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
        QueryWrapper<PaperFile> paperFileQueryWrapper = new QueryWrapper<>();
        paperFileQueryWrapper.eq("susername",username);
       paperFileMapper.delete(paperFileQueryWrapper);

    }

    @Override
    public IPage<PaperFile> page(SelectForm selectForm,User user) {
        Page<PaperFile> paperFilePage = new Page<>(selectForm.getCurrent(), selectForm.getSize());
        QueryWrapper<PaperFile> paperFileQueryWrapper = new QueryWrapper<>();
        paperFileQueryWrapper.eq("tusername",user.getUsername());
        paperFileQueryWrapper.like("susername",selectForm.getName());
        return paperFileMapper.selectPage(paperFilePage, paperFileQueryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deletePaperTea(DeleteForm deleteForm) {
        List<Integer> list = deleteForm.getList();
        for (Integer id : list) {
            PaperFile paperFile = paperFileMapper.selectById(id);
            boolean b = aliyunOSSUtil.deleteFile(paperFile.getPath() + "/" + paperFile.getFilename() + "." + paperFile.getSuffix());
            if(!b){
                throw new MyException(ResultCode.USER_DELETE_ERRPR);
            }
            paperFileMapper.deleteById(id);

        }

    }
}
