package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kf.handler.MyException;
import com.kf.mapper.ChoCgyMapper;
import com.kf.mapper.ChoCgyRecordMapper;
import com.kf.pojo.ChoCgy;
import com.kf.pojo.Defence;
import com.kf.mapper.DefenceMapper;
import com.kf.pojo.User;
import com.kf.pojo.form.AddDefence;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.SelectForm;
import com.kf.service.DefenceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.util.ResultCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class DefenceServiceImpl extends ServiceImpl<DefenceMapper, Defence> implements DefenceService {

    @Autowired
    DefenceMapper defenceMapper;
    @Autowired
    ChoCgyRecordMapper choCgyRecordMapper;
    @Autowired
    ChoCgyMapper choCgyMapper;
    @Override
    public IPage<Defence> getDefenceTea(SelectForm selectForm,String tusername) {
        Page<Defence> defencePage = new Page<>(selectForm.getCurrent(), selectForm.getSize());

        QueryWrapper<Defence> defenceQueryWrapper = new QueryWrapper<>();
        defenceQueryWrapper.eq("tusername1",tusername).eq("deleted",0).or()
                .eq("tusername2",tusername).eq("deleted",0).or().eq("tusername3",tusername).eq("deleted",0)
                .or().eq("tusername4",tusername).eq("deleted",0).or().eq("tusername",tusername).eq("deleted",0);



        if(!"".equals(selectForm.getName())){
            defenceQueryWrapper.like("tea_name",selectForm.getName()).eq("deleted",0).or().like("teaName1",selectForm.getName()).eq("deleted",0)
                    .or().like("teaName2",selectForm.getName()).eq("deleted",0)
                    .eq("deleted",0).or().like("teaName2",selectForm.getName())
                    .eq("deleted",0).or().like("teaName3",selectForm.getName()).eq("deleted",0).or()
                    .like("teaName4",selectForm.getName()).eq("deleted",0);
        }

        return defenceMapper.selectPage(defencePage, defenceQueryWrapper);

    }

    @Override
    public Defence getDefenceStu(User user) {

        QueryWrapper<Defence> defenceQueryWrapper = new QueryWrapper<>();
        QueryWrapper<ChoCgy> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("susername",user.getUsername());
        ChoCgy choCgy = choCgyMapper.selectOne(queryWrapper);
        defenceQueryWrapper.eq("tusername",choCgy.getTusername());
        Defence defence = defenceMapper.selectOne(defenceQueryWrapper);
        return defence;

    }

    public String[] split(String name){
        return name.split(" ");

    }
    @Override
    public void insertDefence(AddDefence addDefence,User user) {
        Defence defence = new Defence();

        QueryWrapper<Defence> defenceQueryWrapper = new QueryWrapper<>();
        String[] split = split(addDefence.getTeaName());
        defenceQueryWrapper.eq("tusername",split[0]);
        Defence defence1 = defenceMapper.selectOne(defenceQueryWrapper);
        if(defence1!=null){
            throw  new MyException(ResultCode.DEFENCE_TEACHER_ERROR);
        }
            defence.setDepartment(user.getDepartment());
            defence.setTusername(split[0]);
            defence.setTeaName(split[1]);
            String[] split1 = split(addDefence.getTeaname1());
            defence.setTusername1(split1[0]);
            defence.setTeaname1(split1[1]);
            String[] split2 = split(addDefence.getTeaname2());
            defence.setTusername2(split2[0]);
            defence.setTeaname2(split2[1]);
            String[] split3 = split(addDefence.getTeaname3());
            defence.setTusername3(split3[0]);
            defence.setTeaname3(split3[1]);
            String[] split4 = split(addDefence.getTeaname4());
            defence.setTusername4(split4[0]);
            defence.setTeaname4(split4[1]);
            defence.setPlace(addDefence.getPlace());
            defence.setStarttime(addDefence.getStartTime());
            defenceMapper.insert(defence);


    }

    @Override
    public void updateDefence(AddDefence addDefence) {
        String[] split = split(addDefence.getTeaName());
        // 原先答辩安排
        Defence defence = defenceMapper.selectById(addDefence.getId());
        // 指导老师不能修改
        if(split.length > 1) {
            defence.setTusername(split[0]);
            defence.setTeaName(split[1]);
        }

        String[] split1 = split(addDefence.getTeaname1());
        // 需要更新
        if(split1.length > 1) {
            defence.setTusername1(split1[0]);
            defence.setTeaname1(split1[1]);
        }

        String[] split2 = split(addDefence.getTeaname2());
        if(split2.length > 1) {
            defence.setTusername2(split2[0]);
            defence.setTeaname2(split2[1]);
        }
        String[] split3 = split(addDefence.getTeaname3());
        if(split3.length > 1) {
            defence.setTusername3(split3[0]);
            defence.setTeaname3(split3[1]);
        }
        String[] split4 = split(addDefence.getTeaname4());
        if(split4.length > 1) {
            defence.setTusername4(split4[0]);
            defence.setTeaname4(split4[1]);
        }

        defence.setPlace(addDefence.getPlace());
        defence.setStarttime(addDefence.getStartTime());

        // 判断教师是否重复
        List<String> teacherList = new ArrayList<>();
        teacherList.add(defence.getTeaName());
        teacherList.add(defence.getTeaname1());
        teacherList.add(defence.getTeaname2());
        teacherList.add(defence.getTeaname3());
        teacherList.add(defence.getTeaname4());
        for(int i = 0;i < teacherList.size();i ++) {
            for(int j = i+1;j < teacherList.size();j ++) {
                if(teacherList.get(i).equals(teacherList.get(j))) {
                    // 重复
                    throw new MyException(ResultCode.DEFENCE_TEACHER_ERROR);
                }
            }
        }

        defenceMapper.updateById(defence);
        System.out.println("========更新成功======");

    }

    @Override
    public void delete(DeleteForm deleteForm) {
        List<Integer> list = deleteForm.getList();
        for (Integer id : list) {
            defenceMapper.deleteById(id);
        }
    }

    @Override
    public IPage<Defence> select(SelectForm selectForm, String department) {

        Page<Defence> defencePage = new Page<>(selectForm.getCurrent(), selectForm.getSize());
        QueryWrapper<Defence> defenceQueryWrapper = new QueryWrapper<>();
        defenceQueryWrapper.eq("department",department);
        // 判空串
        if(!"".equals(selectForm.getName())  && selectForm.getName() != null){
            defenceQueryWrapper.like("tea_name",selectForm.getName()).eq("deleted",0).or().like("teaName1",selectForm.getName()).eq("deleted",0)
                    .or().like("teaName2",selectForm.getName()).eq("deleted",0)
                    .eq("deleted",0).or().like("teaName2",selectForm.getName())
                    .eq("deleted",0).or().like("teaName3",selectForm.getName()).eq("deleted",0).or()
                    .like("teaName4",selectForm.getName()).eq("deleted",0);
        }
        return defenceMapper.selectPage(defencePage, defenceQueryWrapper);
    }
}
