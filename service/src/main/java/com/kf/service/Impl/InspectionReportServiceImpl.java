package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.config.AliyunOSSUtil;
import com.kf.mapper.InspectionReportMapper;
import com.kf.pojo.InspectionReport;
import com.kf.service.InspectionReportService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

@Service
public class InspectionReportServiceImpl extends ServiceImpl<InspectionReportMapper, InspectionReport> implements InspectionReportService {

    @Resource
    AliyunOSSUtil aliyunOSSUtil;

    @Resource
    InspectionReportMapper inspectionReportMapper;

    @Override
    public int getCount(InspectionReport inspectionReport) {
        String susername = inspectionReport.getSusername();
        Boolean stage = inspectionReport.getStage();
        LambdaQueryWrapper<InspectionReport> lqw = new LambdaQueryWrapper<>();
        lqw.eq(InspectionReport::getSusername, susername).eq(InspectionReport::getStage, stage);
        return inspectionReportMapper.selectCount(lqw);
    }

    @Override
    public void downInspectionReport(Integer stage, String susername, HttpServletResponse response) throws IOException {
        // 获取文件地址
        LambdaQueryWrapper<InspectionReport> lqw = new LambdaQueryWrapper<>();
        lqw.eq(InspectionReport::getSusername, susername).eq(InspectionReport::getStage, stage);
        InspectionReport inspectionReport = inspectionReportMapper.selectOne(lqw);
        String path = inspectionReport.getPath();
        String filename = inspectionReport.getFilename();
        // 确保不同客户端能正常显示文件名
        response.addHeader("Content-Disposition", "attachment;filename=" +new String((filename).getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1));
        aliyunOSSUtil.exportOssFile(response.getOutputStream(),path);
    }

    @Override
    public void updateStatus(String susername) {
        inspectionReportMapper.updateStatus(susername);
    }
}
