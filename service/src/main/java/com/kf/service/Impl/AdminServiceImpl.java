package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.mapper.AdminMapper;
import com.kf.mapper.NoticeMapper;
import com.kf.mapper.NoticefileMapper;
import com.kf.pojo.Admin;
import com.kf.pojo.Notice;
import com.kf.pojo.Noticefile;
import com.kf.pojo.User;
import com.kf.pojo.form.AdminForm;
import com.kf.service.AdminService;
import com.kf.service.UserService;
import com.kf.util.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 管理员表 服务实现类
 * </p>
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {
    @Resource
    AdminMapper adminMapper;
    @Resource
    UserService userService;
    @Resource
    NoticeMapper noticeMapper;
    @Resource
    NoticefileMapper noticefileMapper;
    @Override
    public void insert(Admin admin) {
        adminMapper.insert(admin);
    }

    @Override
    public void updateSelf(Admin admin) {
        adminMapper.updateById(admin);
    }

    @Override
    public void updateAdmin(User user, AdminForm adminForm, Admin admin) {
        int userId = user.getUserId();
        QueryWrapper<Notice> noticeQueryWrapper = new QueryWrapper<>();
        noticeQueryWrapper.eq("user_id",userId);

        List<Notice> notices = noticeMapper.selectList(noticeQueryWrapper);
        if(notices!=null){
            notices.forEach(x->{
                x.setCreateby(adminForm.getName()).setUsername(adminForm.getUsername());
                noticeMapper.updateById(x);
            });
        }


        QueryWrapper<Noticefile> noticefileQueryWrapper = new QueryWrapper<>();
        noticefileQueryWrapper.eq("user_id",userId);
        List<Noticefile> noticefiles = noticefileMapper.selectList(noticefileQueryWrapper);
        if(noticefiles!=null){
            noticefiles.forEach(x->{
                        x.setCreateby(adminForm.getName()).setUsername(adminForm.getUsername());
                        noticefileMapper.updateById(x);
                    }
            );

        }


        String encrypt = MD5Utils.encrypt(adminForm.getUsername(), adminForm.getPassword());
        user.setPassword(encrypt)
                .setName(adminForm.getName())
                .setEmail(adminForm.getEmail())
                .setSex(adminForm.getSex())
                .setPhone(adminForm.getPhone())
                .setDepartment(adminForm.getDepartment())
                .setState(adminForm.getState())
                .setStatus(adminForm.getStatus())
                .setUsername(adminForm.getUsername());
        userService.updateById(user);
        admin.setAdName(adminForm.getName())
                .setAdSex(adminForm.getSex())
                .setAdPhone(adminForm.getPhone())
                .setAdEmail(adminForm.getEmail())
                .setAdWx(adminForm.getWx())
                .setAdStatus(adminForm.getStatus());
        adminMapper.updateById(admin);
    }
}
