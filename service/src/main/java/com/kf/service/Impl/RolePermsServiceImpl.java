package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.mapper.RolePermsMapper;
import com.kf.pojo.RolePerms;
import com.kf.service.RolePermsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class RolePermsServiceImpl extends ServiceImpl<RolePermsMapper, RolePerms> implements RolePermsService {

    @Autowired
    RolePermsMapper rolePermsMapper;


    @Override
    public List<RolePerms> getPerms(int roleId){
        QueryWrapper<RolePerms> objectQueryWrapper = new QueryWrapper<RolePerms>();
        objectQueryWrapper.eq("role_id",roleId);
        return rolePermsMapper.selectList(objectQueryWrapper);
    }

    @Override
    public void  insertRolePerm(int roleId,List<Integer> list){
        QueryWrapper<RolePerms> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_id", roleId);
        List<RolePerms> rolePerms = rolePermsMapper.selectList(queryWrapper);
        if(rolePerms!=null){
            rolePermsMapper.delete(queryWrapper);
        }
        for (Integer integer : list) {
            RolePerms rolePerms1 = new RolePerms();
            rolePerms1.setRoleId(roleId);
            rolePerms1.setPermId(integer);
            rolePermsMapper.insert(rolePerms1);
        }
    }



}
