package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kf.mapper.ChoCgyMapper;
import com.kf.pojo.Category;
import com.kf.pojo.ChoCgy;
import com.kf.pojo.ChoCgyRecord;
import com.kf.mapper.ChoCgyRecordMapper;
import com.kf.pojo.User;
import com.kf.service.ChoCgyRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class ChoCgyRecordServiceImpl extends ServiceImpl<ChoCgyRecordMapper, ChoCgyRecord> implements ChoCgyRecordService {
    @Autowired
    ChoCgyRecordMapper choCgyRecordMapper;
    @Autowired
    UserService userService;
    @Autowired
    ChoCgyMapper choCgyMapper;
    @Override
    public ChoCgyRecord selectByUsername(String username) {
        QueryWrapper<ChoCgyRecord> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("susername",username);
        return choCgyRecordMapper.selectOne(queryWrapper);

    }

    @Override
    public void deleteCgy(String  username) {

        choCgyRecordMapper.deleteCgy(username);
    }

    @Override
    public void insertChoCgyRecord(Category category) {
        QueryWrapper<ChoCgyRecord> choCgyRecordQueryWrapper = new QueryWrapper<>();
        choCgyRecordQueryWrapper.eq("user_id",category.getUserId());
        ChoCgyRecord choCgyRecord1 = choCgyRecordMapper.selectOne(choCgyRecordQueryWrapper);
        Integer userId = category.getUserId();
        User user = userService.getById(userId);
        if(choCgyRecord1==null){
            ChoCgyRecord choCgyRecord = new ChoCgyRecord();
            choCgyRecord.setCgyName(category.getCgyName());
            choCgyRecord.setCgyId(category.getCgyId());
            choCgyRecord.setUserId(userId);
            choCgyRecord.setSusername(user.getUsername());
            choCgyRecord.setStuName(user.getName());
            choCgyRecord.setTusername(category.getTusername());
            choCgyRecord.setTeaName(category.getTeaName());
            choCgyRecord.setStatus(category.getCgyStatus());
            choCgyRecord.setCgySource(category.getCgySource());
            choCgyRecord.setAdvise(category.getCgyAdvise());
            choCgyRecordMapper.insert(choCgyRecord);
        }else {
            choCgyRecord1.setStatus(category.getCgyStatus());
            choCgyRecord1.setAdvise(category.getCgyAdvise());
            choCgyRecordMapper.updateById(choCgyRecord1);
        }

        QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
        choCgyQueryWrapper.eq("user_id",category.getUserId());
        ChoCgy choCgy1 = choCgyMapper.selectOne(choCgyQueryWrapper);
        if(choCgy1==null){
            ChoCgy choCgy = new ChoCgy();
            choCgy.setUserId(userId);

            choCgy.setStuName(user.getName());
            choCgy.setCgyName(category.getCgyName());
            choCgy.setCgyId(category.getCgyId());
            choCgy.setSusername(user.getUsername());
            choCgy.setTusername(category.getTusername());
            choCgy.setTeaName(category.getTeaName());
            choCgy.setCgySource(category.getCgySource());
            choCgyMapper.insert(choCgy);
        }else{
                choCgy1.setCgyId(category.getCgyId())
                        .setStuName(user.getName())
                        .setUserId(category.getUserId());
                choCgy1.setSusername(user.getName());
                choCgy1.setTusername(category.getTusername());
                choCgy1.setTeaName(category.getTeaName());
                choCgy1.setCgyName(category.getCgyName());
                choCgy1.setCgySource(category.getCgySource());
                choCgyMapper.updateById(choCgy1);
            }


    }
}
