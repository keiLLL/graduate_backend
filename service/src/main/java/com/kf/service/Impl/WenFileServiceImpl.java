package com.kf.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kf.config.AliyunOSSUtil;
import com.kf.handler.MyException;
import com.kf.mapper.ChoCgyMapper;
import com.kf.pojo.ChoCgy;
import com.kf.pojo.User;
import com.kf.pojo.WenFile;
import com.kf.mapper.WenFileMapper;
import com.kf.pojo.form.SelectWenForm;
import com.kf.service.WenFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;
import com.kf.util.ResultCode;


import javax.annotation.Resource;
import java.util.ResourceBundle;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class WenFileServiceImpl extends ServiceImpl<WenFileMapper, WenFile> implements WenFileService {

    @Resource
    WenFileMapper wenFileMapper;
    @Resource
    ChoCgyMapper choCgyMapper;
    @Resource
    AliyunOSSUtil aliyunOSSUtil;

    @Override
    public void insert(WenFile wenFile, User student, String[] split,String describe,String fileCode) {
        ResourceBundle resource = ResourceBundle.getBundle("oss");
        String wenurl = resource.getString("wenurl");
        QueryWrapper<WenFile> wenFileQueryWrapper = new QueryWrapper<>();
        wenFileQueryWrapper.eq("susername",student.getUsername());
        WenFile wenFile1 = wenFileMapper.selectOne(wenFileQueryWrapper);
        if(wenFile1!=null){
            String filePath=wenFile1.getPath()+"/"+wenFile1.getFilecode()+"."+ wenFile1.getSuffix();
            System.out.println("进来了");
            aliyunOSSUtil.deleteFile(filePath);
            System.out.println("进来了1");
            wenFile1.setDescribes(describe);
            wenFile1.setUserId(student.getUserId());
            wenFile1.setFilename(split[0]);
            wenFile1.setSuffix(split[1]);
            wenFile1.setCreateby(student.getName());
            wenFile1.setPath(wenurl);
            wenFile1.setFilecode(fileCode);

            QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
            choCgyQueryWrapper.eq("susername",student.getUsername());
            ChoCgy choCgy = choCgyMapper.selectOne(choCgyQueryWrapper);
            if(choCgy==null){
                System.out.println("我的指导老师为空");
                throw new MyException(ResultCode.STUDENT_CHOOSE_ERROR3);
            }
            wenFile1.setTusername(choCgy.getTusername());
            System.out.println("进来了3");

            wenFileMapper.updateById(wenFile1);
            System.out.println("进来了4");

        }else{
            wenFile.setFilename(split[0]);
            wenFile.setUserId(student.getUserId());
            wenFile.setDescribes(describe);
            wenFile.setPath(wenurl);
            wenFile.setSuffix(split[1]);
            wenFile.setCreateby(student.getName());
            wenFile.setFilecode(fileCode);
            QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
            choCgyQueryWrapper.eq("susername",student.getUsername());

            ChoCgy choCgy = choCgyMapper.selectOne(choCgyQueryWrapper);

            if(choCgy==null){
                System.out.println("我的指导老师为空");
                throw new MyException(ResultCode.STUDENT_CHOOSE_ERROR3);
            }
            wenFile.setCreateby(student.getName());
            wenFile.setSusername(student.getUsername());
            wenFile.setTusername(choCgy.getTusername());
            wenFileMapper.insert(wenFile);
        }



    }

    @Override
    public WenFile selectByUsername(String username) {
        QueryWrapper<WenFile> wenFileQueryWrapper = new QueryWrapper<>();
        wenFileQueryWrapper.eq("susername",username);
        return wenFileMapper.selectOne(wenFileQueryWrapper);
    }

    @Override
    public void deleteWenFile(String username) {
        WenFile wenFile = selectByUsername(username);
        aliyunOSSUtil.deleteFile(wenFile.getPath()+"/"+wenFile.getFilecode()+"."+ wenFile.getSuffix());
        QueryWrapper<WenFile> wenFileQueryWrapper = new QueryWrapper<>();
        wenFileQueryWrapper.eq("susername",username);
        wenFileMapper.delete(wenFileQueryWrapper);
    }

    @Override
    public IPage<WenFile> page(SelectWenForm selectForm, User teacher) {
        Page<WenFile> wenFilePage = new Page<>(selectForm.getCurrent(), selectForm.getSize());
        QueryWrapper<WenFile> wenFileQueryWrapper = new QueryWrapper<>();
        wenFileQueryWrapper.eq("tusername",teacher.getUsername());
        if(selectForm.getStatus()!=3){
            wenFileQueryWrapper.eq("status",selectForm.getStatus());
        }
        wenFileQueryWrapper.like("susername",selectForm.getUsername());
        return wenFileMapper.selectPage(wenFilePage, wenFileQueryWrapper);
    }
}
