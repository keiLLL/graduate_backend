package com.kf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.GradFile;

/**
 * <p>
 * 文件表 服务类
 */
public interface FileService extends IService<GradFile> {
    void insertRecords(String name,String suffix,String path,String createBy);
    void insert(GradFile gradFile);
}
