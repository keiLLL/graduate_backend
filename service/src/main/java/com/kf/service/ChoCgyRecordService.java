package com.kf.service;

import com.kf.pojo.Category;
import com.kf.pojo.ChoCgyRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 */
public interface ChoCgyRecordService extends IService<ChoCgyRecord> {
    ChoCgyRecord selectByUsername(String username);
    void deleteCgy(String username);
    void insertChoCgyRecord(Category category);
}
