package com.kf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.Permission;
import com.kf.pojo.User;
import com.kf.pojo.form.AddMenuForm;

import java.util.List;

/**
 * <p>
 *  服务类
 */
public interface PermissionService extends IService<Permission> {

    List<Permission> findAll();
    public  Permission getPermsName(int perId);
    public List<Permission> selectAllXia(int perms_id);
    List<Permission> findMyPerms(User user);
    public Permission findChildren(Permission permission, List<Permission> list);
    public List<Permission> listToTree(List<Permission> permissions);
    void insertTreeperm(Permission permission);
    List<Permission> treeToList(List<Permission>  permissionList,List<Permission> perIdList);
    void addMenu(AddMenuForm addMenuForm);
    void updatePermision(AddMenuForm addMenuForm);
    void deletePermision(int id);
}
