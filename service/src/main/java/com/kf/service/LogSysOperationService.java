package com.kf.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kf.pojo.LogSysOperation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.User;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 操作日志 服务类
 */
public interface LogSysOperationService extends IService<LogSysOperation> {
    void insert(LogSysOperation logSysOperation);
    IPage<LogSysOperation> pageOpera(String name, int current, int size);
    void exports(HttpServletResponse response, String tmpFileName);
}
