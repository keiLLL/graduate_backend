package com.kf.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kf.pojo.User;
import com.kf.pojo.WenFile;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.form.SelectForm;
import com.kf.pojo.form.SelectWenForm;


public interface WenFileService extends IService<WenFile> {
   void insert(WenFile wenFile, User user, String[] split,String describe,String fileCode);
   WenFile selectByUsername(String username);
   void deleteWenFile(String username);

   IPage<WenFile> page(SelectWenForm selectForm, User teacher);
}
