package com.kf.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.Records;
import com.kf.pojo.User;
import com.kf.pojo.form.AddRecord;
import com.kf.pojo.form.RecordForm;
import com.kf.pojo.form.SelectForm;
import com.kf.pojo.form.SelectRecordForm;

import java.util.List;

/**
 * <p>
 * 学生指导记录表 服务类
 */
public interface RecordsService extends IService<Records> {
    void inertRecord(RecordForm recordForm,User student,String tusername);
    IPage<Records> pageRecord(SelectForm selectForm,User student);
    void updateRecord(RecordForm recordForm,User student);
    void deleteRecord(List<Integer> list);
    IPage<Records> page(SelectForm selectForm,User teacher);
    void insertRecordTea(AddRecord addRecord,User teacher);
    IPage<Records> listRecords(SelectForm selectForm);
    void deleteById(int id);
    IPage<Records> pageRecordAll(SelectRecordForm selectRecordForm,User user);
}
