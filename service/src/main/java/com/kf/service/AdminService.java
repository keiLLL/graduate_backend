package com.kf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.Admin;
import com.kf.pojo.User;
import com.kf.pojo.form.AdminForm;

/**
 * <p>
 * 管理员表 服务类
 */
public interface AdminService extends IService<Admin> {
     void insert(Admin admin);
     void updateSelf(Admin admin);
     void updateAdmin(User user, AdminForm adminForm,Admin admin);

}
