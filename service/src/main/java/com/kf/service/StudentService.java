package com.kf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.Student;
import com.kf.pojo.User;
import com.kf.pojo.form.StudentForm;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface StudentService extends IService<Student> {
    Student findById(int userId);
    void updateSelf(Student student);
    void insert(Student student);
    void updateStudent(User user,Student student, String encrypt, StudentForm studentForm);
    void updateStudentSelf(User user,Student student, StudentForm studentForm);

}
