package com.kf.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.Category;
import com.kf.pojo.User;
import com.kf.pojo.form.*;

import java.util.List;

/**
 * <p>
 * 课题类别信息表 服务类
 */
public interface CategoryService extends IService<Category> {
    void insertCategory(Category category);

    /**
     * //一个查询框调用两个查询方法，两种查询！可以通过老师教职工号和课题名查询
     */
//    IPage<Category> pageCategoryByuserId(String username,int userId,int current,int size);
    IPage<Category> pageCategoryBycgyName(String username,String cateName,int current,int size);

    IPage<Category> pageCategoryByDuoCondi(SelectCgyForm selectCgyForm);

    void insertCgyRecord(int cgyId,User student);
    Category selectByUserId(int userId);
    Category setCategory(AddCgyForm addCgyForm, User user,Category category);
    Category setCategoryUpdate(AddCgyForm addCgyForm, User user,Category category);
    Category setTeaCategory(TeaAddCgyForm addCgyForm, User user, Category category);

    Category setTeaCategoryUpdate(TeaAddCgyForm addCgyForm, User user, Category category);
    void delete(Category category);
    void deleteCgy(DeleteForm deleteForm,User user);
    IPage<Category> teaPageMyCategory(SelectForm selectForm,User teacher);
    void batchCheckCategory(CheckList<Integer> checkList);
    void batchLeaderCheck(CheckList<Integer> checkList);
    void deleteAllCgy(int cgyId);

    Category selectByCgyName(String cgyName);
}
