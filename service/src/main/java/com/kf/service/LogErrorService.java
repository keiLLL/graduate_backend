package com.kf.service;

import cn.hutool.http.HttpResponse;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kf.pojo.LogError;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.LogLogin;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 异常日志 服务类
 */
public interface LogErrorService extends IService<LogError> {
    IPage<LogError> pageLogError(String name, int current, int size);
    void exports(HttpServletResponse response,String tmpFileName);
}
