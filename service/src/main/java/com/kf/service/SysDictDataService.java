package com.kf.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kf.pojo.SysDictData;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.form.AddSysDicData;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.SelectDictData;
import com.kf.pojo.form.SelectForm;

/**
 * <p>
 * 字典数据 服务类

 */
public interface SysDictDataService extends IService<SysDictData> {

    IPage<SysDictData> selecPage(SelectDictData selectForm);
    void delete(DeleteForm deleteForm);
    void insertData(AddSysDicData addSysDicData);
    void updateData(AddSysDicData addSysDicData);
}
