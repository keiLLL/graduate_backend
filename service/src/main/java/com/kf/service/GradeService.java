package com.kf.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kf.pojo.Grade;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.User;
import com.kf.pojo.form.AddGrade;
import com.kf.pojo.form.SelectForm;

/**
 * <p>
 *  服务类
 */
public interface GradeService extends IService<Grade> {
    void insert(AddGrade addGrade, User user);
    IPage<Grade> pageGrade(SelectForm selectForm,User user);
}
