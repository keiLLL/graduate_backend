package com.kf.service;

import com.kf.pojo.Noticefile;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.User;

/**
 * <p>
 *  服务类
 */
public interface NoticefileService extends IService<Noticefile> {
    void insertNoticeFile(Noticefile noticefile, User user,String[] split,int noticeId,String fileCode);
    void deleteFile(int noticeId);
    void deleteFileByNoticeId(int noticeid);
    void updateNoticeFile(Noticefile noticefile, User user, String[] split,int noticeId,String fileCode);
}
