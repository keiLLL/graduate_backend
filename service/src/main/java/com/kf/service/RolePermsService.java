package com.kf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.RolePerms;

import java.util.List;

/**
 * <p>
 *  服务类
 */
public interface RolePermsService extends IService<RolePerms> {
    public List<RolePerms> getPerms(int roleId);
    public void  insertRolePerm(int roleId,List<Integer> list);
}
