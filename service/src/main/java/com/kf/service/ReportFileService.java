package com.kf.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kf.pojo.ReportFile;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.User;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.ReportVo;
import com.kf.pojo.form.SelectReportForm;

/**
 * <p>
 *  服务类
 */
public interface ReportFileService extends IService<ReportFile> {
    ReportFile getByUserId(int id);
    void insert(ReportFile reportFile, User student,String[] split,String fileCode);
    void delete(int userId);
    IPage<ReportFile> pageReport(User user, SelectReportForm selectReportForm);
    void deletebyId(DeleteForm deleteForm);
}
