package com.kf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.Stage;

public interface StageService extends IService<Stage> {
    Stage findById(Integer stuId);
}
