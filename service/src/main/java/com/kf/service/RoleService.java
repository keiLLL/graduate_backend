package com.kf.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.Role;
import com.kf.pojo.User;
import com.kf.pojo.form.PageFormRole;
import com.kf.pojo.form.SelectForm;

import java.util.List;

/**
 * <p>
 *  服务类
 */
public interface RoleService extends IService<Role> {
    public Role getRole(int roleId);
    public List<Role> findAll();
    public int addRole(Role role);
    IPage<Role> pageRole(String name,int current, int size);
    List<Role> selectRoleByName(String roleName);
    void deleteRole(List<Integer> list);
    Role findRoleUser(int roleId);
    PageFormRole redisGetRole(SelectForm selectForm,String pagekeys) throws InterruptedException;
}
