package com.kf.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kf.pojo.ChoCgy;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.User;
import com.kf.pojo.form.AddGrade;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.SelectForm;

/**
 * <p>
 *  服务类

 */
public interface ChoCgyService extends IService<ChoCgy> {
        void insertCho(ChoCgy choCgy);
        void deleteChoMy(DeleteForm deleteForm);
        ChoCgy selectBySusername(String susername);
        ChoCgy selectByuserId(int userId);
        IPage<ChoCgy> page(SelectForm selectForm, User teacher);
        void insertGrade(AddGrade addGrade);
        IPage<ChoCgy> pageAll(SelectForm selectForm);
}
