package com.kf.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.Category;
import com.kf.pojo.ChoCgyRecord;
import com.kf.pojo.Teacher;
import com.kf.pojo.User;
import com.kf.pojo.form.*;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 教师表 服务类
 */
public interface TeacherService extends IService<Teacher> {
    void insert(Teacher teacher);
    void updateSelf(Teacher teacher);
    void cheChoRecord(CheckCgyForm checkCgyForm);
    PageForm<ChoCgyRecord> pageChoMy(ChoMyForm choMyForm);
    void batchCheckRecord(CheckList<Integer> checkList);
    Teacher findById(int userId);
    List<Teacher> selectList(User user);
    IPage<Category> pageCategory(SelectForm selectForm);
    void updateTeacher(User user,TeacherForm teacherForm,Teacher teacher);
    void updateTeacherSelf(User user,TeacherForm teacherForm,Teacher teacher);
}
