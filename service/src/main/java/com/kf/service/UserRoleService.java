package com.kf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.UserRole;


import java.util.List;


public interface UserRoleService extends IService<UserRole> {
    List<UserRole> getUserRole(int userId);
//    public List<UserRole> findUserById(int role_id);
     void  insertUserRole(int userId,List<Integer> list);
    void  insertRoleUser(List<Integer> list,int roleId);
    void deletRoleUser(List<Integer> list);
    List<UserRole> selectUserRoleList(int roleId);
    void insert(UserRole userRole);
}
