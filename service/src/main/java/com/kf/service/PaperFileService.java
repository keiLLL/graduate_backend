package com.kf.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kf.pojo.PaperFile;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.User;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.SelectForm;

/**
 * <p>
 *  服务类
 */
public interface PaperFileService extends IService<PaperFile> {
    void insertPaper(PaperFile paperFile, User student,String[] split,String fileCode);
    PaperFile selectByUsername(String useranme);
    void  deletePaper(String username);
    IPage<PaperFile> page(SelectForm selectForm,User teacher);
    void deletePaperTea(DeleteForm deleteForm);
}
