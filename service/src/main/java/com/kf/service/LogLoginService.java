package com.kf.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kf.pojo.LogLogin;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.LogSysOperation;

/**
 * <p>
 * 登录日志 服务类
 */
public interface LogLoginService extends IService<LogLogin> {
    public IPage<LogLogin> pageLogin(String name, int current, int size);
    LogLogin getByName(String username);
}
