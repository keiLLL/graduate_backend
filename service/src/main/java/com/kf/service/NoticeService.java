package com.kf.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.Notice;
import com.kf.pojo.User;
import com.kf.pojo.form.AddNotice;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.NoticeForm;


/**
 * <p>
 * 留言板信息表 服务类
 */
public interface NoticeService extends IService<Notice> {
    int insertNotice(AddNotice addNotice, User user);
    Notice getNoticeById(String id);
    void updateNotice(AddNotice addNotice,User user);
    void deleteNotice(DeleteForm deleteForm);

}
