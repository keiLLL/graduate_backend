package com.kf.service;

import com.kf.pojo.Notice;
import com.kf.pojo.User;
import com.kf.pojo.form.NoticeForm;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface CommonService {

    List<NoticeForm> getMyNotice(String notice, User user) throws IOException;
    List<Notice> getAllNotice(User user) throws IOException;
    Boolean send(Map<String, Object> param,String phone) throws Exception;
}
