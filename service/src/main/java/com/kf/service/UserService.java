package com.kf.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.Admin;
import com.kf.pojo.Student;
import com.kf.pojo.Teacher;
import com.kf.pojo.User;
import com.kf.pojo.form.*;

import java.util.List;

public interface UserService extends IService<User> {
//     User queryUserByName2(String username);
    User queryUserByName(String username);
    void addUser(AddUserForm adduserForm);
    void updatePassword(User user);
    List<User> selectAllUser();
    //分页展示所有用户
    IPage<User> pageUser(String name,int current, int size);
    void deleteUser(List<Integer> userId);
    List<User> selectUserByName(String name);
    List<User> selectUserListById(List<Integer> userId);
    int countTeaByDepartmen(User user);
    int countStuByDepartmen(User user);
    User findByPhone(String phone);
    void addUserAll(AddUserForm addUserForm,User user);
    StudentForm getStudent(Student student,User user,int state);
    TeacherForm getTeacher(Teacher teacher, User user, int state);
    AdminForm getTeacher(Admin admin, User user, int state);
    PageFormUser reidsGetUser(SelectForm selectForm,String pagekeys) throws Exception;
}
