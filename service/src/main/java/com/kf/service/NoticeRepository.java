package com.kf.service;


import com.kf.pojo.Notice;
import org.springframework.data.elasticsearch.annotations.Highlight;
import org.springframework.data.elasticsearch.annotations.HighlightField;
import org.springframework.data.elasticsearch.annotations.HighlightParameters;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoticeRepository extends ElasticsearchRepository<Notice, String> {

    @Highlight(fields = {
            @HighlightField(name = "createby")
    })
    @Query("{\"match\":{\"createby\":\"?0\"}}")
    SearchHits<Notice> findTitle(String keyword);

    @Query(  "{\"multi_match\": {" +
            "\"query\": \"?0\"," +
            "\"fields\": [\"createby\",\"noticeTopic\",\"noticeContent\"]}}"
    )
    @Highlight(fields = {
            @HighlightField(name = "createby"),
            @HighlightField(name = "noticeTopic"),
            @HighlightField(name = "noticeContent")
    },
            parameters = @HighlightParameters(
                    preTags = "<font color=\"#f00\">",
                    postTags = "</font>",
                    fragmentSize = 500,
                    numberOfFragments = 3
            ))
    SearchHits<Notice> findTitleOrDesc(String keyword);

    @Query(  "{" +
            "\"multi_match\": {" +
            "\"query\": \"?0\"," +
            "\"fields\": [\"createby\",\"noticeTopic\",\"noticeContent\"]}}")
    @Highlight(
            fields = {
                    @HighlightField(name = "createby"),
                    @HighlightField(name = "noticeTopic"),
                    @HighlightField(name = "noticeContent")
            },
            parameters = @HighlightParameters(
                    preTags = "<strong><font style='color:red'>",
                    postTags = "</font></strong>",
                    fragmentSize = 500,
                    numberOfFragments = 3
            )
    )
    SearchHits<Notice> findTitleOrDesc1(String keyword);
}

