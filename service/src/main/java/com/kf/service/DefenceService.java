package com.kf.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kf.pojo.Defence;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.User;
import com.kf.pojo.form.AddDefence;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.SelectForm;

/**
 * <p>
 *  服务类
 */
public interface DefenceService extends IService<Defence> {
    IPage<Defence> getDefenceTea(SelectForm selectForm, String tusername);
    Defence getDefenceStu(User user);
    void insertDefence(AddDefence addDefence,User user);
    void updateDefence(AddDefence addDefence);
    void delete(DeleteForm deleteForm);
    IPage<Defence> select(SelectForm selectForm, String department);
}
