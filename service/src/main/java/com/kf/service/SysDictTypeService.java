package com.kf.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kf.pojo.SysDictData;
import com.kf.pojo.SysDictType;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kf.pojo.form.AddSysDictType;
import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.SelectDictData;
import com.kf.pojo.form.SelectForm;

import java.util.List;

/**
 * <p>
 * 字典类型 服务类
 */
public interface SysDictTypeService extends IService<SysDictType> {

    IPage<SysDictType> selectPage(SelectForm selectForm);
    void delete(DeleteForm deleteForm);
    void insertType(AddSysDictType addSysDictType);
    void updateType(AddSysDictType addSysDictType);
    List<SysDictType> selectData();
}
