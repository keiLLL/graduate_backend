package com.kf.thread;//package com.kf.thread;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.kf.mapper.LogErrorMapper;
import com.kf.mapper.LogSysOperationMapper;
import com.kf.pojo.LogError;
import com.kf.pojo.LogSysOperation;
import com.kf.pojo.form.LogErrorDTO;
import com.kf.pojo.form.LogSysOperationDTO;


import java.io.File;
import java.util.List;


public class DownloadExcelRunnable1 implements Runnable {
    private int index;
    private int pageSize;
    private LogSysOperationMapper mapper;
    private File file;

    public DownloadExcelRunnable1(int index, int pageSize, File file, LogSysOperationMapper mapper) {
        this.index = index;
        this.pageSize = pageSize;
        this.mapper = mapper;
        this.file = file;
    }

    @Override
    public void run() {
        try {

            List<LogSysOperation> list = mapper.findPage(index* pageSize, pageSize);
            // 使用easyexcel生成excel文件
            writeExcel(file, LogSysOperationDTO.class, list, ExcelTypeEnum.XLS);
            System.out.println(Thread.currentThread().getName()+",mulit导出excel完成");
        } catch (Exception e) {
            System.out.println(Thread.currentThread().getName() + "出现错误！！！");
            e.printStackTrace();
        }
    }

    /**
     * @Title: writeExcel
     * @Description: 写入excel到本地路径
     */
    private void writeExcel(File newFile, Class<?> clazz, List<?> datalist, ExcelTypeEnum excelType) {
        EasyExcel.write(newFile, clazz).excelType(excelType).sheet("sheet1").doWrite(datalist);
    }
}
