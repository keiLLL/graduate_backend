//import com.alibaba.fastjson.JSON;
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.kf.config.ElasticSearchClientConfig;
//import com.kf.mapper.NoticeMapper;
//import com.kf.pojo.Notice;
//import com.kf.pojo.form.NoticeForm;
//import com.kf.service.NoticeService;
//import org.elasticsearch.action.bulk.BulkRequest;
//import org.elasticsearch.action.bulk.BulkResponse;
//import org.elasticsearch.action.index.IndexRequest;
//import org.elasticsearch.client.RequestOptions;
//import org.elasticsearch.client.RestHighLevelClient;
//import org.elasticsearch.common.xcontent.XContentType;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import javax.annotation.Resource;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
//public class esTest {
//    @Autowired
//    NoticeService noticeService;
//    @Autowired
//    RestHighLevelClient restHighLevelClient;
//    @Test
//    public void findByDistrictId() throws IOException {
//        // 数据库中查询数据
//        String id = "114";
//        List<Notice> NoticeList = new ArrayList<>();
//        Notice testNotice = noticeService.getById(id);
//        NoticeList.add(testNotice);
////        List<Notice> NoticeList = noticeService.getById(id);
//        List<NoticeForm> NoticeFormList = new ArrayList<>();
//        for(Notice notice : NoticeList){
//            NoticeForm noticeForm = new NoticeForm();
//            noticeForm.setId(notice.getId().toString());
//            noticeForm.setUsername(notice.getUsername());
//            noticeForm.setCreateby(notice.getCreateby());
//            noticeForm.setTarget(notice.getTarget().toString());
//            noticeForm.setNoticeTopic(notice.getNoticeTopic());
//            noticeForm.setNoticeContent(notice.getNoticeContent());
//            noticeForm.setCreatetime(notice.getCreatetime());
//            NoticeFormList.add(noticeForm);
//        }
//        BulkRequest bulkRequest = new BulkRequest();
//        for (NoticeForm noticeForm2 : NoticeFormList) {
//
//            /*将对象转换成字符串*/
//            String jsonString = JSON.toJSONString(noticeForm2);
//
//            /*创建请求*/
//            IndexRequest indexRequest = new IndexRequest();
//            indexRequest.index("kang_index");
//            indexRequest.id(noticeForm2.getId()+"");
//            indexRequest.source(jsonString, XContentType.JSON);
//
//            /*批量存入大量请求*/
//            bulkRequest.add(indexRequest);
//        }
//
//        /*请求转发，得到响应*/
////        BulkResponse resp =ElasticSearchUtilTE.getClient().bulk(bulkRequest, RequestOptions.DEFAULT);
//        BulkResponse resp = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
//        System.out.println(resp.status());
//    }
//}
