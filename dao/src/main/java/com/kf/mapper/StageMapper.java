package com.kf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kf.pojo.Stage;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface StageMapper extends BaseMapper<Stage> {

}
