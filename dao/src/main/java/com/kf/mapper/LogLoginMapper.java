package com.kf.mapper;

import com.kf.pojo.LogLogin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 登录日志 Mapper 接口
 */
@Mapper
public interface LogLoginMapper extends BaseMapper<LogLogin> {
    void deleteByDeleted(int deleted);
}
