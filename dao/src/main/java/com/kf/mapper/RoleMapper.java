package com.kf.mapper;


import com.kf.pojo.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kf.pojo.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
@Repository
//@CacheNamespace(implementation= MybatisPlusRedisCache.class,eviction= MybatisPlusRedisCache.class)
public interface RoleMapper extends BaseMapper<Role> {
        List<UserRole> findUserById(int roleId);
        List<Role> findAll();
        List<Role> selectRoleByName(String name);
        Role findRoleUser(int roleId);
        void deleteByDeleted(int deleted);
}
