package com.kf.mapper;

import com.kf.pojo.LogError;
import com.kf.pojo.LogSysOperation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 操作日志 Mapper 接口
 */

@Mapper
public interface LogSysOperationMapper extends BaseMapper<LogSysOperation> {
//    void insertLogOpera(int status);
//    void insertLogOperation(LogSysOperation logSysOperation);
//void deleteByDeleted(int deleted);
    @Select("select * from log_sys_operation limit  #{pageNum},#{pageSize}")
    List<LogSysOperation> findPage(@Param("pageNum") int pageNum, @Param("pageSize") int pageSize);

}
