package com.kf.mapper;

import com.kf.pojo.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 留言板信息表 Mapper 接口
 */
@Mapper
public interface NoticeMapper extends BaseMapper<Notice> {

}
