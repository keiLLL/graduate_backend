package com.kf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kf.pojo.Defence;
import com.kf.pojo.MidtermDefence;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
public interface MidtermDefenceMapper extends BaseMapper<MidtermDefence> {
    void deleteByDeleted(int deleted);
}
