package com.kf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kf.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper extends BaseMapper<User>{
    User  queryUserByName(String username);
    List<User> queryById();
    User queryStuById(int userId);
    List<User> queryAllStudent();
    List<User> queryAllTeacher();
    List<User> selectUserByName(String name);
    void deleteByDeleted(int deleted);
}
