package com.kf.mapper;

import com.kf.pojo.SysDictData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 字典数据 Mapper 接口
 */
@Mapper
public interface SysDictDataMapper extends BaseMapper<SysDictData> {
    void deleteByDeleted(int deleted);
}
