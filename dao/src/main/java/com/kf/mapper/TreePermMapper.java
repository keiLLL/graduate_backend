package com.kf.mapper;

import com.kf.pojo.TreePerm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 */
@Repository
@Mapper
public interface TreePermMapper extends BaseMapper<TreePerm> {

    List<TreePerm> findAllParentByid(int id);
    List<TreePerm> findAllChildrenTreeByid(int id);
    void deleteByIdtree(int id);
    void deleteByDeleted(int deleted);

    void deleteByIdtree2(Integer permId);
}
