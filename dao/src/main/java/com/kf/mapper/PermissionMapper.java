package com.kf.mapper;

import com.kf.pojo.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
@Repository
public interface PermissionMapper extends BaseMapper<Permission> {
    void deleteByDeleted(int deleted);
}
