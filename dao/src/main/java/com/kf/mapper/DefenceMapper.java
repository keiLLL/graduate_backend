package com.kf.mapper;

import com.kf.pojo.Defence;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
public interface DefenceMapper extends BaseMapper<Defence> {
    void deleteByDeleted(int deleted);
}
