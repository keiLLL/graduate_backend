package com.kf.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.kf.pojo.RolePerms;
import com.kf.pojo.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
@Repository
public interface UserRoleMapper extends BaseMapper<UserRole> {
    public List<UserRole> findUserById(int role_id);
    void deleteByRoleId(int roleId);
    void deleteByDeleted(int deleted);
}
