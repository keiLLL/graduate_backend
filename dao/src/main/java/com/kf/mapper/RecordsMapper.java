package com.kf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kf.pojo.Records;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 学生指导记录表 Mapper 接口
 */
@Mapper
public interface RecordsMapper extends BaseMapper<Records> {
    void deleteByDeleted(int deleted);
}
