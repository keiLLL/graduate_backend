package com.kf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.kf.pojo.Report;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
public interface ReportMapper extends BaseMapper<Report> {
    void deleteByDeleted(int deleted);
}
