package com.kf.mapper;

import com.kf.pojo.SysDictType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 字典类型 Mapper 接口
 * */
@Mapper
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {
    void deleteByDeleted(int deleted);
}
