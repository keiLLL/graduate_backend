package com.kf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kf.pojo.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
@Repository
public interface StudentMapper extends BaseMapper<Student> {
    void deleteByDeleted(int deleted);
}
