package com.kf.mapper;

import com.kf.pojo.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 管理员表 Mapper 接口
 */
@Mapper
public interface AdminMapper extends BaseMapper<Admin> {
    void deleteByDeleted(int deleted);
}
