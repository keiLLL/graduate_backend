package com.kf.mapper;

import com.kf.pojo.Noticefile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
public interface NoticefileMapper extends BaseMapper<Noticefile> {

}
