package com.kf.mapper;

import com.kf.pojo.Teacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 教师表 Mapper 接口
 */
@Mapper
public interface TeacherMapper extends BaseMapper<Teacher> {
    void deleteByDeleted(int deleted);
}
