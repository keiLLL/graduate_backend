package com.kf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kf.pojo.Admin;
import com.kf.pojo.InspectionReport;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface InspectionReportMapper extends BaseMapper<InspectionReport> {
    void updateStatus(String susername);
}
