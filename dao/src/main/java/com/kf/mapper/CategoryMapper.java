package com.kf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kf.pojo.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 课题类别信息表 Mapper 接口
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
    void deleteByDeleted(int deleted);
}
