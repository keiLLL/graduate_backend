package com.kf.mapper;

import com.kf.pojo.WenFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
public interface WenFileMapper extends BaseMapper<WenFile> {
    void deleteByDeleted(int deleted);
}
