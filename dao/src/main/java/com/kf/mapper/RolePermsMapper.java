package com.kf.mapper;

import com.kf.pojo.RolePerms;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
@Repository
public interface RolePermsMapper extends BaseMapper<RolePerms> {
    void deleteRolePerms(int roleId);
    void deleteByDeleted(int deleted);
}
