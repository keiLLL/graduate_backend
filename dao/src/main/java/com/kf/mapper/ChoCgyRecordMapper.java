package com.kf.mapper;

import com.kf.pojo.ChoCgyRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
public interface ChoCgyRecordMapper extends BaseMapper<ChoCgyRecord> {
    void deleteCgy(String username);
    void deleteByDeleted(int deleted);
}
