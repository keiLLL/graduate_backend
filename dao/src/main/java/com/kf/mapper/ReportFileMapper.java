package com.kf.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kf.pojo.ReportFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kf.pojo.form.ReportVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
public interface ReportFileMapper extends BaseMapper<ReportFile> {

}
