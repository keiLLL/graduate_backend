package com.kf.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kf.pojo.ChoCgy;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
public interface ChoCgyMapper extends BaseMapper<ChoCgy> {
    void deleteByDeleted(int deleted);
}
