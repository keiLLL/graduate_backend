package com.kf.mapper;

import com.kf.pojo.LogError;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 异常日志 Mapper 接口
 */
@Mapper
public interface LogErrorMapper extends BaseMapper<LogError> {
    @Select("select * from log_error limit  #{pageNum},#{pageSize}")
    List<LogError> findPage(@Param("pageNum") int pageNum,@Param("pageSize") int pageSize);


}
