package com.kf.mapper;

import com.kf.pojo.PaperFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
public interface PaperFileMapper extends BaseMapper<PaperFile> {
    void deleteByDeleted(int deleted);
}
