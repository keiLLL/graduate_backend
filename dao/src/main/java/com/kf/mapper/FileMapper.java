package com.kf.mapper;

import com.kf.pojo.GradFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 文件表 Mapper 接口
 */
@Mapper
public interface FileMapper extends BaseMapper<GradFile> {

}
