package com.kf.mapper;

import com.kf.pojo.Grade;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 */
@Mapper
public interface GradeMapper extends BaseMapper<Grade> {
    void deleteByDeleted(int deleted);
}
