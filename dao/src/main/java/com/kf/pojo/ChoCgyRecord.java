package com.kf.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**

 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ChoCgyRecord对象", description="")
public class ChoCgyRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "课题id")
    private Integer cgyId;
    @TableField("user_id")
    private Integer userId;
    @ApiModelProperty(value = "学生学号")
    private String susername;


    @ApiModelProperty(value = "课题名")
    private String cgyName;

    @ApiModelProperty(value = "来源")
    private int cgySource;

    @ApiModelProperty(value = "指导老师工号")
    private String tusername;

    @ApiModelProperty(value = "老师名")
    private String teaName;

    @ApiModelProperty(value = "0未审核，1审核驳回，2审核通过")
    private Integer status;

    @ApiModelProperty(value = "学生id")
    private String stuName;

    @ApiModelProperty(value = "建议")
    private String advise;

    @TableLogic
    private Integer deleted;

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtCreate;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtModified;

    @Version
    private Integer version;


}
