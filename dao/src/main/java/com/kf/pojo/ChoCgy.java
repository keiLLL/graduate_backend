package com.kf.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ChoCgy对象", description="")
public class ChoCgy implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "课题id")
    private Integer cgyId;
    @ApiModelProperty(value = "课题名")
    private String cgyName;
    @TableField("user_id")
    private Integer userId;
    @ApiModelProperty(value = "来源")
    private int cgySource;

    @ApiModelProperty(value = "学生id")
    private String susername;
    @ApiModelProperty(value = "学生id")
    private String stuName;

    @ApiModelProperty(value = "成绩")
    private int grade;
    @ApiModelProperty(value = "成绩")
    private int grade1;
    @ApiModelProperty(value = "成绩")
    private int grade2;

    @ApiModelProperty(value = "成绩")
    private double average;

    @ApiModelProperty(value = "指导老师id")
    private String tusername;

    @ApiModelProperty(value = "指导老师名字")
    private String teaName;

    @ApiModelProperty(value = "逻辑删除")
    @TableLogic
    private Integer deleted;

    @ApiModelProperty(value = "审查报告提交状态")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty(value = "乐观锁")
    @Version
    private Integer version;


}
