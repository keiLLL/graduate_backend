package com.kf.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Student对象", description="")
@AllArgsConstructor
@NoArgsConstructor
public class Student extends People implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "学号")
    @TableId(value = "stu_id", type = IdType.AUTO)
    private int stuId;
    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private int userId;

    @ApiModelProperty(value = "学生姓名")
    private String stuName;

    @ApiModelProperty(value = "性别")
    private String stuSex;

    @ApiModelProperty(value = "所属院系")
    private String stuDepartment;

    @ApiModelProperty(value = "专业")
    private String stuProfessional;

    @ApiModelProperty(value = "用户手机号")
    private String stuPhone;

    @ApiModelProperty(value = "用户邮箱")
    private String stuMail;

    @ApiModelProperty(value = "班级")
    private String stuClass;

    @ApiModelProperty(value = "微信")
    private String stuWx;

    @ApiModelProperty(value = "qq")
    private String stuQq;

//    @ApiModelProperty(value = "密码")
//    private String stuPassword;

    @ApiModelProperty(value = "所选课题")
    private String stuTitle;

    @ApiModelProperty(value = "登录状态，0就是从未登录，1就是登录过，且已修改密码")
    private Integer stuStatus;

    @ApiModelProperty(value = "用户角色 学生1 老师2 管理员3")
    private Integer stuRole;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty(value = "乐观锁")
    @Version
    private Integer version;

    @ApiModelProperty(value = "逻辑删除")
    @TableLogic
    private Integer deleted=0;


}
