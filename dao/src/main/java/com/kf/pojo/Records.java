package com.kf.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 学生指导记录表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Records对象", description="学生指导记录表")
public class Records implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "指导记录id")
    @TableId(value = "records_id", type = IdType.AUTO)
    private Integer recordsId;
    @TableField("user_id")
    private Integer userId;
    @ApiModelProperty(value = "用户id")
    private String  name;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "身份")
    private Integer state;

    @ApiModelProperty(value = "指导教师")
    private String reTeacher;

    @ApiModelProperty(value = "指导收获")
    private String reHarvest;

    @ApiModelProperty(value = "对指导老师建议")
    private String reAdvise;

    @ApiModelProperty(value = "指导目标(老师填写被指导的学生学号)")
    private String reTarget;

    private String department;

    @ApiModelProperty(value = "学生学习情况")
    private String stuSituation;

    @ApiModelProperty(value = "指导内容")
    private String reImfor;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtModified;

    @ApiModelProperty(value = "乐观锁")
    @Version
    private Integer version;

    @ApiModelProperty(value = "逻辑删除")
    @TableLogic
    private Integer deleted;


}
