package com.kf.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * @author LDQ
 */
@Data
@ApiModel(value="检查报告", description="检查报告")
@TableName("inspection_report")
public class InspectionReport implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("文件名")
    private String filename;

    @ApiModelProperty("文件访问路径")
    private String path;

    @ApiModelProperty("上传者名字")
    private String createby;

    @ApiModelProperty("上传者id")
    private Integer userId;

    @ApiModelProperty("学生学号")
    private String susername;

    /**
     * 0表示期中 1表示期末
     */
    @ApiModelProperty("阶段")
    private Boolean stage;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @Version
    private Integer version;

    @TableLogic
    private Integer deleted;
}
