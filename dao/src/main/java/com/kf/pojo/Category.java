package com.kf.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 课题类别信息表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Category对象", description="课题类别信息表")
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "课题id")
    @TableId(value = "cgy_id", type = IdType.AUTO)
    private Integer cgyId;

    @ApiModelProperty(value = "课题创建者id")
    private Integer userId;
    private String username;
    private String name;
    @ApiModelProperty(value = "课题名")
    private String cgyName;

    @ApiModelProperty(value = "系别")
    private String cgyDepart;

    @ApiModelProperty(value = "题目类型")
    private String cgyType;

    @ApiModelProperty(value = "题目来源0学生1老师")
    private Integer cgySource;

    @ApiModelProperty(value = "指导老师教职工号")
    private String tusername;

    @ApiModelProperty(value = "指导老师名字")
    private String teaName;

    @ApiModelProperty(value = "课题描述")
    private String describes;

    @ApiModelProperty(value = "老师审核状态0未审核，1审核驳回，2审核通过")
    private Integer cgyStatus;

    @ApiModelProperty(value = "院系审核0未审核，1审核驳回，2审核通过")
    private Integer cgyLeadstatus;
    @ApiModelProperty(value = "院系审核建议")
    private String  cgyLeadadvice;

    @ApiModelProperty(value = "审核建议")
    private String cgyAdvise;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty(value = "乐观锁")
    @Version
    private Integer version;

    @ApiModelProperty(value = "逻辑删除")
    @TableLogic
    private Integer deleted;


}
