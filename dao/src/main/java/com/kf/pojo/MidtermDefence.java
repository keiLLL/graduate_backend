package com.kf.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 */
@Data
@ApiModel(value="MidtermDefence对象", description="MidtermDefence对象")
@TableName("midterm_defence")
public class MidtermDefence implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String department;
    @ApiModelProperty(value = "指导老师")
    private String tusername;

    @ApiModelProperty(value = "姓名")
    private String teaName;


    @ApiModelProperty(value = "班级")
    @TableField("class")
    private String classes;

    @ApiModelProperty(value = "答辩开始时间")
    @TableField("starttime")
    private String starttime;

    @ApiModelProperty(value = "答辩地点")
    private String place;

    @ApiModelProperty(value = "答辩老师1教职号")
    private String tusername1;

    @ApiModelProperty(value = "答辩老师1")
    @TableField("teaname1")
    private String teaname1;

    @ApiModelProperty(value = "答辩老师2教职号")
    private String tusername2;

    @ApiModelProperty(value = "答辩老师2")
    @TableField("teaname2")
    private String teaname2;

    @ApiModelProperty(value = "答辩老师3教职号")
    private String tusername3;

    @ApiModelProperty(value = "答辩老师3")
    @TableField("teaname3")
    private String teaname3;

    @ApiModelProperty(value = "答辩老师1教职号")
    private String tusername4;

    @ApiModelProperty(value = "答辩老师4")
    @TableField("teaname4")
    private String teaname4;

    @Version
    private Integer version;

    @TableLogic
    private Integer deleted;

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtCreate;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtModified;


}
