package com.kf.pojo;

//适配器模式  Adapter  将被适配者（User）和目标抽象类(Target)组合到一起的一个新类。
public class Adapter extends User implements Target {
	@Override
    public void request() {
        super.adaptee();
    }
}
