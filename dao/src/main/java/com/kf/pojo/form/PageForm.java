package com.kf.pojo.form;

import lombok.Data;

import java.util.List;
@Data
public class PageForm<T> {
    List<T> list;
    long total;
}
