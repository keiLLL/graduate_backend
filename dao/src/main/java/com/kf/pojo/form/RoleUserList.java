package com.kf.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;
@Data
public class RoleUserList {

    @NotNull
    int roleId;
    List<Integer> userList;
}
