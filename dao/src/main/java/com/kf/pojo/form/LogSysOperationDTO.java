package com.kf.pojo.form;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@HeadRowHeight()
@ColumnWidth()
@ContentRowHeight()
public class LogSysOperationDTO {
    @ExcelProperty({"请求人"})
    @TableField("request_user")
    private String requestUser;
    @ExcelProperty({"用户操作"})
    private String operation;
    @ExcelProperty({"请求地址"})
    private String requestUri;
    @ExcelProperty({"请求方法"})
    @TableField("request_method")
    private String requestMethod;
    @ExcelProperty({"请求参数"})
    @TableField("request_params")
    private String requestParams;
    @ExcelProperty({"请求时长(毫秒)"})
    private Integer requestTime;




}
