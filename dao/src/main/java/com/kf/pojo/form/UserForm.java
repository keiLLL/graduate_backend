package com.kf.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserForm {
    @NotNull(groups = {Save.class, Update.class})
    @Length(max = 10,message = "用户名不存在")
    private String username;
    @NotNull
    //密码的强度为：包含大小写字母和数字的组合，不能使用特殊字符，长度在8-20之间。
    @Pattern(message = "包含字母和数字的组合，不能使用特殊字符，长度在6-16之间",regexp = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$",groups = {Login.class})
//    @Length(min = 6,max = 16)
    private String password;

    private int status;
    @Pattern(message = "手机号码格式有误",regexp = "^1(3\\d|4[5-9]|5[0-35-9]|6[567]|7[0-8]|8\\d|9[0-35-9])\\d{8}$")
    @ApiModelProperty(value = "用户手机号")
    private String phone;

    public interface Save {
    }
    public interface First {
    }
    public interface Login {
    }



    /**
     * 更新的时候校验分组
     */
    public interface Update {
    }
}
