package com.kf.pojo.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class RecordForm {
    //前端commitRecord提交的时候不用这个数据
    //修改的时候才用
    @ApiModelProperty(value = "指导记录id，修改时用到")
    private Integer recordsId;
//
//    @ApiModelProperty(value = "指导教师教职工号 stu")
//    @Length(max = 20)
//    private String reTeacher;

    @ApiModelProperty(value = "指导收获 stu")
    @Length(max = 500)
    private String reHarvest;

    @ApiModelProperty(value = "对指导老师建议 stu")
    @Length(max = 100)
    private String reAdvise;

    @ApiModelProperty(value = "指导目标(老师填写被指导的学生学号)")
    @Length(max = 50)
    private String reTarget;

    @ApiModelProperty(value = "学生学习情况")
    @Length(max = 200)
    private String stuSituation;

    @ApiModelProperty(value = "指导内容 stu,tea")
    @Length(max = 500)
    private String reImfor;

}
