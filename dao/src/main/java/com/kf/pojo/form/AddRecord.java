package com.kf.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class AddRecord {
    //只在修改指导记录的时候使用，提交时不用
    int recordsId;

    @Length(max = 20)
    @ApiModelProperty(value = "前端获取表单里的学生学号传进来")
    String susername;


    @ApiModelProperty(value = "学生学习情况")
    @Length(max = 200)
    private String stuSituation;

    @ApiModelProperty(value = "指导内容")
    @Length(max = 500)
    private String reImfor;


}
