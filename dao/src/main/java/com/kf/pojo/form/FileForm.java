package com.kf.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class FileForm {
    @Length(max = 60)
    String path;
    @Length(max = 10)
    String suffix;
    @NotNull
    @Length(max = 50)
    String filename;
}
