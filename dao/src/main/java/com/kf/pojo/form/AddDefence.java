package com.kf.pojo.form;


import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class AddDefence {
    int id;
    @Length(max = 20)
    @NotNull
    String teaName;
    @NotNull
    String startTime;
    @NotNull
    @Length(max = 40)
    String place;
    @Length(max = 40)
    String teaname1;
    @Length(max = 40)
    String teaname2;
    @Length(max = 40)
    String teaname3;
    @Length(max = 40)
    String teaname4;

}
