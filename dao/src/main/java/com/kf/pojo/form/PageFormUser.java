package com.kf.pojo.form;

import com.kf.pojo.User;
import lombok.Data;

import java.util.List;
@Data
public class PageFormUser {
    List<User> list;
    Long  total;
}
