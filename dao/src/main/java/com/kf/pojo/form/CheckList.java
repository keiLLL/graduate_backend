package com.kf.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CheckList<T> {
    /**
     * 审核状态
     */
    @NotNull
    Integer status;
    /**
     * 审核列表
     */
    List<T> list;
    /**
     * 建议
     */
    @Length(max = 500)
    String advise;
}
