package com.kf.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
@Data
public class TeacherForm {
    private String username;
    @NotNull
    private int userId;
    @Pattern(message = "包含大小写字母和数字的组合，不能使用特殊字符，长度在8-16之间",regexp = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,20}$",groups = TeacherForm.Tteacher.class)
    @Pattern(message = "包含大小写字母和数字的组合，不能使用特殊字符，长度在6-20之间",regexp = "^.{6,20}$",groups = TeacherForm.AdminTeacher.class)
    private String password;
    @NotNull
    @Length(min = 1,max = 10)
    private String teaName;
    @Length(max = 1,min = 1)
    private String teaSex;
    @NotNull
    private String teaRecord;
    @NotNull
    @Length(max = 50)
    private String teaPost;
    @Pattern(message = "手机号码格式有误",regexp = "^1(3\\d|4[5-9]|5[0-35-9]|6[567]|7[0-8]|8\\d|9[0-35-9])\\d{8}$")
    private String teaPhone;
    @NotNull
    private String teaDepartment;
    @Pattern(message = "邮箱格式有误",regexp = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$")
    private String teaEmail;
    @Length(max = 40)
    private String teaWx;
    @NotNull
//    @Length(max = 1,min = 1)
    private int state;
    private int status;
    public interface AdminTeacher{

    }
    public interface Tteacher{

    }


}
