package com.kf.pojo.form;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentForm{
    private String username;
    @NotNull
    @Length(min = 1,max = 10)
    private String stuName;
    @Length(max = 1,min = 1)
    private String stuSex;
    @Pattern(message = "包含大小写字母和数字的组合，不能使用特殊字符，长度在8-20之间",regexp = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,20}$",groups = StudentForm.Sstduent.class)
    @Pattern(message = "包含大小写字母和数字的组合，不能使用特殊字符，长度在6-20之间",regexp = "^.{6,20}$",groups = StudentForm.AdminStudent.class)
    private String password;
    @NotNull
    private int userId;
//    @Pattern(message = "只能输入汉字",regexp = "^[\\u4e00-\\u9fa5]{0,}$ \n")
    private String stuDepartment;
//    @Pattern(message = "只能输入汉字",regexp = "^[\\u4e00-\\u9fa5]{0,}$ \n")
    private String stuProfessional;
    @Pattern(message = "手机号码格式有误",regexp = "^1(3\\d|4[5-9]|5[0-35-9]|6[567]|7[0-8]|8\\d|9[0-35-9])\\d{8}$")
    private String stuPhone;
    @Pattern(message = "邮箱格式有误",regexp = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$")
    private String stuMail;
    private String stuClass;
    @NotNull
//    @Length(max = 1,min = 1)
//    private int state;
    private int status =1;
    private String stuWx;
    @Pattern(message = "qq格式有误",regexp = "[1-9][0-9]{4,}")
    private String stuQq;
    @Length(min = 1,max=30)
    private String stuTitle;
    private int state;
    public interface AdminStudent{

    }
    public interface Sstduent{

    }

}
