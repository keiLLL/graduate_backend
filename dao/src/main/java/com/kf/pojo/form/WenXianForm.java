package com.kf.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;
@Data
public class WenXianForm {

//    MultipartFile file;
    @Length(max = 400)
    String describe;
}
