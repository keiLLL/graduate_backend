package com.kf.pojo.form;

import com.kf.pojo.Notice;
import com.kf.pojo.Noticefile;
import lombok.Data;


import java.util.List;

@Data
public class NoticeFileForm {
    Notice notice;
    List<Noticefile> noticefiles;
}
