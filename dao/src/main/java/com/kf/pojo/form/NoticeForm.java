package com.kf.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class NoticeForm {
    private String id;
    @NotNull
    private String username ;
    private String createby;
    private String target ;
    private String noticeTopic ;
    private String noticeContent ;
    private String createtime ;
}
