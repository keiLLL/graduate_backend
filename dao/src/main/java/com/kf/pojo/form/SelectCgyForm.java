package com.kf.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
@Data
public class SelectCgyForm {


    /**
     *  //展示、模糊查询所有老师的课题,多条件模糊查询
     */
    @Length(max = 50)
    private String cgyName;
    @Length(max = 50)
    private String cgyDepart;
    @Length(max = 20)
    private String tusername;
    private int cgySource;
    /**
     * // 审核选择（多条件：已审核、审核驳回、审核通过）模糊查询
     */
    //学生端那个show不用这个
    private int status;
    @Length(max = 20)
    //学生端那个show不用这个
    private String susername;
    @NotNull
    int current;
    @NotNull
    int size;



}
