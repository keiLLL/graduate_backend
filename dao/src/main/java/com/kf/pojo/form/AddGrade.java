package com.kf.pojo.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class AddGrade {
    //指导老师登录进账号 应该是先展示出所有学生，然后为这些学生填写成绩，这个应该绑定表单里面的学号
    @NotNull
    String susername;
    @NotNull
    int grade;
    @NotNull
    int grade1;
    @NotNull
    int grade2;

}
