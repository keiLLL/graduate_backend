package com.kf.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class ChoMyForm {
    @Length(max = 20)
    private String susername;
    private int cgySource;
    private int status;
    @NotNull
    int current;
    @NotNull
    int size;
}
