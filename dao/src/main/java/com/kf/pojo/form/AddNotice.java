package com.kf.pojo.form;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import java.util.Date;
@Data
@Accessors(chain = true)
public class AddNotice {
//    @ApiModelProperty(value = "用户名")
//    @Length(max = 20)
//    private String username;
//    @Length(max = 20)
//    private String createby;

    private  String id;

    @ApiModelProperty(value = "对象")
    private int target;
    @ApiModelProperty(value = "主题")
    @Length(max =50)
    private String noticeTopic;
//    @Length(max =1000)
    @ApiModelProperty(value = "公告内容")
    private String noticeContent;
//
//    @ApiModelProperty(value = "创建时间")
//    private Date gmtCreate;



}
