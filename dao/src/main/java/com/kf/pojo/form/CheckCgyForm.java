package com.kf.pojo.form;

import lombok.Data;

import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotNull;

@Data
public class CheckCgyForm {
    @NotNull
    private int cgyId;
    @NotNull
    private int cgyStatus;
    @Length(max=300)
    private String cgyAdvise;
}
