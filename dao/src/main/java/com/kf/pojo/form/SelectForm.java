package com.kf.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class SelectForm {
    @Length(max = 10)
    String name;
    @NotNull
    int current;
    @NotNull
    int size;


}
