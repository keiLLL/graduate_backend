package com.kf.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportForm {
    @ApiModelProperty(value = "选题意义或前景")
    @Length(max = 1000)
    @NotNull
    private String prospect;
    @Length(max = 1000)
    @NotNull
    @ApiModelProperty(value = "研究方法路线")
    private String route;
    @Length(max = 1000)
    @NotNull
    @ApiModelProperty(value = "总体安排计划")
    private String plan;

}
