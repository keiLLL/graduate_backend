package com.kf.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class SelectRecordForm {
    @Length(max = 20)
    String username;
    @NotNull
    int current;
    @NotNull
    int size;
    int state;
}
