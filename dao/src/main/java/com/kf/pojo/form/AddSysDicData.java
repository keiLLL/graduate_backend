package com.kf.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class AddSysDicData {
    private int id;

    @NotNull
    private int dictTypeId;
    @Length(max = 255)
    private String dictLabel;
    @Length(max = 255)
    private String dictValue;
    @Length(max = 255)
    private String remark;
    int sort;
    @Length(max = 20)
    private String username;
}
