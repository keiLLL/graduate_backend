package com.kf.pojo.form;

import com.kf.pojo.Role;
import com.kf.pojo.User;
import lombok.Data;

import java.util.List;
@Data
public class PageFormRole {
    List<Role> list;
    Long  total;
}
