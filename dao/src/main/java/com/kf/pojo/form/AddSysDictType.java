package com.kf.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class AddSysDictType {
    //修改的时侯用
    private  int id;


    @Length(max = 100)
    private String dictType;
    @Length(max = 255)
    private String dictName;
    @Length(max = 255)
    private String remark;
    private int sort;
    @Length(max = 20)
    private String username;

}
