package com.kf.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChoMyCgy {
    private int cgyId;
    @ApiModelProperty(value = "课题名")
    private String cgyName;
    @ApiModelProperty(value = "选择该课题的学生学号")
    private String susername;
    @ApiModelProperty(value = "选择该课题的学生姓名")
    private String stuname;
    @ApiModelProperty(value = "所属院系")
    private String cgyDepart;
    @ApiModelProperty(value = "课题来源")
    private int cgySource;
    @ApiModelProperty(value = "审核状态")
    private int cgyStatus;
}
