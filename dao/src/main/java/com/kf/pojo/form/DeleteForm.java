package com.kf.pojo.form;

import lombok.Data;

import java.util.List;
@Data
public class DeleteForm {
    List<Integer> list;
}
