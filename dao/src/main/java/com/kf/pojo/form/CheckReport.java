package com.kf.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
@Data
public class CheckReport {
    @NotNull
    private int id;
    @NotNull
    private int status;
    @Length(max=300)
    private String advise;
}
