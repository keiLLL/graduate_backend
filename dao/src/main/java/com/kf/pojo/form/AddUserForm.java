package com.kf.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddUserForm {
    @NotNull
    @Length(max = 10)
    private String username;
    @NotNull
//    @Length(min =6, max = 20)
    //密码的强度为：包含大小写字母和数字的组合，不能使用特殊字符，长度在8-20之间。
//    @Pattern(message = "包含大小写字母和数字的组合，不能使用特殊字符，长度在8-20之间",regexp = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,20}$",groups = User.class)
    @Pattern(message = "包含大小写字母和数字的组合，不能使用特殊字符，长度在6-20之间",regexp = "^.{6,20}$",groups = Admin.class)
    private String password;
    @Length(min = 2,max = 10)
    private String name;
    @ApiModelProperty(value = "所属院系")
    private String department;
    @Pattern(message = "手机号码格式有误",regexp = "^1(3\\d|4[5-9]|5[0-35-9]|6[567]|7[0-8]|8\\d|9[0-35-9])\\d{8}$")
    @ApiModelProperty(value = "用户手机号")
    private String phone;
    @Pattern(message = "邮箱格式有误",regexp = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$")
    @ApiModelProperty(value = "用户邮箱")
    private String email;
    @ApiModelProperty(value = "身份标识")
//    @Length(max=3)
    private int state;
    @ApiModelProperty(value = "性别")
//    @Length(max=3)
    private String  sex;

    public  interface Admin{

    }
    public  interface User{

    }
}
