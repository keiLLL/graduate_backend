package com.kf.pojo.form;

import com.kf.pojo.Category;
import com.kf.pojo.Role;
import lombok.Data;

import java.util.List;
@Data
public class PageFormCgy {
    List<Category> list;
    Long  total;
}
