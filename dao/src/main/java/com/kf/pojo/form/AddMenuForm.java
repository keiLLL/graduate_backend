package com.kf.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class AddMenuForm {
    //修改的时候使用
    int permId;

    @NotNull
    int bton;
    @Length(max = 20)
    String permsName;
    @Length(max = 40)
    String path;
    @Length(max = 40)
    String perms;
    @Length(max = 60)
    String icon;
    @NotNull
    int parentId;
//    @Length(max = 20)
//    String parentName;
}
