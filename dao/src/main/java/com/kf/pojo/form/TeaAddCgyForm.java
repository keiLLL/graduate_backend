package com.kf.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
@Data
public class TeaAddCgyForm {
    //只有在updateMyCgy修改的时候使用
    private Integer cgyId;

    @ApiModelProperty(value = "课题名")
    @NotNull
    @Length(min = 1,max = 20)
    private String cgyName;

    @ApiModelProperty(value = "系别")
    private String cgyDepart;
    @NotNull
    @ApiModelProperty(value = "题目类型")
    private String cgyType;
//    @NotNull
//    private Integer cgySource;
//    @NotNull
//    @ApiModelProperty(value = "指导老师教职工号")
//    @Length(max = 20)
//    private String tusername;
//    @NotNull
//    @Length(max = 30)
//    @ApiModelProperty(value = "指导老师名字")
//    private String teaName;
    @NotNull
    @Length(max = 400)
    @ApiModelProperty(value = "课题描述")
    private String describes;

}
