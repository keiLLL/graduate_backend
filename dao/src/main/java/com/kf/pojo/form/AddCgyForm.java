package com.kf.pojo.form;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class AddCgyForm {


    @ApiModelProperty(value = "课题名")
    @NotNull(message = "不能为空")
    @Length(min = 1,max = 20)
    private String cgyName;
    @NotNull(message = "不能为空")
    @ApiModelProperty(value = "系别")
    private String cgyDepart;
    @NotNull(message = "不能为空")
    @ApiModelProperty(value = "题目类型")
    private String cgyType;
//    @NotNull
//    private Integer cgySource;
    @NotNull(message = "不能为空")
    @ApiModelProperty(value = "指导老师教职工号")
    @Length(max = 40)
    private String tusername;
//    @NotNull
//    @Length(max = 30)
//    @ApiModelProperty(value = "指导老师名字")
//    private String teaName;
    @NotNull(message = "不能为空")
    @Length(max = 400)
    @ApiModelProperty(value = "课题描述")
    private String describes;


}
