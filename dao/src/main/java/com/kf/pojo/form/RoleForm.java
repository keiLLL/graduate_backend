package com.kf.pojo.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleForm {

    private int id;
    @NotNull
    @Length(max = 10,min = 1)
    private String name;
    @NotNull
    private String remark;
}
