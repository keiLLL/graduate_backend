package com.kf.pojo.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
@Data
public class CheckWenForm {

    private int id;

    private int status;
    @Length(max = 500)
    private String advise;
}
