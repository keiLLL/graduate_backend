package com.kf.pojo;

/**
 * 工厂对象工厂模式
 *
 *///
public class FactoryObject {
    public static Student createStudent() {
        return new Student();
    }

    public static Teacher createTeacher() {
        return new Teacher();
    }



    public static People createPeople(String type) {
        switch (type) {
            case "student":
                return new Student();
            case "teacher":
                return new Teacher();
            default:
                return null;
        }
    }
}
