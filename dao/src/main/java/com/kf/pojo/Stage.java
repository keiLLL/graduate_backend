package com.kf.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class Stage implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "学号")
    @TableId(value = "stu_id", type = IdType.AUTO)
    private int stuId;

    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private int userId;

    @ApiModelProperty(value = "阶段id")
//    @TableField("stage_id")
    private int activeStep;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @ApiModelProperty(value = "乐观锁")
    @Version
    private Integer version;

    @ApiModelProperty(value = "逻辑删除")
    @TableLogic
    private Integer deleted=0;
}
