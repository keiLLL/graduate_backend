package com.kf.pojo.iterator;

import com.kf.pojo.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentAggregateImpl implements StudentAggregate {

    private List<Student> list = new ArrayList<Student>();  // 学生列表



    @Override
    public void removeStudent(Student student) {
        this.list.remove(student);
    }

    @Override
    public void addStudent(Student student) {

    }

    @Override
    public StudentIterator getStudentIterator() {
        return new StudentIteratorImpl(list);
    }
}
