package com.kf.pojo.iterator;

import com.kf.pojo.Student;

public interface StudentAggregate {
    void addStudent(Student student);

    void removeStudent(Student student);

    StudentIterator getStudentIterator();
}
