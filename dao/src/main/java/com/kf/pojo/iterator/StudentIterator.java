package com.kf.pojo.iterator;

import com.kf.pojo.Student;

public interface StudentIterator {
    boolean hasNext();
    Student next();
}
