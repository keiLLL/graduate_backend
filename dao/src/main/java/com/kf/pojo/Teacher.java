package com.kf.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Teacher对象", description="教师表")
public class Teacher extends People  implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "tea_id", type = IdType.AUTO)
    private Integer teaId;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "教师姓名")
    private String teaName;

    @ApiModelProperty(value = "教师性别")
    private String teaSex;

    @ApiModelProperty(value = "教师学历")
    private String teaRecord;

    @ApiModelProperty(value = "教师职称")
    private String teaPost;

    @ApiModelProperty(value = "教师电话")
    private String teaPhone;

    @ApiModelProperty(value = "系别")
    private String teaDepartment;

    @ApiModelProperty(value = "邮箱号")
    private String teaEmail;

    @ApiModelProperty(value = "教师微信")
    private String teaWx;

    @ApiModelProperty(value = "首次登陆状态")
    private int teaStatus;



    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtModified;

    @ApiModelProperty(value = "乐观锁")
    @TableField("VERSION")
    private Integer version;

    @ApiModelProperty(value = "逻辑删除")
    @TableLogic
    private Integer deleted=0;


}
