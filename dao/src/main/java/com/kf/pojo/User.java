package com.kf.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;


import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class User implements Serializable{
    @TableId("user_id")
    private int userId;
    @NotNull
    @Length(max = 10)
    private String username;
    @NotNull
    @Length(min = 6, max = 20)
    private String password;
    private int state;
    private String name;
    private String sex;
    private String department;
    private String phone;
    private String email;
    private int status;
    @TableField(exist = false)
    private String salt;
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtModified;

    @ApiModelProperty(value = "乐观锁")
    @Version
    private Integer version=1;

    @ApiModelProperty(value = "逻辑删除")
    @TableLogic
    private Integer deleted=0;

    @TableField(exist = false)
    private List<Role> roleList;

    @TableField(exist = false)
    private Student student;

    @TableField(exist = false)
    private Teacher teacher;

    public User(String username,String password){
        this.username=username;
        this.password=password;
    }

    public void adaptee(){
        System.out.println();
    }
}
