package com.kf.config;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import static cn.hutool.crypto.CipherMode.decrypt;


/**
 * rsa跑龙套
 */
@Configuration
public class RsaUtil {
    private static final Logger logger = LoggerFactory.getLogger(RsaUtil.class);
    @Resource
    RedisUtil redisUtil;
    /**
     * 放置公钥与私钥
     *
     * volatile 保证当前的map在主内存中保存最新值
     *
     */
    private static volatile Map<String, String> keyMap = new HashMap<String, String>(2);
    @PostConstruct
    public  void generate() throws Exception {
         //生成公钥和私钥
         genKeyPair();
         String aPublic = keyMap.get("public");
         String aPrivate = keyMap.get("private");
         redisUtil.set("public",aPublic);
         redisUtil.set("private",aPrivate);

     }
    /**
     * 随机生成密钥对
     *
     */
    public  void genKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
        keyPairGen.initialize(1024, new SecureRandom());
        KeyPair keyPair = keyPairGen.generateKeyPair();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        String publicKeyString = new String(Base64.encodeBase64(publicKey.getEncoded()));
        String privateKeyString = new String(Base64.encodeBase64((privateKey.getEncoded())));
        keyMap.put("public", publicKeyString);
        keyMap.put("private", privateKeyString);
        redisUtil.set("public", publicKeyString);
        redisUtil.set("private", privateKeyString);
    }
    /**
     * 获取公钥
     *
     * @return
     */
    public String getPublicKey() {

        String publicKey = keyMap.get("public");
        if (publicKey == null) {
            // 加锁 防止多线程生产多个密钥
            synchronized (keyMap.getClass()) {
                if (publicKey == null) {
                    try {
                        genKeyPair();
                        publicKey = keyMap.get("public");
                    } catch (NoSuchAlgorithmException e) {
                        logger.error("获取随机公私钥错误", e);
                    }
                }
            }
        }
        return publicKey;
    }

    /**
     * 获取私钥
     *
     * @return 私钥
     */
    private  String getPrivateKey() {
//        String privateKey = keyMap.get("private");
        String privateKey = (String) redisUtil.get("private");

        if (privateKey == null) {
            logger.error("未获取私钥!!!");
        }
        return privateKey;
    }


    /**
     * RSA公钥加密
     *
     * @param str       加密字符串
     * @param publicKey 公钥
     * @return 密文
     * @throws Exception 加密过程中的异常信息
     */
    public  String encrypt(String str, String publicKey) throws Exception {
        //base64编码的公钥
        byte[] decoded = Base64.decodeBase64(publicKey);
        RSAPublicKey pubKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decoded));
        //RSA加密
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        String outStr = Base64.encodeBase64String(cipher.doFinal(str.getBytes("UTF-8")));
        return outStr;
    }

    /**
     * RSA私钥解密
     *
     * @param str 加密字符串
     * @return 铭文
     * @throws Exception 解密过程中的异常信息
     */
    public  String decrypt(String str) {
        String outStr = "";
        //64位解码加密后的字符串
        byte[] inputByte = new byte[0];
        try {
            inputByte = Base64.decodeBase64(str.getBytes("UTF-8"));

            //base64编码的私钥
//            System.out.println(getPrivateKey());
            byte[] decoded = Base64.decodeBase64(getPrivateKey());
            RSAPrivateKey priKey = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(decoded));
            //RSA解密
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, priKey);
            outStr = new String(cipher.doFinal(inputByte));
        } catch (UnsupportedEncodingException e) {
            logger.error("不支持的编码", e);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            logger.error("未找到找到指定算法", e);
        } catch (InvalidKeyException e) {
            logger.error("无效的秘钥", e);
        } catch (NoSuchPaddingException e) {
            logger.error("请求特定填充机制, 但该环境中未提供时", e);
        } catch (BadPaddingException e) {
            logger.error("预期对输入数据使用特定填充机制, 但未正确填充数据", e);
        } catch (InvalidKeySpecException e) {
            logger.error("无效的密钥规范", e);
        } catch (IllegalBlockSizeException e) {
            logger.error("非法的块大小", e);
        }
        return outStr;
    }

}

