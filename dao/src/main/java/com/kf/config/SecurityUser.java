

package com.kf.config;

import com.kf.pojo.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

public class SecurityUser {

    public static Subject getSubject() {
        try {
            return SecurityUtils.getSubject();
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 获取用户信息
     */
    public static User getUser() {
        Subject subject = getSubject();
        if(subject == null){
            return new User();
        }

        User user = (User)subject.getPrincipal();
        if(user == null){
            return new User();
        }

        return user;
    }


    /**
     * 获取用户ID
     */
    public static int getUserId() {
        return getUser().getUserId();
    }

}