//package com.kf;
//
//import com.alibaba.nacos.api.config.annotation.NacosValue;
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.kf.config.RedisUtil;
//import com.kf.config.RsaUtil;
//import com.kf.controller.LoginController;
//import com.kf.mapper.*;
//import com.kf.pojo.*;
//import com.kf.pojo.form.AddMenuForm;
//import com.kf.pojo.form.AddUserForm;
//import com.kf.service.*;
//
//import com.kf.util.JsonUtils;
//import com.kf.util.MD5Utils;
//import com.kf.util.R;
//import com.kf.util.ResultCode;
//import lombok.ToString;
//import org.jasypt.encryption.StringEncryptor;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import javax.annotation.Resource;
//import java.util.*;
//import java.util.stream.Collectors;
//
//@SpringBootTest
//class ControllerApplicationTests {
//
////    @Test
////    void contextLoads() {
////
////        System.out.println(MD5Utils.encrypt("2020110223", "123456"));
////        System.out.println(MD5Utils.encrypt("student", "123456"));
////        System.out.println(MD5Utils.encrypt("teacher", "123456"));
////        System.out.println(MD5Utils.encrypt("123123", "123456"));
////        System.out.println(MD5Utils.encrypt("test", "123456"));
////        System.out.println(MD5Utils.encrypt("2020110228", "123456"));
////        System.out.println(MD5Utils.encrypt("xuqian", "123456"));
////        System.out.println(MD5Utils.encrypt("xuqian2", "123456"));
////        System.out.println(MD5Utils.encrypt("xuqian3", "123456"));
////
////    }
//    @Autowired
//    UserService userService;
//    @Autowired
//    RedisUtil redisUtil;
//    @Autowired
//    NoticeMapper noticeMapper;
//    @Test
//    void contextLoads2() {
////        String pagekeys="current:"+2;
////        if(redisUtil.get(pagekeys)!=null){
////            List<User> o = (List<User>)redisUtil.get(pagekeys);
////            o.stream().forEach(System.out::println);
////        }else{
////            boolean b = redisUtil.hasKey("com.kf.mapper.UserMapper");
////            if (b){
////                redisUtil.del("com.kf.mapper.UserMapper");
////            }
//            IPage<User> userIPage = userService.pageUser("",1, 4);
////            redisUtil.set(pagekeys,userIPage.getRecords());
//            System.out.println(userIPage);
//            userIPage.getRecords().forEach(System.out::println);
////        }
//
//    }
////    @Autowired
////    RoleService roleService;
////    @Test
////    void contextLoads3() {
//////        List<User> userList1 = userService.selectAllUser();
//////        userList1.forEach(System.out::println);
////        List<User> userList = userService.selectUserByName("康");
////        userList.stream().forEach(System.out::println);
////    }
////    @Test
////    void contextLoads4() {
//////        Role role = roleService.findRoleUser(1);
//////        List<UserRole> userList = role.getUserList();
//////        userList.forEach(System.out::println);
////    }
////    @Autowired
////    UserRoleMapper userRoleMapper;
////
////    @Test
////    void contextLoads5() {
////        HashMap<String,Object> hashMap = new HashMap<>();
////        hashMap.put("user_id",1);
////        userRoleMapper.selectByMap(hashMap).forEach(System.out::println);
////    }
//    @Autowired
//    StudentService studentService;
//    @Autowired
//    TeacherService teacherService;
//    @Autowired
//    AdminService adminService;
//
//    @Autowired
//    StudentMapper studentMapper;
//    @Autowired
//    TeacherMapper teacherMapper;
//    @Autowired
//    TreePermMapper treePermMapper;
//    @Autowired
//    AdminMapper adminMapper;
//    @Autowired
//    PermissionMapper permissionMapper;
//    void insertTreeperm(Permission permission){
//        TreePerm treePerm = new TreePerm();
//        //先把这个权限插入自身数据 例 2 0 0 2
//        treePerm.setAncestorsKey(permission.getPermId());
//        treePerm.setDistance(0);
//        treePerm.setMemberKey(permission.getPermId());
//        int insert = treePermMapper.insert(treePerm);
//        if(permission.getParentId()==0){
//            return;
//        }
//        //插入本身与父节点的关系  1 0 0 2
//        treePerm.setAncestorsKey(permission.getParentId());
//        treePerm.setDistance(1);
//        treePerm.setMemberKey(permission.getPermId());
//        treePermMapper.insert(treePerm);
//        //通过连表查询到所有上级
//        List<TreePerm> allParentByid = treePermMapper.findAllParentByid(permission.getParentId());
//        for (TreePerm tree : allParentByid) {
//            TreePerm treePerm1 = new TreePerm();
//            treePerm1.setMemberKey(permission.getPermId());
//            treePerm1.setDistance(tree.getDistance()+1);
//            treePerm1.setAncestorsKey(tree.getAncestorsKey());
//            int insert1 = treePermMapper.insert(treePerm1);
//        }
//
//    }
//    @Test
//    void contextLoads6() {
//
////
////            QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
////            queryWrapper.eq("user_id",27);
////            Student student = studentMapper.selectOne(queryWrapper);
////            System.out.println(student);
////            QueryWrapper<Teacher> queryWrapper1 = new QueryWrapper<>();
////            queryWrapper.eq("user_id",21);
////            Teacher teacher = teacherMapper.selectOne(queryWrapper1);
////            System.out.println(teacher);
////            QueryWrapper<Admin> queryWrapper2 = new QueryWrapper<>();
////            queryWrapper.eq("user_id",20);
////            Admin admin = adminMapper.selectOne(queryWrapper2);
////            System.out.println(admin);
//
////        Role role = roleService.getRole(11);
////        System.out.println(role);
//
//
////        int[] ints ={4,5,7,8,9,10,11,12,13,14,15,16,48,17};
////        int[] ints ={18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47};
////        int[] ints={53,54};
////        int[] ints={56,57,58};
////
////        for (int anInt : ints) {
////            Permission permission = permissionMapper.selectById(anInt);
////            insertTreeperm(permission);
////        }
//
//
//
//    }
////    @Test
////    void contextLoads7() {
////
////        String str = "房估字(2014)第YPQD0006号";
////        String jieguo = str.substring(str.indexOf("第")+1,str.indexOf("号"));
////        System.out.println(jieguo);
////        String a = "敏感词";
////        String b = "我的敏感词";
////        System.out.println(b.contains(a));
////        if (b.contains(a)) {
////            System.out.println("有敏感词");
////        }
////
////        User user = new User("12", "123");
////        AddUserForm addUserForm = new AddUserForm();
////        String string = JsonUtils.toJsonString(addUserForm);
////        System.out.println(string);
////        String pass="password";
////        System.out.println(string.contains(pass));
////        String[] passwords = string.split("password");
////        String[] split1 = passwords[0].split(",");
////        String s1=split1[0];
////        for (int i = 1; i < split1.length-1; i++) {
////            s1=s1+","+split1[i];
////        }
////        System.out.println(s1);
////        System.out.println("=======================================");
////        String s = null;
////
////        String[] split = passwords[1].split(",");
////        s=split[1];
////        for (int i = 2; i < split.length; i++) {
////               s=s+","+split[i];
////        }
////
////        System.out.println(s);
////
////        System.out.println("=======================================");
////        System.out.println(s1+","+s);
////        String depass=s1+",\"password\":\"*****\","+s;
////        System.out.println(depass);
////    }
//
//    @Autowired
//    PermissionService permissionService;
//    @Autowired
//    UserRoleService userRoleService;
//    @Autowired
//    RolePermsService rolePermsService;
////    @Test
////    List<Permission>  contextLoads8(){
//////        User user = userService.queryUserByName(LoginController.username);
////        List<Permission> permissions = new ArrayList<>();
////        List<UserRole> userRole = userRoleService.getUserRole(37);
////        for (UserRole userrole : userRole) {
////            System.out.println("角色id"+userrole.getRoleId());
//////            roleService
////            Role role = roleService.getRole(userrole.getRoleId());
////            //角色权限表 通过角色id 拿到当前 角色-权限这一行数据(可能是多行数据，因为多对多)
////
////            List<RolePerms> perms = rolePermsService.getPerms(userrole.getRoleId());
////            for (RolePerms perm : perms) {
////                System.out.println("权限id"+perm.getPermId());
////                //权限表 通过权限id 拿到权限这一行数据
////                Permission permission = permissionService.getPermsName(perm.getPermId());
////                permissions.add(permission);
////            }
////
////        }
////        System.out.println("111");
////        permissions.forEach(System.out::println);
////        return permissions;
////    }
//
////    private Permission findChildren(Permission permission, List<Permission> list) {
////
////        List children = new ArrayList<Permission>();
////
////        for (Permission permission1 : list) {
////
////            if (permission1.getParentId()==permission.getPermId()) {
////                //递归调用
////                children.add(findChildren(permission1, list));
////
////            }
////
////        }
////        permission.setChildren(children);
////        return permission;
////
////    }
////    @Test
////    public List listToTree2() {
////        List<Permission> permissions = contextLoads8();
////        List<Permission> tree = new ArrayList<Permission>();
////        for (Permission permission : permissions) {
////            if (permission.getParentId() ==3) {
////                tree.add(findChildren(permission, permissions));
////            }
////        }
////        for (Permission o : tree) {
////            System.out.println(o);
////        }
////        System.out.println(tree);
////        return tree;
////
////    }
////    @Autowired
////    LogSysOperationMapper logSysOperationMapper;
////    @Autowired
////    LogLoginMapper logLoginMapper;
////    @Test
////    void contextLoads9() {
////        LogSysOperation logSysOperation = new LogSysOperation();
////        logSysOperation.setStatus(1);
////        logSysOperationMapper.insert(logSysOperation);
////        LogLogin logLogin = new LogLogin();
////        logLogin.setStatus(1);
////        logLoginMapper.insert(logLogin);
////    }
////    @Autowired
////    ChoCgyMapper choCgyMapper;
////    @Test
////    void contextLoads10() {
////        QueryWrapper<ChoCgy> choCgyQueryWrapper = new QueryWrapper<>();
////        choCgyQueryWrapper.eq("susername","admin");
////        ChoCgy choCgy = choCgyMapper.selectOne(choCgyQueryWrapper);
////        System.out.println(choCgy==null);
////    }
//    @Autowired
//    RsaUtil rsaUtil;
//    @Test
//    void contextLoads11() throws Exception {
////        String encrypt = rsaUtil.encrypt("Admin1234", (String) redisUtil.get("public"));
////        System.out.println("加密之后："+encrypt);
////        System.out.println(redisUtil.get("public"));
////        String decrypt = rsaUtil.decrypt("Y5gpUmpiwB/flK63wWpzXg4QxD0U7vRb9RKom9AskJzZa5Q1/NLZuH/QZFgPrX/xTRHU05u20Aqc0pEQQ/lZzSkh+Z5HbAJuruM4cKJmiuxR1oFkkjlrU0ju8FrfC3RABa7KBo8MA98bqqNmjDJoeEsqWyGdobt8BJjVgJ3BV18=");
////        System.out.println("还原之后："+decrypt);
//    }
//    @Autowired
//    SysDictTypeService sysDictTypeService;
////    @Test
////    void contextLoads12() throws Exception {
////        List<SysDictType> sysDictTypes = sysDictTypeService.selectData();
////        System.out.println(sysDictTypes);
////    }
////    @Test
////    void contextLoads12() throws Exception {
////        List<Permission> permissionList = permissionService.selectAllXia(3);
////        List<Permission> perIdList=new ArrayList<>();
////        List<Permission> list = permissionService.treeToList(permissionList,perIdList);
////        for (Permission permission : list) {
////            permission.setChildren(null);
////            System.out.println(permission);
////        }
////
//    //    }
////@NacosValue(value = "${ams.testContent}", autoRefreshed = true)
////String myNacosDemo;
//
//    @Test
//    void contextLoads12() throws Exception {
////        AddMenuForm addMenuForm = new AddMenuForm();
////        addMenuForm.setPermId(16);
////        addMenuForm.setParentId(3);
////        addMenuForm.setBton(0);
////        addMenuForm.setPermsName("日志管理1");
////        addMenuForm.setPerms("aaaa:aaaa");
////        addMenuForm.setPath("/aaaa/aaaa");
////        permissionService.updatePermision(addMenuForm);
//
////        System.out.println(myNacosDemo);
//    }
//
//    @Test
//    void contextLoads13() throws Exception {
//        User user = userService.queryUserByName("Admin");
//        System.out.println(user);
//    }
//    @Resource
//    StringEncryptor stringEncryptor;
//    @Test
//    void contextLoads14() throws Exception {
//        System.out.println(stringEncryptor.encrypt("jdbc:mysql://47.109.39.103:3306/graduate-system?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai&useSSL=false"));
//
//    }
//    @Test
//    void contextLoads15() throws Exception {
////        System.out.println(stringEncryptor.encrypt("Dishiyizu.5"));
////
////        List<Notice> list = noticeMapper.selectList(null);
//////        List<Notice> studentNotice = list.stream().filter(x -> x.getTarget() == 1).collect(Collectors.toList());
////
////        list.sort(Comparator.comparingDouble(Notice::getHeat));
////        Collections.reverse(list);
////        for (Notice notice : list) {
////            System.out.println(notice.getHeat());
////        }
//        Notice notice = noticeMapper.selectById(113);
//        long time = notice.getGmtModified().getTime();
//
//        long time1 = notice.getGmtModified().getTime();
//        double heattime = (time1 % 1000000000L) / 1000.0;
//        double heat = (heattime / 2 + 100 * 1000) / 10000;
//        System.out.println(">>>>>>>>>>>"+heattime);
//        System.out.println("----------"+heat);
//        double heat1 = notice.getHeat();
//        System.out.println("=====" + heat1);
////        notice.setHeat(heat + heat1);
////        noticeMapper.updateById(notice);
//        System.out.println(heat + heat1);
//
//    }
//}
//
