package com.kf;



import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
//@NacosPropertySource(dataId = "OSS-Config", autoRefreshed = true)
public class ControllerApplication {
    public static void main(String[] args) {

        SpringApplication.run(ControllerApplication.class, args);
    }

}
