package com.kf.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.kf.aop.LogOperation;

import com.kf.config.AliyunOSSUtil;
import com.kf.config.SecurityUser;
import com.kf.handler.MyException;
import com.kf.pojo.*;
import com.kf.pojo.form.*;
import com.kf.service.*;
import com.kf.util.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


/**
 * <p>
 *  前端控制器
 */
@RestController
@RequestMapping("/student")
@Api("学生")
public class StudentController {
    @Resource
    private AliyunOSSUtil aliyunOSSUtil;


    @Resource
    WenFileService wenFileService;
    @Resource
    StudentService studentService;
    @Resource
    UserService userService;
    @Resource
    ReportService reportService;
    @Resource
    RecordsService recordsService;

    @Resource
    CategoryService categoryService;
    @Resource
    PaperFileService paperFileService;
    @Resource
    ChoCgyRecordService choCgyRecordService;
    @Resource
    ChoCgyService choCgyService;
    @Resource
    DefenceService  defenceService;
    @Resource
    ReportFileService  reportFileService;

    @Resource
    MidtermDefenceService MidtermdefenceService;

    @Resource
    InspectionReportService inspectionReportService;

    @Resource
    StageService stageService;

    @GetMapping("/pos")
    public R dad() {
        return R.ok("vovo");
    }


    static User kei;
    /**
     * 选择自我
     *
     * @return {@link R}
     */
    @GetMapping("/self/info")
    @ApiOperation("查询个人信息")
//    @LogOperation("查询个人信息")
    public R selectSelf() {

        try {
            User user = SecurityUser.getUser();
            Student student = studentService.findById(user.getUserId());
            return R.okey(ResultCode.SUCCESS).put("data", student);
        } catch (Exception e) {
            e.printStackTrace();
            return R.error(ResultCode.USER_SELECT_ERROR);
        }
    }

    @PostMapping("/update/self/info")
    @ApiOperation("修改个人信息")
//    @LogOperation("修改个人信息")
    @Transactional(rollbackFor = Exception.class)
    public R updateSelf(@Validated(StudentForm.Sstduent.class) @RequestBody StudentForm studentForm) {

        try {
//            User user = SecurityUser.getUser();
            User user = LoginController.user1;
//            System.out.println("=========================user:"+user);

            if(!user.getPhone().equals(studentForm.getStuPhone())){
                User byPhone = userService.findByPhone(studentForm.getStuPhone());
                if(byPhone!=null){

                    return  R.error(ResultCode.STUDENT_ADD_ERROR2);

                }
            }

            Student student =studentService.findById(studentForm.getUserId());
            studentForm.setStatus(1);
            studentForm.setState(1);
            studentService.updateStudentSelf(user,student,studentForm);


        } catch (Exception e) {
            e.printStackTrace();
            return R.error(ResultCode.USER_UPDATE_ERROR);
        }

        return R.okey(ResultCode.SUCCESS);
    }

//    @PostMapping("/wenxian")
//    @ApiOperation("上传文献综述")
//    @Transactional(rollbackFor = Exception.class)
////    @LogOperation("上传文献综述")
//    public R comWenXian(@Validated @RequestParam("file") MultipartFile file) throws IOException {
//
//        String filename = file.getOriginalFilename();
//        System.out.println(filename + "==filename");
//        ResourceBundle resource = ResourceBundle.getBundle("oss");
//        String wenurl = resource.getString("wenurl");
//        User user = LoginController.user1;
//        WenFile wenFile1 = wenFileService.selectByUsername(user.getUsername());
//        if(wenFile1!=null){
//            if(wenFile1.getStatus()==2){
//                return R.error(ResultCode.STUDENT_ADVISE_ERROR2);
//            }
//        }
//        assert filename != null;
//        if (!"".equals(filename.trim())) {
//
//            String[] split =new String[2];
//            split[0]= filename.substring(0,filename.lastIndexOf("."));
//            split[1] = filename.substring(filename.lastIndexOf(".")+1,
//                    filename.length());
////            if(AliyunOSSUtil.ZIP.equals(split[1]))
//            if(split[1].equals("zip")){
//                System.out.println("类型正确");
//            }else{
//                return R.error(ResultCode.USER_UPFILE_ERROR);
//            }
//            String fileCode = aliyunOSSUtil.getFileCode();
//            try {
//                WenFile wenFile = new WenFile();
//                wenFileService.insert(wenFile, user, split, null,fileCode);
//            } catch (Exception e) {
//                return R.error(ResultCode.STUDENT_UP_ERROR);
//            }
//            ChoCgy choCgy = choCgyService.selectBySusername(user.getUsername());
//            if (choCgy==null){
//                return R.error(ResultCode.STUDENT_CHOOSE_ERROR10);
//            }else {
//                // 上传到OSS
//               aliyunOSSUtil.upLoad4(file, wenurl,fileCode,split[1]);
//            }
//
//        }
//
//        return R.okey(ResultCode.SUCCESS);
//    }
@PostMapping("/wenxian")
@ApiOperation("上传文献综述")
@Transactional(rollbackFor = Exception.class)
//    @LogOperation("上传文献综述")
public R comWenXian(@Validated @RequestParam("file") MultipartFile file) throws IOException {

    String filename = file.getOriginalFilename();
    System.out.println(filename + "==filename");
    ResourceBundle resource = ResourceBundle.getBundle("oss");
    String wenurl = resource.getString("wenurl");
    User user = LoginController.user1;
    WenFile wenFile1 = wenFileService.selectByUsername(user.getUsername());
    if(wenFile1!=null){
        if(wenFile1.getStatus()==2){
            return R.error(ResultCode.STUDENT_ADVISE_ERROR2);
        }
    }
    assert filename != null;
    if (!"".equals(filename.trim())) {

        String[] split =new String[2];
        split[0]= filename.substring(0,filename.lastIndexOf("."));
        split[1] = filename.substring(filename.lastIndexOf(".")+1,
                filename.length());
//            if(AliyunOSSUtil.ZIP.equals(split[1]))
        if(AliyunOSSUtil.TXT.equals(split[1])|| AliyunOSSUtil.DOCX.equals(split[1])|| AliyunOSSUtil.PDF.equals(split[1])|| AliyunOSSUtil.DOC.equals(split[1])|| AliyunOSSUtil.XLS.equals(split[1])){
            System.out.println("类型正确");
        }else{
            return R.error(ResultCode.USER_UPFILE_ERROR);
        }
        String fileCode = aliyunOSSUtil.getFileCode();
        try {
            WenFile wenFile = new WenFile();
            wenFileService.insert(wenFile, user, split, null,fileCode);
        } catch (Exception e) {
            return R.error(ResultCode.STUDENT_UP_ERROR);
        }
        ChoCgy choCgy = choCgyService.selectBySusername(user.getUsername());
        if (choCgy==null){
            return R.error(ResultCode.STUDENT_CHOOSE_ERROR10);
        }else {
            // 上传到OSS
            aliyunOSSUtil.upLoad3(file, wenurl,fileCode,split[1]);
        }

    }

    return R.okey(ResultCode.SUCCESS);
}
    @PostMapping("/down/wenxian")
//    @LogOperation("下载文献")
    @ApiOperation("下载文献")
    public R downWenXian(@NotNull @RequestParam("id")int id, HttpServletResponse response)  {
        try {

            WenFile wenFile = wenFileService.getById(id);
            //文件路径
            String path=wenFile.getPath()+"/"+wenFile.getFilecode()+"."+ wenFile.getSuffix();

            response.addHeader("Content-Disposition", "attachment;filename=" +new String((wenFile.getFilename()+"."+ wenFile.getSuffix()).getBytes("UTF-8"),"ISO-8859-1"));
            aliyunOSSUtil.exportOssFile(response.getOutputStream(),path);
            return R.okey(ResultCode.SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            return R.error(ResultCode.FAIL);
        }
    }

    @ApiOperation("展示自己的上传文献记录")
    @GetMapping("/show/wenxian")
//    @LogOperation("展示自己的上传文献记录")
    @Transactional(rollbackFor = Exception.class)
    public R showWenXian() {
        User user = LoginController.user1;
        WenFile wenFile = wenFileService.selectByUsername(user.getUsername());
        return R.okey(ResultCode.SUCCESS).put("data",wenFile);
    }

    @ApiOperation("删除自己的上传文献记录，同时删除文件")
//    @DeleteMapping("/delete/wenxian")
    @PostMapping("/delete/wenxian")
//    @LogOperation("删除自己的文献")
    @Transactional( rollbackFor= Exception.class)
    public R deleteWenxian() {
        try {
            User user = LoginController.user1;
            WenFile wenFile = wenFileService.selectByUsername(user.getUsername());
            if(wenFile==null){
                return R.error(ResultCode.STUDENT_CHOOSE_ERROR9);
            }
            if(wenFile.getStatus()==2){
                return R.error(ResultCode.STUDENT_DELETE_ERROR2);
            }
            wenFileService.deleteWenFile(user.getUsername());
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
        return R.okey(ResultCode.SUCCESS);
    }


    @ApiOperation("提交开题报告(提交和修改都是这一个接口)")
    @PostMapping("/report")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("提交开题报告")
    public R comrepot(@Validated @RequestParam("file") MultipartFile file) {

        String filename = file.getOriginalFilename();
        ResourceBundle resource = ResourceBundle.getBundle("oss");
        String reporturl = resource.getString("reporturl");
        User user = LoginController.user1;
        ReportFile reportFile = reportFileService.getByUserId(user.getUserId());
        if(reportFile!=null){
            if(reportFile.getStatus()==2){
                return R.error(ResultCode.STUDENT_ADVISE_ERROR2);
            }
        }
        assert filename != null;
        if (!"".equals(filename.trim())) {

            String[] split =new String[2];
            split[0]= filename.substring(0,filename.lastIndexOf("."));
            split[1] = filename.substring(filename.lastIndexOf(".")+1,
                    filename.length());

            if(AliyunOSSUtil.TXT.equals(split[1])|| AliyunOSSUtil.DOCX.equals(split[1])|| AliyunOSSUtil.PDF.equals(split[1])|| AliyunOSSUtil.DOC.equals(split[1])|| AliyunOSSUtil.XLS.equals(split[1])){
                System.out.println("类型正确");
            }else{
                return R.error(ResultCode.USER_UPFILE_ERROR);
            }
            String fileCode = aliyunOSSUtil.getFileCode();
            try {
                ReportFile reportFile1 = new ReportFile();
                reportFileService.insert(reportFile1,user,split,fileCode);
            } catch (Exception e) {
                return R.error(ResultCode.STUDENT_UP_ERROR);
            }
            ChoCgy choCgy = choCgyService.selectByuserId(user.getUserId());
            if (choCgy==null){
                return R.error(ResultCode.STUDENT_CHOOSE_ERROR10);
            }else {
                // 上传到OSS
                aliyunOSSUtil.upLoad3(file, reporturl,fileCode,split[1]);
            }
        }
//        //提交开题报告后将状态置为2
//        Stage stage = stageService.findById(user.getUserId());
//        stage.setActiveStep(2);
//        stageService.updateById(stage);
        return R.okey(ResultCode.SUCCESS);
    }

    @PostMapping("/down/report")
//    @LogOperation("下载开题报告")
    @ApiOperation("下载开题报告")
    public R downReport(@NotNull @RequestParam("id")int id, HttpServletResponse response)  {
        try {

            ReportFile reportFile = reportFileService.getById(id);
            //文件路径
            String path=reportFile.getPath()+"/"+reportFile.getFilecode()+"."+ reportFile.getSuffix();

            response.addHeader("Content-Disposition", "attachment;filename=" +new String((reportFile.getFilename()+"."+ reportFile.getSuffix()).getBytes("UTF-8"),"ISO-8859-1"));
            aliyunOSSUtil.exportOssFile(response.getOutputStream(),path);
            return R.okey(ResultCode.SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            return R.error(ResultCode.FAIL);
        }
    }
    @ApiOperation("展示自己的开题报告")
    @GetMapping("/show/reports")
//    @LogOperation("展示开题报告")
    @Transactional(rollbackFor = Exception.class)
    public R showReport() {
        User user = LoginController.user1;
        ReportFile reportFile = reportFileService.getByUserId(user.getUserId());

        return R.okey(ResultCode.SUCCESS).put("data",reportFile);
    }

    @ApiOperation("删除自己的开题报告")
    @DeleteMapping("/delete/report")
//    @LogOperation("删除开题报告")
    @Transactional(rollbackFor = Exception.class)
    public R deleteReport() {
        User user = LoginController.user1;
        ReportFile reportFile = reportFileService.getByUserId(user.getUserId());
        if(reportFile==null){
            return R.error(ResultCode.STUDENT_CHOOSE_ERROR9);
        }
        if(reportFile.getStatus()==2){
            return R.error(ResultCode.STUDENT_DELETE_ERROR2);
        }
        reportFileService.delete(user.getUserId());
        return R.okey(ResultCode.SUCCESS);
    }


    @ApiOperation("学生申报题目(修改也是这个接口)")
    @PostMapping("/category")
//    @LogOperation("学生申报题目")
    @Transactional(rollbackFor = Exception.class)
    public R applyTitle(@Validated @RequestBody AddCgyForm addCgyForm) {
        try {
            User user = LoginController.user1;
            Category category2 = categoryService.selectByUserId(user.getUserId());

            ChoCgyRecord choCgyRecord = choCgyRecordService.selectByUsername(user.getUsername());
            if(category2!=null&&category2.getCgyStatus()==2){
                return R.error(ResultCode.STUDENT_APPLY_ERROR3);
            }
            if(category2!=null&&category2.getCgyLeadstatus()==2){
                return R.error(ResultCode.STUDENT_APPLY_ERROR3);
            }
            if(choCgyRecord!=null){
                return R.error(ResultCode.STUDENT_CHOOSE_ERROR6);
            }
            if (categoryService.selectByUserId(user.getUserId()) == null) {
                System.out.println("未申报过课题,所以插入");
                Category category = new Category();
                Category category1 = categoryService.setCategory(addCgyForm, user, category);
                String[] split = addCgyForm.getTusername().split(" ");
                User user1 = userService.queryUserByName(split[0]);
                category1.setTeaName(user1.getName());
                category1.setCgyStatus(0);
                categoryService.insertCategory(category1);
            } else {
                System.out.println("已经申报过课题,所以更新");
                Category category = categoryService.selectByUserId(user.getUserId());
                if(category.getCgyStatus()==2&&category.getCgyLeadstatus()==2){
                    return R.error(ResultCode.STUDENT_ADVISE_ERROR2);
                }
                Category category1 = categoryService.setCategoryUpdate(addCgyForm, user, category);
                String[] split = addCgyForm.getTusername().split(" ");
                User user1 = userService.queryUserByName(split[0]);
                category1.setTeaName(user1.getName());
                category1.setCgyStatus(0);
                categoryService.updateById(category1);
            }
            return R.okey(ResultCode.SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.STUDENT_CHOOSE_ERROR2);
        }
    }

    @ApiOperation("拿到自己申报的课题")
    @GetMapping("/category")
//    @LogOperation("拿到自己申报的课题")
    @Transactional(rollbackFor = Exception.class)
    public R getCgy() {
        User user = LoginController.user1;
        kei=user;
        QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();
        categoryQueryWrapper.eq("user_id",user.getUserId());
        Category category = categoryService.getOne(categoryQueryWrapper);
        return R.okey(ResultCode.SUCCESS).put("data",category);
    }
    @ApiOperation("删除自己申报的课题")
    @DeleteMapping("/delete/category")
//    @LogOperation("删除自己申报的课题")
    @Transactional(rollbackFor = Exception.class)
    public R deleteMyCgy() {
        User user = LoginController.user1;
        Category category = categoryService.selectByUserId(user.getUserId());
        if(category==null){
            return R.error(ResultCode.STUDENT_CHOOSE_ERROR9);
        }
        if(category.getCgyStatus()==2||category.getCgyLeadstatus()==2){
            return R.error(ResultCode.STUDENT_DELETE_ERROR2);
        }
       categoryService.delete(category);
        return R.okey(ResultCode.SUCCESS);
    }


    @PostMapping("/show/categorys")
    @ApiOperation("展示、模糊查询所有老师的课题,多条件模糊查询")
//    @LogOperation("多条件查询课题")
    public R showCgy(@Validated @RequestBody SelectCgyForm selectCgyForm) {
        IPage<Category> categoryIPage = categoryService.pageCategoryByDuoCondi(selectCgyForm);
        PageForm<Category> categoryPageForm = new PageForm<>();
        categoryPageForm.setList(categoryIPage.getRecords());
        categoryPageForm.setTotal(categoryIPage.getTotal());
        return R.okey(ResultCode.SUCCESS).put("data", categoryPageForm);
    }

    @GetMapping("/category/{cgyId}/info")
    @ApiOperation("拿到课题信息")
//    @LogOperation("拿到课题信息")
    public R getCgyById(@NotNull @PathVariable("cgyId") int cgyId) {
        Category category = categoryService.getById(cgyId);
        return R.okey(ResultCode.SUCCESS).put("data", category);
    }

    @ApiOperation("学生选择题目")
    @PostMapping("/choose/category/{cgyId}")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("学生选择题目")
    public R chooseCgy(@Pattern(regexp = "^[0-9]*$\n", message = "只能是数字") @PathVariable("cgyId") int cgyId) {

            User user = LoginController.user1;
        Category category = categoryService.selectByUserId(user.getUserId());
            System.out.println("我在这儿");
            if(category!=null){
                return R.error(ResultCode.STUDENT_CHOOSE_ERROR7);
            }


            Category category1 = categoryService.getById(cgyId);
            ChoCgyRecord choCgyRecord = choCgyRecordService.selectByUsername(user.getUsername());
            if(choCgyRecord==null&&category1.getCgySource()==2) {
                    System.out.println("我在选择题目");
                    categoryService.insertCgyRecord(cgyId, user);
                    return R.okey(ResultCode.SUCCESS);
             }else if(category1.getCgySource()==1){
                    return R.error(ResultCode.STUDENT_CHOOSE_ERROR4);
            }else if(choCgyRecord!=null&&choCgyRecord.getStatus()==2){
                    return R.error(ResultCode.STUDENT_CHOOSE_ERROR11);
            }else if(choCgyRecord!=null){
                categoryService.insertCgyRecord(cgyId, user);
                return R.okey(ResultCode.SUCCESS);
            }
            return R.okey(ResultCode.SUCCESS);

    }

    @ApiOperation("展示自己选择的题目")
    @GetMapping("/category/choose")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("展示自己选择的题目")
    public R showCgy() {
        try {
            User user = LoginController.user1;
//            User user = kei;
            ChoCgyRecord choCgyRecord = choCgyRecordService.selectByUsername(user.getUsername());

            return R.okey(ResultCode.SUCCESS).put("data",choCgyRecord);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }

    }

    @ApiOperation("删除自己选择的题目")
    @PostMapping("/delete/category/choose")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("删除自己选择的题目")
    public R deleteCgy() {

        User user = LoginController.user1;
        try {
            ChoCgyRecord choCgyRecord = choCgyRecordService.selectByUsername(user.getUsername());
            if(choCgyRecord==null){
                return R.error(ResultCode.STUDENT_CHOOSE_ERROR9);
            }
            if(choCgyRecord.getStatus()==2){
                return R.error(ResultCode.USER_DELETE_ERROR2);
            }
            choCgyRecordService.deleteCgy(user.getUsername());
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
        return R.okey(ResultCode.SUCCESS);
    }
    @ApiOperation("学生提交指导记录")
    @PostMapping("/records")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("学生提交指导记录")
    public R commitRecord(@Validated @RequestBody RecordForm recordForm){
        try {
            User user = LoginController.user1;
            ChoCgy choCgy = choCgyService.selectBySusername(user.getUsername());
            if(choCgy==null){
                return R.error(ResultCode.STUDENT_UP_ERROR);
            }

            recordsService.inertRecord(recordForm,user,choCgy.getTusername());
        } catch (Exception e) {
            throw  new MyException(ResultCode.FAIL);
        }
        return R.okey(ResultCode.SUCCESS);
    }
    
    @ApiOperation("分页展示指导记录")
    @GetMapping("/show/records")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("分页展示指导记录")
    public R showRecord(@Validated @ModelAttribute SelectForm selectForm){
        PageForm<Records> recordsPageForm = new PageForm<>();
        try {
            User user = LoginController.user1;
            IPage<Records> recordsIPage = recordsService.pageRecord(selectForm,user);
            long total = recordsIPage.getTotal();
            List<Records> records = recordsIPage.getRecords();

            recordsPageForm.setList(records);
            recordsPageForm.setTotal(total);
        } catch (Exception e) {
            throw  new MyException(ResultCode.USER_SELECT_ERROR);
        }
        return R.okey(ResultCode.SUCCESS).put("data",recordsPageForm);
    }

    @ApiOperation("拿到具体指导记录")
    @GetMapping("/record/{recordsId}/info")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("拿到具体指导记录")
    public R getRecord(@PathVariable("recordsId") int recordsId) {

        try {
            Records records = recordsService.getById(recordsId);
            return R.okey(ResultCode.SUCCESS).put("data",records);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }

    }


    @ApiOperation("学生修改指导记录")
    @PostMapping("/update/records")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("学生修改指导记录")
    public R updateRecord(@Validated @RequestBody RecordForm recordForm){
        try {
            User user = LoginController.user1;
            recordsService.updateRecord(recordForm,user);
        } catch (Exception e) {
            throw  new MyException(ResultCode.FAIL);
        }
        return R.okey(ResultCode.SUCCESS);
    }

    @ApiOperation("学生删除指导记录")
    @DeleteMapping("/delete/records")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("学生删除指导记录")
    public R deletRecord(@Validated @ModelAttribute DeleteForm deleteForm){
        try {
            recordsService.deleteRecord(deleteForm.getList());
        } catch (Exception e) {
            throw  new MyException(ResultCode.FAIL);
        }
        return R.okey(ResultCode.SUCCESS);
    }
//


    @PostMapping("/paper")
    @ApiOperation("上传毕业论文")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("上传毕业论文")
    public R commitPaper( @RequestParam("file") MultipartFile file) throws IOException {
        User user = LoginController.user1;
        String filename = file.getOriginalFilename();
        System.out.println(filename + "==filename");
        ResourceBundle resource = ResourceBundle.getBundle("oss");
        String paperurl = resource.getString("paperurl");
        PaperFile paperFile1 = paperFileService.selectByUsername(user.getUsername());
        if(paperFile1!=null){
            if(paperFile1.getStatus()==2){
                return R.error(ResultCode.STUDENT_ADVISE_ERROR2);
            }
        }
        assert filename != null;
        if (!"".equals(filename.trim())) {
            String[] split =new String[2];
            split[0]= filename.substring(0,filename.lastIndexOf("."));
            split[1] = filename.substring(filename.lastIndexOf(".")+1,
                    filename.length());

            if(AliyunOSSUtil.TXT.equals(split[1])|| AliyunOSSUtil.DOCX.equals(split[1])|| AliyunOSSUtil.PDF.equals(split[1])|| AliyunOSSUtil.DOC.equals(split[1])|| AliyunOSSUtil.XLS.equals(split[1])){
                System.out.println("类型正确");
            }else{
                return R.error(ResultCode.USER_UPFILE_ERROR);
            }
            String fileCode = aliyunOSSUtil.getFileCode();
            PaperFile paperFile = new PaperFile();
            try {
                paperFileService.insertPaper(paperFile,user,split,fileCode);
            } catch (Exception e) {
                return R.error(ResultCode.STUDENT_UP_ERROR);
            }
            // 上传到OSS
           aliyunOSSUtil.upLoad3(file, paperurl,fileCode,split[1]);
        }
        return R.okey(ResultCode.SUCCESS);
    }

    @PostMapping("/down/paper")
//    @LogOperation("下载论文")
    @ApiOperation("下载论文")
    public R downPaper(@NotNull @RequestParam("id")int id, HttpServletResponse response)  {
        try {

            PaperFile paperFile = paperFileService.getById(id);
            //文件路径
            String path=paperFile.getPath()+"/"+paperFile.getFilecode()+"."+ paperFile.getSuffix();

            response.addHeader("Content-Disposition", "attachment;filename=" +new String((paperFile.getFilename()+"."+ paperFile.getSuffix()).getBytes("UTF-8"),"ISO-8859-1"));
            aliyunOSSUtil.exportOssFile(response.getOutputStream(),path);
            return R.okey(ResultCode.SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            return R.error(ResultCode.FAIL);
        }
    }
    @ApiOperation("展示自己的上传论文记录")
    @GetMapping("/paper")
//    @LogOperation("展示自己的上传论文记录")
    @Transactional(rollbackFor = Exception.class)
    public R showPaper() {
        User user = LoginController.user1;
        PaperFile paperFile = paperFileService.selectByUsername(user.getUsername());
        return R.okey(ResultCode.SUCCESS).put("data",paperFile);
    }
    @ApiOperation("删除自己的上传论文记录，同时删除文件")
    @DeleteMapping("/delete/paper")
//    @LogOperation("删除自己的论文")
    @Transactional(rollbackFor = Exception.class)
    public R deletePaper() {
        try {
            User user = LoginController.user1;
            PaperFile paperFile = paperFileService.selectByUsername(user.getUsername());
            if(paperFile==null){
                return R.error(ResultCode.STUDENT_CHOOSE_ERROR9);
            }
            if(paperFile.getStatus()==2){
                return R.error(ResultCode.STUDENT_DELETE_ERROR2);
            }

            paperFileService.deletePaper(user.getUsername());
        } catch (Exception e) {

            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
        return R.okey(ResultCode.SUCCESS);
    }

    @GetMapping("/defence")
    @ApiOperation("学生查询自己的答辩安排")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("学生查询自己的答辩安排")
    public R getDefence(){
        try {
            User user = LoginController.user1;

            ChoCgy choCgy = choCgyService.selectBySusername(user.getUsername());
            if(choCgy==null){
                return  R.error(ResultCode.STUDENT_CHOOSE_ERROR3);
            }
            Defence defenceTea = defenceService.getDefenceStu(user);
            List<Defence> defenceLi = new ArrayList<>();
            defenceLi.add(defenceTea);
            return R.okey(ResultCode.SUCCESS).put("data",defenceLi);
        }catch (MyException e) {
            throw new MyException(ResultCode.STUDENT_CHOOSE_ERROR3);
        }catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }

    }
    @GetMapping("/grade")
    @ApiOperation("学生查询自己成绩")
    @Transactional(rollbackFor = Exception.class)
    public R getGrade(){
        try {
            User user = LoginController.user1;

            ChoCgy choCgy = choCgyService.selectBySusername(user.getUsername());
            ArrayList<ChoCgy> grades = new ArrayList<>();
            grades.add(choCgy);
            return R.okey(ResultCode.SUCCESS).put("data",grades);
        }catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }

    }

    @GetMapping("/midtermDefence")
    @ApiOperation("学生查询自己的期中答辩安排")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("学生查询自己的期中答辩安排")
    public R getMidtermDefence(){
        try {
            User user = LoginController.user1;

            ChoCgy choCgy = choCgyService.selectBySusername(user.getUsername());
            if(choCgy==null){
                return  R.error(ResultCode.STUDENT_CHOOSE_ERROR3);
            }
            MidtermDefence midtermDefenceTea = MidtermdefenceService.getDefenceStu(user);
            List<MidtermDefence> defenceLi = new ArrayList<>();
            defenceLi.add(midtermDefenceTea);
            return R.okey(ResultCode.SUCCESS).put("data",defenceLi);
        }catch (MyException e) {
            throw new MyException(ResultCode.STUDENT_CHOOSE_ERROR3);
        }catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }

    }

    /**
     *下载和预览审查报告
     */
    @PostMapping("/down/inspectionReport")
    @ApiOperation("下载和预览审查报告")
    public R inspectionReport(@NotNull @RequestParam("stage")Integer stage, HttpServletResponse response)  {
        try {
            // 获取用户信息
            User user = LoginController.user1;
            inspectionReportService.downInspectionReport(stage, user.getUsername(), response);
            return R.okey(ResultCode.SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            return R.error(ResultCode.FAIL);
        }
    }

}




