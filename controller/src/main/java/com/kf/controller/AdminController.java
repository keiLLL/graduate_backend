package com.kf.controller;

import cn.hutool.http.HttpResponse;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.kf.aop.LogOperation;
import com.kf.config.RedisUtil;
import com.kf.handler.MyException;

import com.kf.mapper.RolePermsMapper;
import com.kf.mapper.UserRoleMapper;
import com.kf.pojo.*;
import com.kf.pojo.form.*;
import com.kf.service.*;
import com.kf.util.MD5Utils;
import com.kf.util.R;
import com.kf.util.ResultCode;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Delete;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 管理控制器
 */
@RestController
@RequestMapping("/admin")
public class AdminController {
    @Resource
    UserService userService;
    @Resource
    RolePermsService rolePermsService;
    @Resource
    RolePermsMapper rolePermsMapper;
    @Resource
    StudentService studentService;
    @Resource
    AdminService adminService;
    @Resource
    TeacherService teacherService;
    @Resource
    RoleService roleService;
    @Resource
    RedisUtil redisUtil;
    @Resource
    UserRoleService userRoleService;
    @Resource
    PermissionService permissionService;
    @Resource
    UserRoleMapper userRoleMapper;
    @Resource
    SysDictTypeService sysDictTypeService;
    @Resource
    SysDictDataService sysDictDataService;

    public static User user2;
    /**
     * 添加用户
     *
     * @param adduserForm adduser形式
     * @return {@link R}
     */
    @PostMapping("/users")
    @ApiOperation("添加用户")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("添加用户")
    public R addUser(@Validated(AddUserForm.Admin.class) @RequestBody AddUserForm adduserForm) {

        try {
            User user2 = userService.queryUserByName(adduserForm.getUsername());
            if(user2!=null){
                return R.error(ResultCode.STUDENT_ADD_ERROR);
            }
            User user3 = userService.findByPhone(adduserForm.getPhone());
            if(user3!=null){
                return R.error(ResultCode.STUDENT_ADD_ERROR2);
            }
            userService.addUser(adduserForm);
            User user1 = userService.queryUserByName(adduserForm.getUsername());
            userService.addUserAll(adduserForm,user1);

        }catch (MyException e) {
            e.printStackTrace();
            throw new MyException(ResultCode.STUDENT_ADD_ERROR);
        }catch (Exception e) {
            System.out.println("捕获到了异常");
            return R.error(ResultCode.STUDENT_ADD_ERROR);
        }
        redisUtil.deleteByPrex("user:current:*");
        return R.okey(ResultCode.SUCCESS);


    }


    /**
     * 显示用户
     *
     * @param selectForm 选择形式
     * @return {@link R}
     */
    @GetMapping("/show/user")
    @ApiOperation("展示、模糊查询所有用户")
//    @LogOperation("查询所有用户")
    @Transactional(rollbackFor = Exception.class)
    public R showUser(@Validated @ModelAttribute  SelectForm selectForm) throws MyException {
        try {
            if("".equals(selectForm.getName())){
                String pagekeys="user:current:"+selectForm.getCurrent()+":"+selectForm;
                if(redisUtil.get(pagekeys)!=null){
                    PageFormUser pageFormUser1 = (PageFormUser)redisUtil.get(pagekeys);
                    return  R.okey(ResultCode.SUCCESS).put("data", pageFormUser1);
                }else {
                    PageFormUser pageFormUser = userService.reidsGetUser(selectForm, pagekeys);
                    return R.okey(ResultCode.SUCCESS).put("data", pageFormUser);
                }
            }else {
                PageFormUser pageFormUser2 = new PageFormUser();
                IPage<User> userIPage =userService.pageUser(selectForm.getName(), selectForm.getCurrent(), selectForm.getSize());
                List<User> records = userIPage.getRecords();
                pageFormUser2.setList(records);
                pageFormUser2.setTotal(userIPage.getTotal());
                return R.okey(ResultCode.SUCCESS).put("data", pageFormUser2);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @GetMapping("/user/{userId}/info")
    @ApiOperation("拿到当前用户的具体信息")
//    @LogOperation("拿到当前用户的具体信息")
    public R getUser(@NotNull @PathVariable("userId") int userId) throws MyException {
        try {

            User user = userService.getById(userId);
            user2=user;
            int state = user.getState();

            if(state==1){
                Student student = studentService.findById(user.getUserId());
                StudentForm studentForm = userService.getStudent(student, user, state);
                return R.okey(ResultCode.SUCCESS).put("data",studentForm);
            }else if(state==2){
                QueryWrapper<Teacher> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("user_id",user.getUserId());
                Teacher teacher = teacherService.getOne(queryWrapper);
                TeacherForm teacherForm = userService.getTeacher(teacher, user, state);
                return R.okey(ResultCode.SUCCESS).put("data",teacherForm);
            }else {
                QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("user_id",user.getUserId());
                Admin admin = adminService.getOne(queryWrapper);

                AdminForm adminForm = new AdminForm();
                adminForm.setState(state)
                        .setUsername(user.getUsername())
                        .setDepartment(user.getDepartment())
                        .setEmail(admin.getAdEmail())
                        .setName(admin.getAdName())
                        .setSex(admin.getAdSex())
                        .setStatus(admin.getAdStatus())
                        .setPhone(admin.getAdPhone())
                        .setWx(admin.getAdWx())
                        .setUserId(admin.getUserId());
                return R.okey(ResultCode.SUCCESS).put("data",adminForm);
            }
        } catch (Exception e) {
            e.printStackTrace();
           throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }


    /**
     * 删除用户
     *
     * @param deleteForm 用户id
     * @return {@link R}
     */
    @DeleteMapping("/delete/users")
    @ApiOperation("删除用户")
//    @LogOperation("删除用户")
    public R deleteUser(@Validated @ModelAttribute DeleteForm deleteForm) throws MyException {
        try {
            userService.deleteUser(deleteForm.getList());
            redisUtil.deleteByPrex("user:current:*");
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }


    @GetMapping("/show/roles")
    @ApiOperation("展示、模糊查询所有角色")
//    @LogOperation("查询所有角色")
    public R showRole(@Validated @ModelAttribute  SelectForm selectForm){
        try {
            if("".equals(selectForm.getName())){
                String pagekeys="role:current:"+selectForm.getCurrent()+":"+selectForm.getSize();
                if(redisUtil.get(pagekeys)!=null){
                    PageFormRole  pageFormRole1=(PageFormRole)redisUtil.get(pagekeys);
                    return R.okey(ResultCode.SUCCESS).put("data",pageFormRole1);
                }else{
                    PageFormRole pageFormRole = roleService.redisGetRole(selectForm, pagekeys);
                    return R.okey(ResultCode.SUCCESS).put("data",pageFormRole);
                }
            }else{
                PageFormRole pageFormRole2 = new PageFormRole();
                IPage<Role> roleIPage = roleService.pageRole(selectForm.getName(), selectForm.getCurrent(), selectForm.getSize());
                List<Role> records = roleIPage.getRecords();
                pageFormRole2.setList(records);
                pageFormRole2.setTotal(roleIPage.getTotal());
                return R.okey(ResultCode.SUCCESS).put("data",pageFormRole2);
            }




        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }
    /**
     * 删除角色
     * 责任链模式当删除角色的时候， 用户-角色、角色-权限两张表的数据也要删除
     * @param deleteForm 角色id列表
     * @return {@link R}
     */
    @DeleteMapping("/delete/roles")
    @ApiOperation("删除角色")
//    @LogOperation("删除角色")
    public R deleteRole(@Validated @ModelAttribute DeleteForm deleteForm){
        try {
            roleService.deleteRole(deleteForm.getList());
            redisUtil.deleteByPrex("role:current:*");
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }

    /**
     * 更新学生
     *
     * @param studentForm 学生形式
     * @return {@link R}
     */
    @PostMapping("/update/students")
    @ApiOperation("修改学生信息")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("修改学生信息")
    public R updateStudent(@Validated(StudentForm.AdminStudent.class) @RequestBody StudentForm studentForm) {

        try {
            User user = userService.getById(studentForm.getUserId());
            if(!user.getUsername().equals(studentForm.getUsername())){
                User user1 = userService.queryUserByName(studentForm.getUsername());
                if(user1!=null){
                    return R.error(ResultCode.STUDENT_ADD_ERROR);
                }
            }
            if(!user.getPhone().equals(studentForm.getStuPhone())){
                User byPhone = userService.findByPhone(studentForm.getStuPhone());
                if(byPhone!=null){
                    return  R.error(ResultCode.STUDENT_ADD_ERROR2);
                }
            }
            String encrypt = MD5Utils.encrypt(studentForm.getUsername(), studentForm.getPassword());
            Student student =studentService.findById(studentForm.getUserId());
            studentService.updateStudent(user,student,encrypt,studentForm);
        } catch (MyException e) {
            e.printStackTrace();
            return  R.error(ResultCode.USER_UPDATE_ERROR);
        }catch (NullPointerException e){
            throw new NullPointerException();
        }catch (Exception e){
            throw e;
        }
        redisUtil.deleteByPrex("user:current:*");
        return  R.okey(ResultCode.SUCCESS);

    }

    /**
     * 更新教师
     *
     * @param teacherForm 老师形式
     * @return {@link R}
     */
    @PostMapping("/update/teachers")
    @ApiOperation("修改老师信息")
//    @LogOperation("修改老师信息")
    @Transactional(rollbackFor = Exception.class)
    public R updateTeacher(@Validated(TeacherForm.AdminTeacher.class) @RequestBody TeacherForm teacherForm){
        try {

            User user = userService.getById(teacherForm.getUserId());
            if(!user.getUsername().equals(teacherForm.getUsername())){
                User user1 = userService.queryUserByName(teacherForm.getUsername());
                if(user1!=null){
                    return R.error(ResultCode.STUDENT_ADD_ERROR);
                }
            }
            if(!user.getPhone().equals(teacherForm.getTeaPhone())){
                User byPhone = userService.findByPhone(teacherForm.getTeaPhone());
                if(byPhone!=null){
                    return  R.error(ResultCode.STUDENT_ADD_ERROR2);
                }
            }

            try {
                QueryWrapper<Teacher> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("user_id",user.getUserId());
                Teacher teacher = teacherService.getOne(queryWrapper);
                teacherService.updateTeacher(user,teacherForm,teacher);
            } catch (Exception e) {
                e.printStackTrace();
                throw new MyException(ResultCode.USER_UPDATE_ERROR);
            }
            redisUtil.deleteByPrex("user:current:*");
            return  R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_UPDATE_ERROR);
        }
    }

    /**
     * 更新管理
     *
     * @param adminForm 管理形式
     * @return {@link R}
     */
    @PostMapping("/update/admins")
    @ApiOperation("修改管理员信息")
//    @LogOperation("修改管理员信息")
    @Transactional(rollbackFor = Exception.class)
    public R updateAdmin(@Validated @RequestBody AdminForm adminForm){
        try {
            int userId = adminForm.getUserId();
            User user = userService.getById(userId);
            if(!user.getUsername().equals(adminForm.getUsername())){
                User user1 = userService.queryUserByName(adminForm.getUsername());
                if(user1!=null){
                    return R.error(ResultCode.STUDENT_ADD_ERROR);
                }
            }
            if(!user.getPhone().equals(adminForm.getPhone())){
                User byPhone = userService.findByPhone(adminForm.getPhone());
                if(byPhone!=null){
                    return  R.error(ResultCode.STUDENT_ADD_ERROR2);
                }
            }
            try {
                QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("user_id",user.getUserId());
                Admin admin = adminService.getOne(queryWrapper);
                adminService.updateAdmin(user,adminForm,admin);
            } catch (Exception e) {
                e.printStackTrace();
                throw new MyException(ResultCode.USER_UPDATE_ERROR);
            }
            redisUtil.deleteByPrex("user:current:*");
            return  R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_UPDATE_ERROR);
        }
    }
    @PostMapping("/update/roles")
    @ApiOperation("修改角色信息")
//    @LogOperation("修改角色信息")
    @Transactional(rollbackFor = Exception.class)
    public R updateRole(@Validated @RequestBody RoleForm roleForm) throws MyException {
        try {
            Role role =roleService.getRole(roleForm.getId());

            role.setRemark(roleForm.getRemark());
            role.setName(roleForm.getName());
            roleService.updateById(role);
            redisUtil.deleteByPrex("role:current:*");
            return  R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("我进角色异常了");
            throw new MyException(ResultCode.USER_UPDATE_ERROR);

        }
    }
    @GetMapping("/perms/{roleId}/role")
    @ApiOperation("拿到该角色的所有权限")
//    @LogOperation("拿到该角色的所有权限")
    @Transactional(rollbackFor = Exception.class)
    public R getPerms(@NotNull @PathVariable("roleId") int roleId){
        try {
            List<RolePerms> perms = rolePermsService.getPerms(roleId);
//            List<Permission> permsList=new ArrayList<>();
            List<Integer> list=new ArrayList<>();
            for (RolePerms perm : perms) {
                Permission permission = permissionService.getById(perm.getPermId());
//                permsList.add(permission);
                list.add(permission.getPermId());
            }
            return  R.okey(ResultCode.SUCCESS).put("data",list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_SELECT_ERROR);

        }
    }
    @PostMapping("/update/role/perms")
    @ApiOperation("更新角色的权限")
//    @LogOperation("更新角色的权限")
    @Transactional(rollbackFor = Exception.class)
    public R upRolePerms(@Validated @RequestBody RolePermsList rolePermsList){
        try {
            int roleId = rolePermsList.getRoleId();
            rolePermsMapper.deleteRolePerms(roleId);
            for (Integer integer : rolePermsList.getPermslist()) {
                RolePerms rolePerms = new RolePerms();
                rolePerms.setRoleId(roleId);
                rolePerms.setPermId(integer);
                rolePermsMapper.insert(rolePerms);
            }
            return  R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_UPDATE_ERROR);
        }
    }

    @GetMapping("/role/{roleId}/users")
    @ApiOperation("拿到该角色已有的成员")
//    @LogOperation("拿到该角色已有的成员")
    @Transactional(rollbackFor = Exception.class)
    public R getRoleUser(@NotNull @PathVariable("roleId") int roleId){
        try {

            List<Integer> userIdList=new ArrayList<>();
            List<UserRole> userRoles = userRoleService.selectUserRoleList(roleId);
            for (UserRole userRole : userRoles) {
                Integer userId = userRole.getUserId();
                userIdList.add(userId);
            }
            List<User> userList = userService.selectUserListById(userIdList);

            return  R.okey(ResultCode.SUCCESS).put("data",userIdList);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }
    @GetMapping("/role/{roleId}/nousers")
    @ApiOperation("拿到该角色没有的成员")
//    @LogOperation("拿到该角色没有的成员")
    @Transactional(rollbackFor = Exception.class)
    public R getRoleNoUser(@NotNull @PathVariable("roleId") int roleId){
        try {


            List<User> list=new ArrayList<>();
            List<UserRole> userRoles = userRoleService.selectUserRoleList(roleId);
            List<User> userList1 = userService.selectAllUser();
            for (User user : userList1) {
                int userId = user.getUserId();
                for (UserRole userRole : userRoles) {
                    Integer userId1 = userRole.getUserId();
                    if(userId!=userId1){
                        User user1 = userService.getById(userId);
                        list.add(user1);
                    }
                }
            }
            return  R.okey(ResultCode.SUCCESS).put("data",list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @PostMapping("/update/role/users")
    @ApiOperation("更新角色的用户")
//    @LogOperation("更新角色的用户")
    @Transactional(rollbackFor = Exception.class)
    public R upRoleUser(@Validated @RequestBody  RoleUserList roleUserList){
        try {
            int roleId = roleUserList.getRoleId();
            userRoleMapper.deleteByRoleId(roleId);
            for (Integer integer : roleUserList.getUserList()) {
                UserRole userRole = new UserRole();
                userRole.setUserId(integer);
                userRole.setRoleId(roleId);
                userRoleMapper.insert(userRole);
            }
            return  R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_UPDATE_ERROR);
        }
    }
    @GetMapping("/role/{roleId}/msg")
    @ApiOperation("拿到具体角色")
//    @LogOperation("拿到具体角色")
    public R getRoleMsg(@NotNull @PathVariable("roleId") int roleId){
        try {
            Role role = roleService.getRole(roleId);
            return  R.okey(ResultCode.SUCCESS).put("data",role);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }
    @GetMapping("/all/users")
    @ApiOperation("拿到所有用户")
//    @LogOperation("拿到所有用户")
    public R getAllUser(){
        try {
            List<User> userList = userService.selectAllUser();
            return  R.okey(ResultCode.SUCCESS).put("data",userList);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }
    @GetMapping("/show/sysdicttype")
    @ApiOperation("模糊查询、展示所有字典类型")
//    @LogOperation("模糊查询、展示所有字典类型")
    public R getSysDictType(@Validated @ModelAttribute SelectForm selectForm){
        try {
            IPage<SysDictType> sysDictTypeIPage = sysDictTypeService.selectPage(selectForm);

            PageForm<SysDictType> sysDictTypePageForm = new PageForm<>();
            sysDictTypePageForm.setTotal(sysDictTypeIPage.getTotal());
            sysDictTypePageForm.setList(sysDictTypeIPage.getRecords());
            return  R.okey(ResultCode.SUCCESS).put("data",sysDictTypePageForm);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }
    @DeleteMapping("/delete/sysdicttype")
    @ApiOperation("删除字典类型")
//    @LogOperation("删除字典类型")
    @Transactional(rollbackFor = Exception.class)
    public R deleteSysDictType(@Validated @ModelAttribute DeleteForm deleteForm){
        try {
            sysDictTypeService.delete(deleteForm);
            return  R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }

    @PostMapping("/sysdicttype")
    @ApiOperation("增加字典类型")
//    @LogOperation("增加字典类型")
    @Transactional(rollbackFor = Exception.class)
    public R addSysDictType(@Validated @RequestBody AddSysDictType addSysDictType){
        try {
            sysDictTypeService.insertType(addSysDictType);
            return  R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_INSERT_ERROR);
        }
    }
    @PostMapping("/update/sysdicttype")
    @ApiOperation("修改字典类型")
//    @LogOperation("修改字典类型")
    @Transactional(rollbackFor = Exception.class)
    public R updateSysDictType(@Validated @RequestBody AddSysDictType addSysDictType){
        try {
            sysDictTypeService.updateType(addSysDictType);
            return  R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_INSERT_ERROR);
        }
    }

    @GetMapping("/show/sysdictdata")
    @ApiOperation("模糊查询、展示字典数据")
//    @LogOperation("模糊查询、展示字典数据")
    public R getSysDictData(@Validated @ModelAttribute SelectDictData selectForm){
        try {
            IPage<SysDictData> sysDictDataIPage = sysDictDataService.selecPage(selectForm);
            PageForm<SysDictData> selectDictDataPageForm = new PageForm<>();
            selectDictDataPageForm.setTotal(sysDictDataIPage.getTotal());
            selectDictDataPageForm.setList(sysDictDataIPage.getRecords());
            return  R.okey(ResultCode.SUCCESS).put("data",selectDictDataPageForm);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @DeleteMapping("/delete/sysdictdata")
    @ApiOperation("删除字典数据")
//    @LogOperation("删除字典数据")
    @Transactional(rollbackFor = Exception.class)
    public R deleteSysDictData(@Validated @ModelAttribute DeleteForm deleteForm){
        try {

            sysDictDataService.delete(deleteForm);
            return  R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }
    @PostMapping("/sysdictdata")
    @ApiOperation("增加字典数据")
//    @LogOperation("增加字典数据")
    @Transactional(rollbackFor = Exception.class)
    public R addSysDictType(@Validated @RequestBody AddSysDicData addSysDictData){
        try {
            sysDictDataService.insertData(addSysDictData);
            return  R.okey(ResultCode.SUCCESS);
        }catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_INSERT_ERROR);
        }
    }

    @PostMapping("/update/sysdictdata")
    @ApiOperation("修改字典数据")
//    @LogOperation("修改字典数据")
    @Transactional(rollbackFor = Exception.class)
    public R updateSysDictData(@Validated @RequestBody AddSysDicData addSysDicData){
        try {
            sysDictDataService.updateData(addSysDicData);
            return  R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_UPDATE_ERROR);
        }
    }
    @GetMapping("/sysdicttype/{id}/info")
    @ApiOperation("拿到字典类型的具体信息")
//    @LogOperation("拿到字典类型的具体信息")
    public R getSysDictTypeById(@Validated @PathVariable("id") int id){
        try {
            SysDictType sysDictType = sysDictTypeService.getById(id);
            return  R.okey(ResultCode.SUCCESS).put("data",sysDictType);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_UPDATE_ERROR);
        }
    }
    @GetMapping("/sysdictdata/{id}/info")
    @ApiOperation("拿到字典数据的具体信息")
//    @LogOperation("拿到字典数据的具体信息")
    public R getSysDictDataById(@Validated @PathVariable("id") int id){
        try {
            SysDictData sysDictType = sysDictDataService.getById(id);
            return  R.okey(ResultCode.SUCCESS).put("data",sysDictType);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @GetMapping("/all/menus")
    @ApiOperation("拿到所有菜单和按钮")
//    @LogOperation("拿到所有菜单和按钮")
    public R getAllmenu(){

        try {
            List<Permission> permissionList = permissionService.selectAllXia(3);
            return R.okey(ResultCode.SUCCESS).put("data",permissionList);

        }catch (Exception e){
            return R.error(ResultCode.FAIL);
        }
    }

    @PostMapping("/menu")
    @ApiOperation("添加菜单或按钮")
//    @LogOperation("添加菜单或按钮")
    @Transactional(rollbackFor = Exception.class)
    public R addMenu(@Validated @RequestBody AddMenuForm addMenuForm){

        try {
            permissionService.addMenu(addMenuForm);
            return R.okey(ResultCode.SUCCESS);
        }catch (MyException e) {
            e.printStackTrace();
            throw new MyException(ResultCode.CAI_ADD_ERROR3);
        } catch (Exception e){
            return R.error(ResultCode.FAIL);
        }
    }
    @PostMapping("/update/menus")
    @ApiOperation("修改菜单或按钮")
//    @LogOperation("修改菜单或按钮")
    public R uodateMenu(@Validated @RequestBody AddMenuForm addMenuForm){

        try {
            permissionService.updatePermision(addMenuForm);
            return R.okey(ResultCode.SUCCESS);
        }catch (MyException e) {
            e.printStackTrace();
            return R.error(ResultCode.CAI_ADD_ERROR3);
        } catch (Exception e){
            return R.error(ResultCode.FAIL);
        }
    }
    @DeleteMapping("/delete/menu")
    @ApiOperation("删除菜单或按钮")
//    @LogOperation("删除菜单或按钮")
    public R deleteMenu(@Validated @RequestParam("permId") int permId){

        try {
            permissionService.deletePermision(permId);
            return R.okey(ResultCode.SUCCESS);
        }catch (Exception e){
            return R.error(ResultCode.FAIL);
        }
    }
    @GetMapping("/menu/{permId}/info")
    @ApiOperation("拿到当前权限的具体信息")
//    @LogOperation("拿到当前权限的具体信息")
    public R getMenuById(@Validated  @PathVariable("permId") int permId){

        try {
            Permission permission = permissionService.getById(permId);
            return R.okey(ResultCode.SUCCESS).put("data",permission);
        }catch (Exception e){
            return R.error(ResultCode.FAIL);
        }
    }



}
