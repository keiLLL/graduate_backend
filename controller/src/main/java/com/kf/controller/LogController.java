//package com.kf.controller;
//
//import cn.hutool.http.HttpResponse;
//import com.baomidou.mybatisplus.core.metadata.IPage;
////import com.kf.aop.LogOperation;
//import com.kf.config.RedisUtil;
//import com.kf.config.SecurityUser;
//import com.kf.handler.MyException;
//import com.kf.pojo.*;
//import com.kf.pojo.form.PageForm;
//import com.kf.pojo.form.SelectForm;
//import com.kf.service.LogErrorService;
//import com.kf.service.LogLoginService;
//import com.kf.service.LogSysOperationService;
//import com.kf.service.PermissionService;
//import com.kf.util.R;
//import com.kf.util.ResultCode;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.validation.constraints.NotNull;
//import java.util.List;
//
///**
// * 日志控制器
// */
//@RestController
//@RequestMapping("/log")
//public class LogController {
//    @Resource
//    LogErrorService logErrorService;
//    @Resource
//    LogSysOperationService logSysOperationService;
//    @Resource
//    LogLoginService logLoginService;
//    @Resource
//    PermissionService permissionService;
//    @Resource
//    RedisUtil redisUtil;
//    @ApiOperation("异常日志")
//    @GetMapping("/show/error/log")
//    public R selectErrorLog(@Validated  @ModelAttribute SelectForm selectForm)  {
//        PageForm<LogError> logErrorPageForm =new PageForm<>();
//        try {
//
//            String pagekeys="errorlog:current:"+selectForm.getCurrent()+":"+selectForm.getSize();
//
//                IPage<LogError> logErrorIPage = logErrorService.pageLogError(selectForm.getName(), selectForm.getCurrent(), selectForm.getSize());
//                long total = logErrorIPage.getTotal();
//                List<LogError> records = logErrorIPage.getRecords();
//                logErrorPageForm.setList(records);
//                logErrorPageForm.setTotal(total);
//                redisUtil.set(pagekeys,logErrorPageForm);
//                return R.okey(ResultCode.SUCCESS).put("data",logErrorPageForm);
////            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw  new MyException(ResultCode.USER_SELECT_ERROR);
//        }
//
//    }
//
//    @ApiOperation("登录日志")
//    @GetMapping("/show/login/log")
//    public R selectLoginLog(@Validated  @ModelAttribute SelectForm selectForm) {
//        PageForm<LogLogin> logLoginPageForm = new PageForm<>();
//        try {
//
//                IPage<LogLogin> logLoginIPage = logLoginService.pageLogin(selectForm.getName(), selectForm.getCurrent(), selectForm.getSize());
//                logLoginPageForm.setTotal(logLoginIPage.getTotal());
//                logLoginPageForm.setList(logLoginIPage.getRecords());
//
//                return R.okey(ResultCode.SUCCESS).put("data",logLoginPageForm);
////            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw  new MyException(ResultCode.USER_SELECT_ERROR);
//        }
//
//
//
//    }
//
//    @ApiOperation("操作日志")
//    @GetMapping("/show/operation/log")
//    public R selectOperationLog(@Validated  @ModelAttribute SelectForm selectForm){
//        PageForm<LogSysOperation> logSysOperationPageForm = new PageForm<>();
//        try {
//
//
//                IPage<LogSysOperation> logSysOperationIPage = logSysOperationService.pageOpera(selectForm.getName(), selectForm.getCurrent(), selectForm.getSize());
//
//                logSysOperationPageForm.setTotal(logSysOperationIPage.getTotal());
//                logSysOperationPageForm.setList(logSysOperationIPage.getRecords());
//                return R.okey(ResultCode.SUCCESS).put("data",logSysOperationPageForm);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw  new MyException(ResultCode.USER_SELECT_ERROR);
//        }
//
//
//    }
//
//
//
//    @GetMapping("/perms")
////    @LogOperation("获取当前登录权限")
//    public R loginPerm() throws MyException {
//
//        User user = SecurityUser.getUser();
//        System.out.println("loginperm当前登录用户"+user);
//        List<Permission> permissions1 = permissionService.findMyPerms(user);
//        List<Permission> permissions = permissionService.listToTree(permissions1);
//        return R.okey(ResultCode.SUCCESS).put("data",permissions);
//    }
////    @GetMapping("/excels")
////    public R exportExcel(HttpServletResponse response){
////        logErrorService.exports(response,"异常日志");
////        return R.ok();
////    }
//    @GetMapping("/excels")
//    public R exportExcel(HttpServletResponse response){
//        logSysOperationService.exports(response,"操作日志.zip");
//        return R.ok();
//    }
//}
