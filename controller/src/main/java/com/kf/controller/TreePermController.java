package com.kf.controller;


import com.kf.pojo.Permission;
import com.kf.service.PermissionService;
import com.kf.util.R;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 */
@RestController
@RequestMapping("/treeperm")
public class TreePermController {

    @Autowired
    PermissionService permissionService;
    @GetMapping("/all/perms")
    @ApiOperation("查询全部权限")
    public R getAllPerms(){
//        @PathVariable("id") @Pattern(message = "只能是数字",regexp = "^[0-9]*$\n")int id
        List<Permission> permissions1 = permissionService.selectAllXia(3);
        return  R.ok().put("data",permissions1);
    }

}

