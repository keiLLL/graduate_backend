package com.kf.controller;

import com.kf.config.ScheduleTask;
import com.kf.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@Slf4j
@RestController
@RequestMapping("/test")
public class TimeController {

    private final ScheduleTask scheduleTask;

    @Autowired
    public TimeController(ScheduleTask scheduleTask) {
        this.scheduleTask = scheduleTask;
    }

    @PostMapping("/cron")
    public R updateCron(@NotNull @PathVariable("cron") String cron) {
        log.info("new cron :{}", cron);
        scheduleTask.setCron(cron);
        return R.ok();

    }

    @GetMapping("/update/{timer}")
    public R updateTimer(@NotNull @PathVariable("timer") Long timer) {
        log.info("new timer :{}", timer);
        scheduleTask.setTimer(timer);
        return R.ok();
    }
}