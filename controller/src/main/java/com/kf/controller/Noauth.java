package com.kf.controller;

import com.kf.pojo.form.DeleteForm;
import com.kf.pojo.form.SelectForm;
import com.kf.util.R;
import com.kf.util.ResultCode;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping()
public class Noauth {
    @GetMapping("/noauth")
    public R noauth(){
        return R.error(ResultCode.PERMISSION_ERROR);
    }

//    @DeleteMapping("/deletesd")
//    public R noauth1(DeleteForm deleteForm){
//        System.out.println(deleteForm);
//        return R.okey(ResultCode.SUCCESS);
//    }

}
