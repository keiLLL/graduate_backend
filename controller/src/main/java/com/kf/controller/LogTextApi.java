//package com.kf.controller;
//
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//
//@RestController
//public class LogTextApi {
//    private final Logger logger= LoggerFactory.getLogger(this.getClass());
//
//    @GetMapping("/log")
//    public String log(){
//        logger.info("info___log");
//        logger.warn("warn___log");
//        logger.error("error___log");
//        logger.debug("debug___log");
//        logger.trace("trace___log");
//        return "logtext";
//    }
//}