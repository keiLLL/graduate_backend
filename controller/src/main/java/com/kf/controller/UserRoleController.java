package com.kf.controller;


import com.kf.service.UserRoleService;
import com.kf.util.R;
import com.kf.util.ResultCode;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * <p>
 *  前端控制器
 */
@RestController
@RequestMapping("/userrole")
public class UserRoleController {
    @Resource
    UserRoleService userRoleService;


    @GetMapping("/hello")
    public R hello(){
        return R.ok().put("data","lala");
    }

    @ApiOperation("把角色分配给用户，即在用户角色表里面插入数据")
    @PostMapping("/user/role")
    public R addUserRole(@Pattern(message = "只能是数字",regexp = "^[0-9]*$\n") @RequestParam("userId") int userId, @RequestParam("roleId") List<Integer> list){
        try {
            userRoleService.insertUserRole(userId,list);
            return  R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return  R.error(ResultCode.FAIL);
        }
    }
    @ApiOperation("把角色分配给用户，即在用户角色表里面插入数据")
    @PostMapping("/role/user")
    public R addRoleUser(@Pattern(message = "只能是数字",regexp = "^[0-9]*$\n") @RequestParam("roleId") int roleId, @RequestParam("userId") List<Integer> list){
        try {
            userRoleService.insertRoleUser(list,roleId);
            return  R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return  R.error(ResultCode.FAIL);
        }
    }



}

