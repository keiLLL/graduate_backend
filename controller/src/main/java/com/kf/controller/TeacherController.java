package com.kf.controller;



import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.kf.aop.LogOperation;
import com.kf.config.SecurityUser;
import com.kf.handler.MyException;
import com.kf.pojo.*;
import com.kf.pojo.form.*;
import com.kf.service.*;
import com.kf.config.AliyunOSSUtil;

import com.kf.util.R;
import com.kf.util.ResultCode;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.ResourceBundle;

/**
 * <p>
 * 教师表 前端控制器
 */
@RestController
@RequestMapping("/teacher")
public class TeacherController {
    /**
     * 课题
     */
    @Resource
    CategoryService categoryService;

    @Resource
    TeacherService teacherService;
    @Resource
    ChoCgyService choCgyService;

    @Resource
    ChoCgyRecordService choCgyRecordService;
    @Resource
    WenFileService wenFileService;
    @Resource
    private AliyunOSSUtil aliyunOSSUtil;
    @Resource
    ReportService reportService;
    @Resource
    RecordsService recordsService;
    @Resource
    PaperFileService paperFileService;
    @Resource
    DefenceService defenceService;
    @Resource
    UserService userService;
    @Resource
    ReportFileService reportFileService;

    @Resource
    MidtermDefenceService midtermDefenceService;

    @Resource
    InspectionReportService inspectionReportService;

    public static User u1;
    @PostMapping("/update/self/info")
    @ApiOperation("修改个人信息")
//    @LogOperation("修改个人信息")
    public R updateSelf(@Validated(TeacherForm.Tteacher.class) @RequestBody TeacherForm teacherForm){

        try {

            User user = LoginController.user1;
//            User user = SecurityUser.getUser();
            if(!user.getPhone().equals(teacherForm.getTeaPhone())){
                // 查询手机号是否被使用
                User byPhone = userService.findByPhone(teacherForm.getTeaPhone());
                if(byPhone!=null){
                    return  R.error(ResultCode.STUDENT_ADD_ERROR2);
                }
            }
            Teacher teacher = teacherService.findById(user.getUserId());
            // 通过teacherForm中信息更新user和teacher表
            teacherService.updateTeacherSelf(user,teacherForm,teacher);
        } catch (Exception e) {
            e.printStackTrace();
            return R.error(ResultCode.USER_UPDATE_ERROR);
        }
        return R.okey(ResultCode.SUCCESS);
    }

    @PostMapping("/show/categorys")
    @ApiOperation("展示、模糊查询我指导的课题")
    public R showCgy(@Validated @ModelAttribute SelectForm selectForm){
        try {
            User user =  LoginController.user1;
            // 分页查询
            IPage<Category> categoryIPage = categoryService.pageCategoryBycgyName(user.getUsername(), selectForm.getName(), selectForm.getCurrent(), selectForm.getSize());
            PageFormCgy pageFormCgy = new PageFormCgy();
            List<Category> records = categoryIPage.getRecords();
            pageFormCgy.setTotal(categoryIPage.getTotal());
            pageFormCgy.setList(records);
            return R.okey(ResultCode.SUCCESS).put("data",pageFormCgy);

        } catch (Exception e) {
            return R.error(ResultCode.USER_SELECT_ERROR);
        }
    }

    @GetMapping("/category/{cgyid}/info")
    @ApiOperation("获取课题信息")
    public R getCgyById(@NotNull @PathVariable("cgyid") int cgyId){
        Category category = categoryService.getById(cgyId);
        return R.okey(ResultCode.SUCCESS).put("data",category);
    }


    @PostMapping("/batch/check/category")
    @ApiOperation("老师批量审核学生的课题信息")
//    @LogOperation("老师批量审核学生的课题信息")
    @Transactional(rollbackFor = Exception.class)
    public R batchCheckCgy(@Validated @RequestBody CheckList checkList){
        try {
            List<Integer> list = checkList.getList();
            for (Integer id : list) {
                Category category = categoryService.getById(id);
                // 已经审核通过
                if(category.getCgyLeadstatus()==2){
                    return R.error(ResultCode.CATEGORY_APPLY_ERROR);
                }
            }

            list.forEach(x->{
                Category category = categoryService.getById(x);
                category.setCgyStatus(checkList.getStatus());
                category.setCgyAdvise(checkList.getAdvise());
                categoryService.updateById(category);
            });

            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_CHECK_ERROR);
        }
    }
    @DeleteMapping("/delete/category/student")
    @ApiOperation("老师删除学生的申报的课题信息,同时如果选课表中有该课题，应该一并删除")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("老师删除学生的申报的课题信息")
    public R deleteCgy(@Validated @ModelAttribute DeleteForm deleteForm){
        try {
            User user =  LoginController.user1;
            categoryService.deleteCgy(deleteForm,user);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.FAIL);
        }
    }
    /**
     * 显示chomy
     */
    @GetMapping("/show/choose")
    @ApiOperation("（多条件：已审核、审核驳回、审核通过）模糊查询、展示所有选择我为指导老师的有哪些，我再去审核他配不配选我")
    @Transactional(rollbackFor = Exception.class)
    public R showChoMy(@Validated @ModelAttribute ChoMyForm choMyForm){
        try {
            PageForm<ChoCgyRecord> choMyCgyPageForm = teacherService.pageChoMy(choMyForm);
            return R.okey(ResultCode.SUCCESS).put("data",choMyCgyPageForm);
        } catch (Exception e) {
            throw new MyException(ResultCode.FAIL);
        }
    }

    @PostMapping("/batch/check/choose")
    @ApiOperation("批量审核选择我的课题的学生配不配(批量驳回，批量通过都是这个)")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("确认选题")
    public R batchCheckCho(@Validated @RequestBody CheckList checkList){
        try {
            teacherService.batchCheckRecord(checkList);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            return R.error(ResultCode.CATEGORY_CHECK_ERROR);
        }
    }

    @GetMapping("/show/all/category")
    @ApiOperation("模糊查询、展示所有人申请的课题")
    @Transactional(rollbackFor = Exception.class)
    public R showAllApplyCgy(@Validated @ModelAttribute SelectForm selectForm){
        try {
            IPage<Category> categoryIPage = teacherService.pageCategory(selectForm);
            PageForm<Category> categoryPageForm = new PageForm<>();
            categoryPageForm.setTotal(categoryIPage.getTotal());
            categoryPageForm.setList(categoryIPage.getRecords());


            return R.okey(ResultCode.SUCCESS).put("data",categoryPageForm);
        } catch (Exception e) {
            throw new MyException(ResultCode.FAIL);
        }
    }
    @DeleteMapping("/delete/choose")
    @ApiOperation("删除选择我的课题的学生（即删除cho_cgy_record和cho_cgy）")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("删除选择我的课题的学生")
    public R deleteChoMy(@Validated @ModelAttribute DeleteForm deleteForm){
        try {

            choCgyService.deleteChoMy(deleteForm);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }

    @PostMapping("/batch/check/category/leader")
    @ApiOperation("院系老师批量审核课题")
//    @LogOperation("院系老师批量审核课题")
    @Transactional(rollbackFor = Exception.class)
    public R leaderCheckCgy(@Validated @RequestBody CheckList checkList){
        try {
            List<Integer> list = checkList.getList();
            for (Integer id : list) {
                Category category = categoryService.getById(id);
                if(category.getCgyStatus()!=2){
                    return R.error(ResultCode.STUDENT_ADVISE_ERROR);
                }
                if(category.getCgyStatus()==2){
                    if(checkList.getStatus()==1){
                        categoryService.deleteAllCgy(id);
                    }
                    category.setCgyLeadstatus(checkList.getStatus());
                    category.setCgyLeadadvice(checkList.getAdvise());
                    categoryService.updateById(category);
//                    &&category.getCgySource()==1
                    if(category.getCgySource()==1&&checkList.getStatus()==2){
                        choCgyRecordService.insertChoCgyRecord(category);
                    }
                }
            }
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.FAIL);
        }
    }
    @PostMapping("/category")
    @ApiOperation("老师申报课题")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("老师申报课题")
    public R applyCgy(@Validated @RequestBody TeaAddCgyForm teaAddCgyForm){
        try {
            User user =  LoginController.user1;
            Category category = new Category();
            Category category1 = categoryService.setTeaCategory(teaAddCgyForm, user, category);
            category1.setTeaName(user.getName());
            category1.setCgyStatus(2);
            Category category2 = categoryService.selectByCgyName(teaAddCgyForm.getCgyName());
            if(category2!=null){
                return R.error(ResultCode.CATEGORY_APPLY_ERROR3);
            }
            categoryService.insertCategory(category1);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ResultCode.USER_INSERT_ERROR);
        }
    }

    @ApiOperation("展示，模糊查询自己申报的课题")
    @GetMapping("/show/my/categorys")
//    @LogOperation("展示，模糊查询自己申报的课题")
    @Transactional(rollbackFor = Exception.class)
    public R getCgy(@Validated @ModelAttribute SelectForm selectForm) {
        User user =  LoginController.user1;
        IPage<Category> categoryIPage = categoryService.teaPageMyCategory(selectForm,user);
        List<Category> records = categoryIPage.getRecords();
        PageForm<Category> categoryPageForm = new PageForm<>();
        categoryPageForm.setTotal(categoryIPage.getTotal());
        categoryPageForm.setList(records);
        return R.okey(ResultCode.SUCCESS).put("data",categoryPageForm);
    }




    @PostMapping("/update/category")
    @ApiOperation("老师修改申报的课题")
//    @LogOperation("老师修改申报的课题")
    @Transactional(rollbackFor = Exception.class)
    public R updateMyCgy(@Validated @RequestBody TeaAddCgyForm addCgyForm){
        try {
            User user =  LoginController.user1;
            Category category = categoryService.getById(addCgyForm.getCgyId());
            Category category1 = categoryService.setTeaCategoryUpdate(addCgyForm, user, category);
            category1.setTeaName(user.getName());
            category1.setCgyStatus(1);
            categoryService.updateById(category1);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }
    @DeleteMapping("/delete/my/categorys")
    @ApiOperation("老师删除申报的课题,当老师删除学生的申报的课题信息时,同时如果选课表中有该课题，应该一并删除")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("老师删除申报的课题")
    public R deleteMyCgy(@Validated @ModelAttribute DeleteForm deleteForm){
        try {
            User user =  LoginController.user1;
            categoryService.deleteCgy(deleteForm,user);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }
    @GetMapping("/show/wenxian")
    @ApiOperation("多条件（审核通过还是驳回）展示，模糊查询选择我为指导老师的文献")
    @Transactional(rollbackFor = Exception.class)
    public R getWenXian(@Validated @ModelAttribute SelectWenForm selectForm){
        try {
            User user = LoginController.user1;
            IPage<WenFile> page = wenFileService.page(selectForm,user);
            List<WenFile> records = page.getRecords();
            PageForm<WenFile> wenFilePageForm = new PageForm<>();
            wenFilePageForm.setList(records);
            wenFilePageForm.setTotal(page.getTotal());
            return R.okey(ResultCode.SUCCESS).put("data",wenFilePageForm);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @PostMapping("/down/file")
//    @LogOperation("下载文件")
    @ApiOperation("下载文件")
    public R download2(@Validated @RequestBody FileForm fileForm, HttpServletResponse response) {
        try {
            String path=fileForm.getPath()+"/"+fileForm.getFilename()+"."+ fileForm.getSuffix();

            response.addHeader("Content-Disposition", "attachment;filename=" +new String((fileForm.getFilename()+"."+ fileForm.getSuffix()).getBytes("UTF-8"),"ISO-8859-1"));

            aliyunOSSUtil.exportOssFile(response.getOutputStream(),path);
            return R.okey(ResultCode.SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            return R.error(ResultCode.FAIL);
        }
    }
    @PostMapping("/down/wenxian")
//    @LogOperation("下载文献")
    @ApiOperation("下载文献")
    public R downWenxian(@NotNull @RequestParam("id")int id, HttpServletResponse response)  {
        try {

            WenFile wenFile = wenFileService.getById(id);
            //文件路径
            String path=wenFile.getPath()+"/"+wenFile.getFilecode()+"."+ wenFile.getSuffix();

            response.addHeader("Content-Disposition", "attachment;filename=" +new String((wenFile.getFilename()+"."+ wenFile.getSuffix()).getBytes("UTF-8"),"ISO-8859-1"));
            aliyunOSSUtil.exportOssFile(response.getOutputStream(),path);
            return R.okey(ResultCode.SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            return R.error(ResultCode.FAIL);
        }
    }

    @PostMapping("/check/wenxian")
    @ApiOperation("老师审核文献")
//    @LogOperation("老师审核文献")
    @Transactional(rollbackFor = Exception.class)
    public R checkWenXian(@Validated @RequestBody CheckList checkList){
        try {

            List<Integer> list = checkList.getList();
            for (Integer id : list) {
                WenFile wenFile = wenFileService.getById(id);
                wenFile.setAdvise(checkList.getAdvise());
                wenFile.setStatus(checkList.getStatus());
                wenFileService.updateById(wenFile);
            }
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            return R.error(ResultCode.FAIL);
        }
    }

    @GetMapping("/show/report")
    @ApiOperation("展示，模糊查询（多条件：已审核，未审核）选择我为指导老师的开题报告")
    @Transactional(rollbackFor = Exception.class)
    public R getReport(@Validated @ModelAttribute SelectReportForm selectReportForm){
        try {
            User user =  LoginController.user1;
            IPage<ReportFile> reportFileIPage = reportFileService.pageReport(user, selectReportForm);
            PageForm<ReportFile> reportPageForm = new PageForm<>();
            reportPageForm.setTotal(reportFileIPage.getTotal());
            reportPageForm.setList(reportFileIPage.getRecords());
            return R.okey(ResultCode.SUCCESS).put("data",reportPageForm);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }


    @GetMapping("/report/{id}/info")
    @ApiOperation("拿到开题报告的具体信息")
    public R getReport(@PathVariable("id") int id){
        try {
            Report report = reportService.getById(id);
            return R.okey(ResultCode.SUCCESS).put("data",report);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }


    @PostMapping("/batch/check/report")
    @ApiOperation("批量审核开题报告")
//    @LogOperation("批量审核开题报告")
    @Transactional(rollbackFor = Exception.class)
    public R batchgetReport(@Validated @RequestBody CheckList  checkList){
        try {
            List<Integer> list = checkList.getList();
            for (Integer id : list) {
                ReportFile reportFile = reportFileService.getById(id);
                reportFile.setStatus(checkList.getStatus());
                reportFile.setAdvise(checkList.getAdvise());
                reportFileService.updateById(reportFile);
            }
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_CHECK_ERROR);
        }
    }
    @PostMapping("/down/report")
//    @LogOperation("下载开题报告")
    @ApiOperation("下载开题报告")
    public R downReport(@NotNull @RequestParam("id")int id, HttpServletResponse response)  {
        try {

            ReportFile reportFile = reportFileService.getById(id);
            //文件路径
            String path=reportFile.getPath()+"/"+reportFile.getFilecode()+"."+ reportFile.getSuffix();

            response.addHeader("Content-Disposition", "attachment;filename=" +new String((reportFile.getFilename()+"."+ reportFile.getSuffix()).getBytes("UTF-8"),"ISO-8859-1"));
            aliyunOSSUtil.exportOssFile(response.getOutputStream(),path);
            return R.okey(ResultCode.SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            return R.error(ResultCode.FAIL);
        }
    }

    @DeleteMapping("/delete/report")
    @ApiOperation("删除开题报告")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("删除开题报告")
    public R deleteReport(@Validated @ModelAttribute DeleteForm  deleteForm){
        try {
            reportFileService.deletebyId(deleteForm);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }


    @GetMapping("/show/records/student")
    @ApiOperation("展示，模糊查询选择我为指导老师的学生的指导记录")
    public R getRecords(@Validated @ModelAttribute SelectForm selectForm){
        try {
            User user =  LoginController.user1;
            IPage<Records> page = recordsService.page(selectForm, user);
            PageForm<Records> reportPageForm = new PageForm<>();
            reportPageForm.setTotal(page.getTotal());
            reportPageForm.setList(page.getRecords());
            return R.okey(ResultCode.SUCCESS).put("data",reportPageForm);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }
    @GetMapping("/records/{id}/info")
    @ApiOperation("拿到学生的具体指导记录")
    public R getRecordById(@PathVariable("id") int id){
        try {
            Records records = recordsService.getById(id);
            return R.okey(ResultCode.SUCCESS).put("data",records);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @DeleteMapping("/delete/records")
    @ApiOperation("删除学生的指导记录(这个批量和单删的接口都是这一个)")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("删除学生的指导记录")
    public R deleteRecords(@Validated @ModelAttribute DeleteForm  deleteForm){
        try {
            recordsService.deleteRecord(deleteForm.getList());
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }
    @GetMapping("/show/final/choose")
    @ApiOperation("模糊展示所有 已经确认 选择我为指导老师的学生和这个学生的课题，右边一个按钮为他填写指导记录，供院系查看")
    @Transactional(rollbackFor = Exception.class)
    public R deleteRecords(@Validated @ModelAttribute SelectForm  selectForm){
        try {
            User user =  LoginController.user1;
            IPage<ChoCgy> page = choCgyService.page(selectForm, user);
            PageForm<ChoCgy> choCgyPageForm = new PageForm<>();
            choCgyPageForm.setList(page.getRecords());
            choCgyPageForm.setTotal(page.getTotal());
            return R.okey(ResultCode.SUCCESS).put("data",choCgyPageForm);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @PostMapping("/records")
    @ApiOperation("提交指导日志（院）")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("提交指导日志（院）")
    public R commitRecord(@Validated @RequestBody AddRecord  addRecord){
        try {
            User user =  LoginController.user1;
            recordsService.insertRecordTea(addRecord,user);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_INSERT_ERROR);
        }
    }

    /**
     * 得到reportecord茶
     *
     * @return {@link R}
     */
    @GetMapping("/show/records")
    @ApiOperation("查看我为这个学生填写的指导记录(参数：学生学号),(可能有几条)")
    @Transactional(rollbackFor = Exception.class)
    public R getRecordTea(@Validated @ModelAttribute SelectForm selectForm){
        try {

            IPage<Records> recordsIPage = recordsService.listRecords(selectForm);

            return R.okey(ResultCode.SUCCESS).put("data",recordsIPage);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_INSERT_ERROR);
        }
    }

    @GetMapping("/record/{id}/info")
    @ApiOperation("查看我为这个学生填写的指导记录的具体信息")
    @Transactional(rollbackFor = Exception.class)
    public R getRecordTeaById(@Validated @PathVariable("id") int id){
        try {
            Records records = recordsService.getById(id);
            return R.okey(ResultCode.SUCCESS).put("data",records);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_INSERT_ERROR);
        }
    }

    @PostMapping("/update/records")
    @ApiOperation("修改指导日志")
    @Transactional(rollbackFor = Exception.class)
    public R updateRecord(@Validated @RequestBody AddRecord addRecord){
        try {

            Records records = recordsService.getById(addRecord.getRecordsId());
            records.setStuSituation(addRecord.getStuSituation());
            records.setReImfor(addRecord.getReImfor());
            records.setReTarget(addRecord.getSusername());
            recordsService.updateById(records);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_UPDATE_ERROR);
        }
    }

    @DeleteMapping("/delete/records/student")
    @ApiOperation("老师删除自己填写给学生的指导记录(院系老师查看所有人的指导记录时，也可以用这个接口删)")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("老师删除自己填写给学生的指导记录")
    public R deleteRecordById(@Validated @ModelAttribute DeleteForm deleteForm){
        try {
            recordsService.deleteRecord(deleteForm.getList());
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }

    @GetMapping("/show/all/records")
    @ApiOperation("院系老师 模糊展示所有人的指导记录（多条件查询：老师还是学生，院系是哪个）")
    @Transactional(rollbackFor = Exception.class)
    public R getAllRecords(@Validated @ModelAttribute SelectRecordForm selectRecordForm){
        try {
            User user =  LoginController.user1;

            IPage<Records> recordsIPage = recordsService.pageRecordAll(selectRecordForm,user);
            PageForm<Records> recordsPageForm = new PageForm<>();
            recordsPageForm.setList(recordsIPage.getRecords());
            recordsPageForm.setTotal(recordsIPage.getTotal());

            return R.okey(ResultCode.SUCCESS).put("data",recordsPageForm);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @GetMapping("/show/papers")
    @ApiOperation("老师 模糊展示学生的论文")
    @Transactional(rollbackFor = Exception.class)
    public R getPaper(@Validated @ModelAttribute SelectForm selectForm){
        try {
            User user =  LoginController.user1;
            IPage<PaperFile> page = paperFileService.page(selectForm, user);
            PageForm<PaperFile> paperFilePageForm = new PageForm<>();
            paperFilePageForm.setList(page.getRecords());
            paperFilePageForm.setTotal(page.getTotal());
            return R.okey(ResultCode.SUCCESS).put("data",paperFilePageForm);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @PostMapping("/down/paper")
//    @LogOperation("下载论文")
    @ApiOperation("下载论文")
    public R downPaper(@NotNull @RequestParam("id")int id, HttpServletResponse response)  {
        try {

            PaperFile paperFile = paperFileService.getById(id);
            //文件路径
            String path=paperFile.getPath()+"/"+paperFile.getFilecode()+"."+ paperFile.getSuffix();

            response.addHeader("Content-Disposition", "attachment;filename=" +new String((paperFile.getFilename()+"."+ paperFile.getSuffix()).getBytes("UTF-8"),"ISO-8859-1"));
            aliyunOSSUtil.exportOssFile(response.getOutputStream(),path);
            return R.okey(ResultCode.SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            return R.error(ResultCode.FAIL);
        }
    }
    @PostMapping("/batch/check/paper")
    @ApiOperation("老师批量审核学生的论文(审核和批量审核都是这一个)")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("老师批量审核学生的论文")
    public R batchCheckPaper(@Validated @RequestBody CheckList checkList){
        try {

            List<Integer> list = checkList.getList();
            for (Integer integer : list) {
                PaperFile paperFile = paperFileService.getById(integer);
                paperFile.setStatus(checkList.getStatus());
                paperFile.setAdvise(checkList.getAdvise());
                paperFileService.updateById(paperFile);
            }
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @DeleteMapping("/batch/delete/paper")
    @ApiOperation("老师批量删除学生的论文")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("老师批量删除学生的论文")
    public R batchCheckPaper(@Validated @ModelAttribute DeleteForm deleteForm){
        try {

           paperFileService.deletePaperTea(deleteForm);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }

    @GetMapping("/show/defence/self")
    @ApiOperation("老师查询自己的答辩安排")
//    @LogOperation("老师查询自己的答辩安排")
    @Transactional(rollbackFor = Exception.class)
    public R getDefence(@Validated @ModelAttribute SelectForm selectForm){
        try {
            User user =  LoginController.user1;
            IPage<Defence> defenceTea = defenceService.getDefenceTea(selectForm, user.getUsername());

            return R.okey(ResultCode.SUCCESS).put("data",defenceTea);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @GetMapping("/show/defences")
    @ApiOperation("院系老师查询本院的答辩安排")
//    @LogOperation("院系老师查询本院答辩安排")
    @Transactional(rollbackFor = Exception.class)
    public R getDefenceDept(@Validated @ModelAttribute SelectForm selectForm){
        try {
            User user =  LoginController.user1;
            IPage<Defence> defenceTea = defenceService.select(selectForm, user.getDepartment());

            return R.okey(ResultCode.SUCCESS).put("data",defenceTea);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    /**
     * 安排国防
     *
     * @param addDefence 增加答辩
     * @return {@link R}
     */
    @PostMapping("/defence")
    @ApiOperation("院系老师以指导老师为单位安排答辩")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("院系老师以指导老师为单位安排答辩")
    public R arrangeDefence(@Validated @RequestBody AddDefence addDefence){
        try {
            User user =  LoginController.user1;
            defenceService.insertDefence(addDefence,user);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            return R.error(ResultCode.DEFENCE_TEACHER_ERROR);
        }
    }

    @GetMapping("/defence/{id}/info")
    @ApiOperation("拿到答辩安排具体信息")
    @Transactional(rollbackFor = Exception.class)
    public R getDefenceById(@Validated @PathVariable("id") int id){
        try {
            Defence defence = defenceService.getById(id);
            return R.okey(ResultCode.SUCCESS).put("data",defence);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }
    @PostMapping("/update/defence")
    @ApiOperation("修改安排答辩")
//    @LogOperation("修改安排答辩")
    @Transactional(rollbackFor = Exception.class)
    public R updateDefence(@Validated @RequestBody AddDefence addDefence){
        try {
            defenceService.updateDefence(addDefence);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            return R.error(ResultCode.DEFENCE_TEACHER_ERROR);
        }
    }
    @DeleteMapping("/delete/defence")
    @ApiOperation("删除安排答辩")
//    @LogOperation("删除安排答辩")
    @Transactional(rollbackFor = Exception.class)
    public R updateDefence(@Validated @ModelAttribute DeleteForm deleteForm){
        try {
            defenceService.delete(deleteForm);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }
  
    /**
     * 输入级
     *
     * @param addGrade 添加成绩
     * @return {@link R}
     */
    @PostMapping("/grade")
    @ApiOperation("指导老师录入成绩")
//    @LogOperation("指导老师录入成绩")
    @Transactional(rollbackFor = Exception.class)
    public R enterGrade(@Validated @RequestBody AddGrade addGrade){
        try {

            choCgyService.insertGrade(addGrade);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @GetMapping("/grade/{id}/info")
    @ApiOperation("拿到具体成绩")
    @Transactional(rollbackFor = Exception.class)
    public R enterGrade(@Validated @PathVariable("id") int id){
        try {

            ChoCgy choCgy = choCgyService.getById(id);

            return R.okey(ResultCode.SUCCESS).put("data",choCgy);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    @GetMapping("/show/all/grades")
    @ApiOperation("指导老师模糊查询所有人的成绩")
    @Transactional(rollbackFor = Exception.class)
    public R get(@Validated @ModelAttribute SelectForm selectForm){
        try {
            IPage<ChoCgy> choCgyIPage = choCgyService.pageAll(selectForm);
            PageForm<ChoCgy> choCgyPageForm = new PageForm<>();
            choCgyPageForm.setList(choCgyIPage.getRecords());
            choCgyPageForm.setTotal(choCgyIPage.getTotal());
            return R.okey(ResultCode.SUCCESS).put("data",choCgyPageForm);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    /**
     * 期中答辩安排
     */
    @GetMapping("/show/midtermDefences")
    @ApiOperation("院系老师查询本院的期中答辩安排")
    @Transactional(rollbackFor = Exception.class)
    public R getMidtermDefenceDept(@Validated @ModelAttribute SelectForm selectForm){
        try {
            User user =  LoginController.user1;
            IPage<MidtermDefence> midtermDefenceTea = midtermDefenceService.select(selectForm, user.getDepartment());

            return R.okey(ResultCode.SUCCESS).put("data",midtermDefenceTea);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    /**
     * 修改期中答辩安排
     */
    @PostMapping("/update/midtermDefence")
    @ApiOperation("修改期中安排答辩")
//    @LogOperation("修改安排答辩")
    @Transactional(rollbackFor = Exception.class)
    public R updateMidtermDefence(@Validated @RequestBody AddDefence addDefence){
        try {
            midtermDefenceService.updateDefence(addDefence);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            return R.error(ResultCode.DEFENCE_TEACHER_ERROR);
        }
    }

    /**
     * 添加期中答辩
     */
    @PostMapping("/midtermDefence")
    @ApiOperation("院系老师以指导老师为单位安排期中答辩")
    @Transactional(rollbackFor = Exception.class)
//    @LogOperation("院系老师以指导老师为单位安排期中答辩")
    public R arrangeMidtermDefence(@Validated @RequestBody AddDefence addDefence){
        try {
            User user =  LoginController.user1;
            midtermDefenceService.insertDefence(addDefence,user);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            return R.error(ResultCode.DEFENCE_TEACHER_ERROR);
        }
    }

    /**
     * 获取指定id期中答辩安排
     */
    @GetMapping("/midtermDefence/{id}/info")
    @ApiOperation("拿到期中答辩安排具体信息")
    @Transactional(rollbackFor = Exception.class)
    public R getMidtermDefenceById(@Validated @PathVariable("id") int id){
        try {
            MidtermDefence midtermDefence = midtermDefenceService.getById(id);
            return R.okey(ResultCode.SUCCESS).put("data",midtermDefence);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_SELECT_ERROR);
        }
    }

    /**
     * 删除期中答辩安排
     */
    @DeleteMapping("/delete/midtermDefence")
    @ApiOperation("删除安排期中答辩")
//    @LogOperation("删除安排期中答辩")
    @Transactional(rollbackFor = Exception.class)
    public R updateMidtermDefence(@Validated @ModelAttribute DeleteForm deleteForm){
        try {
            midtermDefenceService.delete(deleteForm);
            return R.okey(ResultCode.SUCCESS);
        } catch (Exception e) {
            throw new MyException(ResultCode.USER_DELETE_ERRPR);
        }
    }

    /**
     * 上传检查报告文件返回前端url
     */
    @PostMapping("/getInspectionReportUrl")
    @ApiOperation("上传检查报告返回路径")
    public R getInspectionReportUrl(@Validated @RequestParam("file") MultipartFile file) {
        String filename = file.getOriginalFilename();
        System.out.println("文件名:"+filename+"=====================");
        // 获取配置文件中value
        ResourceBundle resource = ResourceBundle.getBundle("oss");
        String inspectionurl = resource.getString("inspectionurl");
        // 上传至oss
        assert filename != null;
        // 生成filecode oss文件名
        String filecode = aliyunOSSUtil.getFileCode();
        if (!"".equals(filename.trim())) {

            String[] split = new String[2];
            split[0]= filename.substring(0,filename.lastIndexOf("."));
            split[1] = filename.substring(filename.lastIndexOf(".")+1);

            if(AliyunOSSUtil.TXT.equals(split[1])|| AliyunOSSUtil.DOCX.equals(split[1])|| AliyunOSSUtil.PDF.equals(split[1])|| AliyunOSSUtil.DOC.equals(split[1])|| AliyunOSSUtil.XLS.equals(split[1])){
                System.out.println("类型正确");
            }else{
                return R.error(ResultCode.USER_UPFILE_ERROR);
            }
            // 上传到OSS
            aliyunOSSUtil.upLoad3(file, inspectionurl,filecode,split[1]);
        }
        // 后缀
        String suffix = filename.substring(filename.lastIndexOf(".")+1);
        String path = inspectionurl + "/" + filecode+"."+suffix;
        return R.okey(ResultCode.SUCCESS).put("path", path);
    }

    /**
     *上传检查报告
     */
    @PostMapping("/uploadInspectionReport")
    @ApiOperation("上传检查报告")
    @Transactional(rollbackFor = Exception.class)
    public R uploadInspectionReport(@Validated @RequestBody InspectionReport inspectionReport) {
        // 判断是否已经给该学生发送过检查报告
        int flag = inspectionReportService.getCount(inspectionReport);
        if(flag > 0) {
            // 更新检查报告
            LambdaQueryWrapper<InspectionReport> lqw = new LambdaQueryWrapper<>();
            lqw.eq(InspectionReport::getSusername, inspectionReport.getSusername());
            lqw.eq(InspectionReport::getStage, inspectionReport.getStage());
            // 更新
            if(inspectionReportService.update(inspectionReport, lqw)) {
                // 删除原oss文件
                InspectionReport inspectionReport1 = (InspectionReport) inspectionReportService.getObj(lqw);
                String path = inspectionReport1.getPath();
                aliyunOSSUtil.deleteFile(path);
                System.out.println("远程oss文件删除成功");
            }
        } else {
            // 存储检查报告
            inspectionReportService.save(inspectionReport);
            // 修改提交状态
            inspectionReportService.updateStatus(inspectionReport.getSusername());
        }
        return R.okey(ResultCode.SUCCESS);
    }

    /**
     * 下载审查报告模板
     */

}

