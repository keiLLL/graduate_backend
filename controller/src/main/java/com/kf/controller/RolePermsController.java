package com.kf.controller;


import com.kf.service.RolePermsService;
import com.kf.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 */
@RestController
@RequestMapping("/role-perms")
@Api("为角色分配权限")
public class RolePermsController {
    @Autowired
    RolePermsService rolePermsService;

    @ApiOperation("为角色分配权限")
    @PostMapping("/setRolePerm")
    public R setRolePerm(@RequestParam("roleid") int roleId, @RequestParam("permId")List<Integer> list){
        try {
            rolePermsService.insertRolePerm(roleId,list);
            return   R.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return R.error();
        }
    }


}

