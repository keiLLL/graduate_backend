package com.kf.config.shiro;

import org.apache.shiro.authc.HostAuthenticationToken;
import org.apache.shiro.authc.RememberMeAuthenticationToken;

/**
 * 电话号码牌
 */
public class PhoneCodeToken implements HostAuthenticationToken, RememberMeAuthenticationToken {
    private String phone;
    private boolean remember;
    private String host;


    public PhoneCodeToken(String phone){
        this(phone,false,null);
    }


    public PhoneCodeToken(String phone, boolean remember, String host){
        this.phone=phone;
        this.remember=remember;
        this.host=host;
    }
    @Override
    public String getHost() {
        return host;
    }

    @Override
    public boolean isRememberMe() {
        return remember;
    }

    @Override
    public Object getPrincipal() {
        return phone;
    }

    @Override
    public Object getCredentials() {
        return phone;
    }
}